// Planetary gear box for Nema16 motors
//
// Based on Thingiverse Thing 16897
//
// Marcos Ackel 2016-08-03

//include <./MCAD/involute_gears.scad>;
use <./MCAD/involute_gears.scad>;
//use <MetricScrews.scad>

// -- Debug:
//ring1();
//fourgears();
//carrierInt();
//carrierFinal(ogNTeeth, ogModulus, ogHeight);
//sun();
//planet();
//CoverBt();
//CoverTop(11, 1);

// -- To Print:
//ring1();
//CoverBt();
//sun();
//planet();    // print 6x
//carrierInt();
//carrierFinal();
//CoverTop();

// ---- Assembly:
assembly();

//----------------------------------
$fa = 0.5;
$fs = 0.5;

nema16_hole_dist = 31;   // Nema 16 motor holes for screws
nema16_hole_diam = 3.3;  // hole diameter for M3 screw
nema16Width = 39.2;
nema16Axis_diam  = 5.0;  // axis diameter


d1=nema16Width;// diameter of lower ring (external width)
t=3.0;// thickness of all gears
b=0.1;// backlash
c=0.2;// clearance
pa=20;// pressure angle
s=0.6;// vertical clearance
dp=0.90;// ring gear diameter / outside ring diameter

// Outer gear (output driver)
ogNTeeth = 11;  // number of teeth
ogModulus = 1;  // 1 mm modulus
ogHeight  = 13; // 10mm

// For equally spaced planets AND in phase: both sun and ring #teeth must be evenly divisible ty the number of planets
// For equaly spaced planets BUT not in the same phase: the sum of ring+sun must be evenly divisible by the # planets
// For unevenly spaced planets: planets angle constrained by:
//      AngleP2P = 360 * N / (R+S)          (N is a whole number)
// 1:5.25:    16:26
// 1:5:     >18-27  12-18 
// 1:4.8     10-14  15-21
// 1:4.667    9-12
// 1:4.61538 13-17
// 1:4.5     12-15  
ns=10;// number of teeth on sun (lower)   // even?
np1=14;// number of teeth on lower planet  // odd?


plug=2.0;   // used as planetary gear axis
axis=nema16Axis_diam/2;   // motor nema16 axis RADIUS 


//--------- Don't edit below here unless you know what you're doing.

nr1=ns+2*np1;// number of teeth on lower ring
pitch=nr1/(d1*dp);// diametral pitch of all gears

R1=(1+nr1/ns);// sun to planet-carrier ratio
Rp=(np1+ns)/np1;// planet to planet-carrier ratio
R=R1;
echo(str("<b>PITCH  = ",pitch,"</b>"));
echo(str("<b>MODULE = ",1/pitch,"</b>"));
echo(str("Ring Teeth # is : ",nr1));
echo(str("<b>Gear Ratio is 1 : ",R1,"</b>"));
//----------------------------------

module assembly(){
    // Ring:
color([0.5,0.5,0.5]) ring1();
    
    // Sun:
deltaSun = (np1 % 2 == 0)? 180/ns : 0;
translate([0,0,0+s]) rotate([0,0,deltaSun+360*R*R*$t])  //180/ns+360*R*$t])
	color([1,0,0]) sun();
    // 3 planets (first level):
rotate([0,0,360*R*$t]) translate([(ns+np1)/pitch/2,0,0+s])
    rotate([0,0,-360*R*(1+Rp)*$t])
	color([0,0,1]) planet();
rotate([0,0,120+360*R*$t]) translate([(ns+np1)/pitch/2,0,0+s])rotate([0,0,-120-360*R*(1+Rp)*$t])
	color([0,0,1]) planet();
rotate([0,0,-120+360*R*$t]) translate([(ns+np1)/pitch/2,0,0+s])rotate([0,0,120-360*R*(1+Rp)*$t])
	color([0,0,1]) planet();
    
    // Interm carrier:
rotate([0,0,360*R*$t]) translate([0,0,t+2*s])	color([0,1,1])carrierInt();
    
    // 3 planets (second level):
rotate([0,0,360*R*$t]) translate([(ns+np1)/pitch/2,0,2*t+3*s]) rotate([0,0,-360*R*(1+Rp)*$t])
	color([0,0,1]) planet();
rotate([0,0,120+360*R*$t]) translate([(ns+np1)/pitch/2,0,2*t+3*s]) rotate([0,0,-120-360*R*(1+Rp)*$t])
	color([0,0,1]) planet();
rotate([0,0,-120+360*R*$t]) translate([(ns+np1)/pitch/2,0,2*t+3*s]) rotate([0,0,120-360*R*(1+Rp)*$t])
	color([0,0,1]) planet();
    
    // Final carrier:
rotate([0,0,360*R*$t]) translate([0,0,3*t+4*s])	color([0,1,1])carrierFinal();

    // bottom cover:
color([0.5,0.5,0.5]) translate([0,0,-3-c/2]) CoverBt();

    // Top cover:
color([0.5,0.5,0.5]) translate([0,0,4*t+5*s+c/2]) CoverTop();


    
//rotate([0,0,360*R*$t])translate([(ns+np1)/pitch/2,0,0])rotate([0,0,-360*R*(1+Rp)*$t])
	//color([0,0,1])planet();
//rotate([0,0,120+360*R*$t])translate([(ns+np1)/pitch/2,0,0])rotate([0,0,-120-360*R*(1+Rp)*$t])
	//color([0,0,1])planet();
//rotate([0,0,-120+360*R*$t])translate([(ns+np1)/pitch/2,0,0])rotate([0,0,120-360*R*(1+Rp)*$t])
	//color([0,0,1])planet();
//rotate([0,0,360*R*$t])translate([(ns+np1)/pitch/2,0,0])	color([0,1,0])arm();
//*rotate([0,0,120+360*R*$t])translate([(ns+np1)/pitch/2,0,0])	color([0,1,0])arm();
//*rotate([0,0,-120+360*R*$t])translate([(ns+np1)/pitch/2,0,0])	color([0,1,0])arm();
}
//----------------------------------
/*
module fourgears(){
	sun();
	for(i=[0,120,-120])
		rotate([0,0,i])translate([1.2*(ns+np1)/pitch/2,0,0])
			rotate([0,0,-i])planet();
}*/

//----------------------------------

//===== Sun
module sun(height=t)
difference(){
    union() {
        gear(number_of_teeth=ns,
            diametral_pitch=pitch,
            gear_thickness=height,
            rim_thickness=height,
            hub_thickness=height,
            bore_diameter=0,
            backlash=b,
            clearance=c,
            pressure_angle=pa);
        translate([0,0,-(0.8+s)]) cylinder(r=((ns+2)*1/pitch)/2, 0.8+s);
    } 
    cylinder(r=axis+c/2, h=t*3, center=true);
}
//----------------------------------

//===== Planet
module planet()
union(){
	gear(number_of_teeth=np1,
		diametral_pitch=pitch,
		gear_thickness=t,   //t+s+td/2
		rim_thickness=t,
		hub_thickness=t,
		bore_diameter=0,
		backlash=b,
		clearance=c,
		pressure_angle=pa);
    // ressalto para diminuir atrito:
    cylinder(r=plug*1.6, h=t+0.1);
    // eixo da engrenagem planetaria:
    cylinder(r=plug, h=t*2+s/2);

}
//----------------------------------

module carrierFinal()
{
// global: ogNTeech, ogModulus, ogHeight    
// outerGear:
ogDiamPitch = 1/ogModulus;
ogORadius = (ogModulus * (ogNTeeth + 2))/2;
ogThick = s + 2 + ogHeight;

difference() {
    union() {
        carrier();
        translate([0,0,t]) 
            gear(number_of_teeth=ogNTeeth,
                diametral_pitch=ogDiamPitch,
                gear_thickness=ogThick,
                rim_thickness=ogThick,
                hub_thickness=ogThick,
                bore_diameter=0,
                backlash=b,
                clearance=c,
                pressure_angle=pa);
        translate([0,0,t]) cylinder(r=ogORadius,h=2+s+c);
    }
    // hole for the motor axis:
    cylinder(r=axis+c, h=t*12, center=true);
}
}
//----------------------------------

module carrierInt()
{
    difference() {
        union() {
            carrier();
            translate([0,0,t]) color([0,1,0]) sun(t+s/2);
        }
        // orificio para o eixo do motor:
        cylinder(r=axis+c, h=t*3, center=true);
    }
}
//----------------------------------

module carrier()
{
    cDist = (ns+np1)/pitch/2;   // dist sun-planet centers
    extDist = cDist + plug;
    //pt1 = [0,cDist];
    //pt2 = [ cDist*cos(30),-cDist*sin(30),0];
    //pt3 = [-cDist*cos(30),-cDist*sin(30),0];

    pt1 = [         cDist,             0,0];
    pt2 = [-cDist*sin(30),-cDist*cos(30),0];
    pt3 = [-cDist*sin(30), cDist*cos(30),0];

    pts = [ pt1, pt2, pt3 ];
    height = t;
    translate([0,0,t/2])
    difference() {
        translate([0,0,-height/2]) color([1,0,1])
            union() {
                scale([extDist/cDist, extDist/cDist, 1.0])                linear_extrude(height) 
                    polygon([ [pt1[0],pt1[1]], [pt2[0],pt2[1]],
                              [pt3[0],pt3[1]] ]);
                for (i=[0:2]) {
                    translate(pts[i]) union() {
                        cylinder(r=plug*2,h=height);
                        // cilindro espacamento para reduzir atrito:
                        cylinder(r=plug*1.6,h=height+c/2);
                    }  
                }
           }
        // 4 holes for the planets axes   
        for (i=[0:2]) {
            translate(pts[i]) cylinder(r=plug+c/2,h=t*2, center=true);
                    //polygon([[0,cDist],[cDist*cos(30),-cDist*sin(30)],[-cDist*cos(30),-cDist*sin(30)]]);
        }
    }
            
}

/*
module arm()
translate([0,0,t])color([0,1,0])
	difference(){
		union(){
    			cylinder(r=4,t*.9);
			translate([t,0,t*.40])rotate([90,0,-120])cube([t/2,t/2,t*4.5]);
		}
  		translate([0,0,-t/2]) cylinder(r=plug*plugGapModifier,h=t*2);
	}
*/
//----------------------------------

module ring1() {
    height = (4*t)+(5*s);
    union() {
        insidegear(nr1);
        // lateral support (2):
        delta = 1.5;
        pos = nema16Width;
        width = nema16_hole_diam+3;
        for( i = [-1,1] ) 
            translate([i*((pos+delta)/2),-(pos+delta)/2,0]) rotate([0,0,i*45]) 
                LateralSupport(width, height);
    }
    
}
//----------------------------------
module LateralSupport(width, height) {
    difference() {
        union() {
            cylinder(d= width, h = height);
            translate([-width/2,0,0]) cube([width,width*2/3,height]);
        }
        cylinder(d=nema16_hole_diam, h=height+2);
    }
}
//----------------------------------

module insidegear(n)
//rotate([180,0,0])translate([0,0,-t])
difference(){
    height = (4*t)+(5*s);
    Block(d1, height);
	translate([0,0,-.5])
        gear(number_of_teeth=n,
            diametral_pitch=pitch,
            gear_thickness=height+5,
            rim_thickness=height+5,
            hub_thickness=height+5,
            bore_diameter=0,
            backlash=-b,
            clearance=0,
            pressure_angle=pa);
}    
    /*
    
	//cylinder(r=n/pitch/2/dp,h=t+t1);
    extDiam = n/pitch/dp;
    extCylRadius = 0.94 * extDiam/2 * sqrt(2);
    
    echo(str("extCylRadius * 2 ="), extCylRadius*2);
    height = (4*t)+(5*s);
    echo(str("Height: "), height);
    
    intersection() {
        translate([0,0,(height)/2]) cube([extDiam, extDiam, height], center=true);
        cylinder(r=extCylRadius, h=height+1);
    }
//	translate([0,0,-0.5])cylinder(r=3,h=t+t1+1);
	translate([0,0,-.5])
	gear(number_of_teeth=n,
		diametral_pitch=pitch,
		gear_thickness=hight+5,
		rim_thickness=hight+5,
		hub_thickness=hight+5,
		bore_diameter=0,
		backlash=-b,
		clearance=0,
		pressure_angle=pa);
    for ( i = [-1,1] ) {
        for (j = [-1,1] ) {
            translate([i*nema16_hole_dist/2, j*nema16_hole_dist/2, 0]) cylinder(r=nema16_hole_diam/2, h = height+1);
        }
    }
    */
//----------------------------------

module Block(width, height)
{
    difference(){
        extDiam = width;
        extCylRadius = 0.94 * extDiam/2 * sqrt(2);
        echo(str("extCylRadius * 2 ="), extCylRadius*2);
        echo(str("extDiam ="), extDiam);
     
        echo(str("Height: "), height);
        
        intersection() {
            translate([0,0,(height)/2]) cube([extDiam, extDiam, height], center=true);
            cylinder(r=extCylRadius, h=height+1);
        }
        // holes for the screws:
        for ( i = [-1,1] ) {
            for (j = [-1,1] ) {
                translate([i*nema16_hole_dist/2, j*nema16_hole_dist/2, 0]) cylinder(r=nema16_hole_diam/2, h = height+1);
            }
        }
    }
}
//----------------------------------

module CoverBt()
{
    difference() {
        Block(nema16Width, 3);
        // orificio para o eixo:
        cylinder(r=c/2+((ns+2)*1/pitch)/2, 2*t);
        // 22x2: ressalto no corpo do motor
        cylinder(r=(22+c)/2, 2+c/2);
    }
}
//----------------------------------

module CoverTop()
{
    // global: ogNTeeth, ogModulus,
    // nema16_hole_dist
    // outerGear:
    ogDiamPitch = 1/ogModulus;
    ogORadius = (ogModulus * (ogNTeeth + 2))/2;
    
    difference() {
        Block(nema16Width, 2);
        cylinder(r=c/2+ogORadius, 2*t, center=true);
        // holes (for the M3 conic screws heads):
        for ( i = [-1,1] ) {
            for (j = [-1,1] ) {
                translate([i*nema16_hole_dist/2, j*nema16_hole_dist/2, 0.1]) cylinder(d1=2.9+0.2,d2=5.4+0.2,h=1.8+0.2);
            }
        }
                //translate([i*nema16_hole_dist/2, j*nema16_hole_dist/2, 0]) cylinder(r=nema16_hole_diam/2, h = height+1);

        
    }
}
//----------------------------------
//==================================