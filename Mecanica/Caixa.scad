// Caixa para CeamigWiFi
// 
// Marcos Ackel - 2017-06-08
//                2017-09-16
//				  2021-08-23
//
// www.ceamig.org.br

$fa=0.5;
$fs=0.5;

altura = 28;
esp = 2.5;   // espessura parede
bhei = 4.0;  // altura da base de suporte da placa
d = 0.1;	 // distancia para acertar cortes no openscad
f  = 0.5;	 // folga 
f0 = 0.3;   // folga menor
compr = 97.20+2*f;		// comprimento da placa PCB
larg  = 54.40+2*f;		// largura da placa


// Para Imprimir:
base();
//tampa();


// Desenvolvimento:
//base();
//translate([0,0,f]) tampa();
//abaChave(4.0);
//abaFix();
//#minkowski() { cube([10,0.1,0.11], center=true); sphere(0.3); };



module base()
{
	wall = 2.0;  // espessura parede suporte
	dsup = 8.0;  // diametro suporte parafusos
	difference() {
		union() {
			translate([-esp-f,-esp-f,-esp]) cube([compr+2*esp+2*f, larg+2*esp+2*f ,esp]);
			translate([0,0,0]) cube([compr, larg , bhei]);
		}
		translate([wall, wall, 0]) cube([(compr-2*wall),larg-2*wall, bhei+d]); 
		// rebaixos de encaixe da tampa:
		$fn=50;
		for (x = [7, compr-8]) {
			for (y = [0, larg]) {
				if ( !(x==compr-8 && y==0) )
					translate([x, y, 2]) minkowski() { cube([10,0.5,0.1], center=true); sphere(0.7); };
			}
		}
		translate([compr, 7, 2]) minkowski() { cube([0.5,10,0.1], center=true); sphere(0.7); };

		// Texto:
		translate([compr/2,15,-esp+0.55]) rotate([0,180,0]) 
					linear_extrude(2*esp){ text("CeamigWiFi", 5, font="Liberation Sans:style=Bold", halign="center"); };
	}
	translate([(compr)/2, (larg)/2, 0]) cylinder(d=8, h=bhei);
	
	// suportes parafusos:
	dfuro = 2.3;
	suporteParafuso(       4.4+f   ,     5.4, 0, dsup, bhei, dfuro);
	suporteParafuso(compr-(4.4+2*f),    10.0, 0, dsup, bhei, dfuro);
	suporteParafuso(       4.4+f   ,larg-5.4, 0, dsup, bhei, dfuro);
	suporteParafuso(compr-(4.4+2*f),larg-5.4, 0, dsup, bhei, dfuro);
	
	// aba para a chave liga/desliga:
	translate([20, larg, 0]) abaChave(bhei);
	
	// barra suporte conectores:
	translate([13+f,-esp-f-d,0]) cube([compr-11.3-(7), 2*esp, bhei]);
	
	// preenchimento para os recortes entre os conectores:
	// ao lado dos RJ45 (motores):
	//for (x = [31.5,64.5]) {
	for (x = [29.0,62.0]) {   
		translate([x,-esp-f-d,0]) cube([6.0, esp, 10-f0]);
	}
	// entre os RJ11 (keypad/I2C):
	translate([46.0,-esp-f-d,0]) cube([5.0, esp, 10-f0]);
	
	// sob o reset:
	translate([84.0,-esp-f-d,0]) cube([10.0, esp, 8.0-f0]);

	
	// barra sob o conec power e leds:
	translate([-esp-f-d, 12+f,0]) cube([2*esp, 32, bhei]);
	
	// sob os leds:
	h = 10;
	translate([-esp-f-d, 22.0,0]) cube([esp, 23-f, h-f0]);

	// abas de fixacao:
	translate([-esp, larg/4, -esp]) abaFix();
	translate([compr, larg/4, -esp]) mirror() abaFix();
	

}
//---------------------------------------------------

module suporteParafuso(x, y, z, dsup, bhei, dfuro)
{
	difference() {
		translate([x, y, z]) cylinder(d=dsup, h=bhei);
		translate([x, y, z+0.01]) cylinder(d=dfuro, h=bhei);
	}		
}
//---------------------------------------------------

module abaChave(bhei)
{
	compr = 14.3;  // compr chave
	larg  = 9.5;   // larg chave
	
	alt   = 20.0-f;   // altura da aba
	base = 38.0;   // base da aba

	cube([base,esp+f,bhei]);
	translate([0,0,bhei])
	difference() {
		cube([base,esp+f,alt]);
		rotate([0,  15, 0]) 	translate([-base,-d,0]) cube([base,esp+10*d,40]);
		translate([ base,-d,0])  rotate([0, -15, 0]) cube([20,esp+10*d,30]);
		// recorte da chave:
		translate([(base-compr)/2,-d,(alt-larg)/2]) cube([compr,esp+10*d, larg]);
	}
	// reforco e protecao da chave:
	translate([     8-1.5*esp/2, esp, -esp]) cube([1.5*esp, 2*esp, alt+esp]);
	translate([base-8-1.5*esp/2, esp, -esp]) cube([1.5*esp, 2*esp, alt+esp]);
	
	//translate([8,     -esp/2,    0]) rotate([0,0,45]) cube([esp/1.5, esp/1.5, alt-esp]);
	//translate([base-8,-esp/2,    0]) rotate([0,0,45]) cube([esp/1.5, esp/1.5, alt-esp]);
	//translate([8,     0,alt-3])      rotate([45,0,0]) cube([base-2*8, esp/1.5, esp/1.5]);
}
//---------------------------------------------------

module abaFix()
{
	largAba = 10;
	difference() {
		rotate([0,0,90]) trapezoidal( larg/2, larg/4, largAba, esp);
		// furo para fixar a base da caixa:
		translate([-esp/2-largAba/2,1*larg/4,0]) cylinder(d=2.5, h=3*esp, center = true);
	}
	//difference() {
	//	translate([-esp/2-largAba/2,0.8*larg/4,esp]) cylinder(d=4.5, h=1.5*esp);
	//	// furo para o parafuso da tampa:
	//	translate([-esp/2-largAba/2,0.8*larg/4,esp/2]) cylinder(d=1.5, h=2.5*esp);
	//}
}
//---------------------------------------------------

module tampa()
{
	epcb=2.0;     // espessura da pdb (printed circuit board)
	difference() {
		translate([-esp-f, -esp-f, 0]) cube([compr+2*esp+2*f, larg+2*esp+2*f , altura]);
		translate([-f, -f, -d]) cube([compr+2*f, larg+2*f , altura-esp+d]);
		// retira a aba da chave on/off:
		translate([20, larg, 0]) minkowski() { abaChave(bhei); sphere(r=f);};
		//#translate([20-d, larg, 0]) abaChave(bhei); 
		translate([30, larg, 5]) cube([18, 2*esp, 18]);;
		// Texto:
		translate([compr/2,10,altura-0.6]) rotate([0,0,0]) 
					linear_extrude(2*esp){ text("CeamigWiFi", 7, font="Liberation Sans:style=Bold", halign="center"); };
					// B.Riedel:
					//linear_extrude(2*esp){ text("B. Riedel", 7, font="Liberation Sans:style=Bold", halign="center"); };
		// ranhuras:
		lr = 1.8;
		cx  = compr/2+2*esp;
		cy  = 23;
		di  = 5;

		for (n = [0:3]) {
			for (i = [-1,+1]) {
				translate([cx+(i*((n*di))), cy, altura-esp-d]) rotate([0,0,25]) 
						hull() { cylinder(d=lr,h=2*esp); 
								 translate([0,25-(n*di/0.9),0]) cylinder(d=lr,h=2*esp); };
			};
		};
		
		// cortes:
		// RJ45 (motores):
		for (n = [13,68]) {
			translate([n,-esp-f-d,0]) cube([15+2*f, 3*esp, 20+f0]);
		}
		// RJ11 (keypad/I2C):
		for (n = [35,51]) {
			translate([n,-esp-f-d,0]) cube([10+2*f, 2*esp, 20+f0]);
		}
		// bt reset:
		translate([92, +esp-f-d, 10]) rotate([90,0,0]) cylinder(d=5.0,h=2*esp);
		//translate([90,-esp-d,0]) cube([4, 2*esp, 19]);

		// base dos recortes desse lado:
		translate([15,-esp-f-d,0]) cube([65+2*f, 3*esp, 10]);
		// reset:
		translate([83.5,-esp-f-d,0]) cube([10.5+f, esp+1, 8.0]);
		translate([89.5,-esp-f-d,8]) cube([5.0, esp+1, 2.0]);

		
		// conector fonte:
		translate([-esp-f-d, 12,0]) cube([2*esp, 10, 16+f0]);
		
		// furo do led power:
		translate([-esp-f-d, 37,12]) rotate([0,90,0]) cylinder(d=4.0,h=2*esp);
		
		// furo do led Ok:
		translate([-esp-f-d, 43,12]) rotate([0,90,0]) cylinder(d=4.0,h=2*esp);
		h=10+3*d;
		translate([-esp-f-d, 15.0,-d]) cube([esp+2*d, 30, h]);
		translate([-esp-f-d, 35.0,h-f-d]) cube([esp+2*d, 4, 2]);
		translate([-esp-f-d, 41.0,h-f-d]) cube([esp+2*d, 4, 2]);
		
		// CORTE para debug:
		//translate([-10,-10,0]) cube([20,2*compr,2*altura]);
	}
	
	// reforcos dos recortes:
	hh = 15.7;
	for ( dx = [31,47.5,64] ) 
		translate([dx,-f,altura-esp-hh]) cube([esp, esp, hh]);
	
	// ressaltos de encaixe da tampa na base:
	$fn=50;
	wi = 8;
	dl = 0.3;   // quanto maior, menor ficam os ressaltos
	for (i = [7, compr-8]) {
		for (y = [0-dl, larg+dl]) {
			if ( !(i==compr-8 && y==0-dl) )
				translate([i, y, 2]) minkowski() { cube([wi,0.5,0.1], center=true); sphere(0.6); };
		}
	}
	translate([compr+dl, 7, 2]) minkowski() { cube([0.5,wi,0.1], center=true); sphere(0.6); };
	
}
//---------------------------------------------------

module trapezoidal(x1, x2, y, z, center=false)
{
	d2 = (x1-x2)/2;
	pts = [ [0,0], [x1,0], [x1-d2,y], [d2,y] ];
	vet_translate = (center)? [-x1/2,-y/2,-z/2] : [0,0,0];
	translate(vet_translate) linear_extrude(z) polygon(pts);
}
//---------------------------------------------------
//---------------------------------------------------
