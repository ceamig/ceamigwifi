// CEAMIG
// www.ceamig.org.br
//
// Projeto de Automacao de Telescopios
// 
// Engrenagem principal - para Azimute: gear191
// Engrenagem cortada - para Altura: gear191_altura
//
// Marcos Ackel 2017-03-02
// 		marcos.ackel@ceamig.org.br
// Marcelo Macedo Moura 2017-04-05
//		mmmoura@ceamig.org.gr

// Engrenagem Grande 191 dentes (para Azimute)
//translate([outRadius, outRadius, -11])
//	gear191();

// Engrenagem Grande cortada 191 dentes (para Altura)
gear191_altura();

// Engrenagem Pequena (apenas para visualizacao)
//translate([outRadius+10, 0, 0])	gear11();	

// Engrenagem do projeto original em SolidWorks:
//import("Engrenagem-Ceamig-191-02.STL");


// Para imprimir:
//gear191();
//gear191_altura();

//------------------------------------------------
$fa = 0.5;
$fs = 0.5;

use <./MCAD/involute_gears.scad>;
// Parametros (solidworks):
// modulo: 1mm
// Number of Teeth = 191
// Gear Width = 10mm
// Pressure Angle = 20 degrees
// Helial Angle = 0 degrees
// Shaft Diameter = 80mm
// Outside Diameter = 193mm (calc: (numTeeth +2) * modulo
// Root Diameter = 188.2mm  
//      calc: rootDiam = outDiameter - 2*(modulo * iif(modulo >=1.25, 2.25, 2.4))
// Number of Holes = 10
// Shaft Width = 25mm


// Gear:
numTeeth = 191;  // number of teeth
modulo = 1;  // 1 mm modulo
height  = 10; // 10mm
diamPitch = 1/modulo;
outRadius = (modulo * (numTeeth + 2)) / 2;
vertClearance = 0;
clearance = 0.2;
thick = height; //vertClearance + 2 + height;
borda = 4;
innerThick = 3;  
diamShaft = 80; // diametro do furo central

backlash = 0.1;
pressAng = 20;  // pressure angle

numHoles = 10;
diamHole = 35;
	
//------------------------------------------------
module gear191()
{	difference() {
		gear191_base(25);
	
		// Furos no corpo:
		posRadius = outRadius-diamHole/2-10;
		delta = 360/numHoles;
		for (i=[0 : numHoles]) {
			translate([posRadius*sin(i*delta),posRadius*cos(i*delta),0])
				cylinder(d=diamHole,h=thick+5, center=true);
		}
	}
}
//------------------------------------------------

module gear191_base(shaft_height)
{
	echo(str("<b>Out  Diameter = ",outRadius*2,"</b>"));
	compl = (modulo >=1.25)? 2.25 : 2.4;
	rootDiam = (outRadius*2) - 2 * (modulo * compl);
	echo(str("<b>Root Diameter = ",rootDiam,"</b>"));
	
	difference() {
		union() {
			translate([0,0,innerThick])
				cylinder(d1=100,d2=90,h=shaft_height-innerThick);  // cone interno
			difference() {
				gear(number_of_teeth=numTeeth,
					diametral_pitch=diamPitch,
					gear_thickness=thick,
					rim_thickness=thick,
					hub_thickness=thick,
					bore_diameter=diamShaft,
					backlash=backlash,
					clearance=clearance,
					pressure_angle=pressAng);
				translate([0,0,innerThick])
					cylinder(d1=(outRadius-borda)*2-6,d2=(outRadius-borda)*2, 
							h= thick-innerThick+0.1);  // rebaixo do corpo
			}
		}
		cylinder(d=diamShaft,h=60, center=true);  // furo central
		
	}

}	
//------------------------------------------------

module gear191_altura()
{
	angGear = 100;   
	diamHoles = 35;
	numHolesUp = 3;
	numHolesDn = 0;
	angHolesUp = [8, 39.5, 71];
	//angHolesDn = [22, 58];
	
	// reforcos laterais:
	for ( i = [0, angGear-2.2] ) {
		rotate([0,0,i]) 	translate([diamShaft/2,0,0])
			cube([outRadius-diamShaft/2-3, 3.5, thick]);
	}
	difference() {	
		gear191_base(15);
		difference() {
		rotate_extrude(angle=angGear-360,convexity=10)
				translate([outRadius/2+1,0,0]) 
					square(size=[outRadius+1,thick*2+1], center=true);
		cylinder(d=outRadius+2, h=2*thick+1, center=true);
		}
		
		// Furos no corpo:
		posRadius = outRadius-diamHoles/2-9.5;
		delta = 360/numHolesUp;
		for (i=angHolesUp) {
			translate([posRadius*sin(i),posRadius*cos(i),0])
				cylinder(d=diamHoles,h=thick+5, center=true);
		}
		//posRadiusDn = outRadius-diamHoles/2-24.5;
		//for (i=angHolesDn) {
		//	translate([posRadiusDn*sin(i),posRadiusDn*cos(i),0])
		//		cylinder(d=diamHoles,h=thick+5, center=true);
		//}
		
	}
}
//------------------------------------------------

module gear11()
{
	numTeeth = 11;
	ogDiamPitch = 1/modulo;
	radius = (modulo * (numTeeth + 2))/2;

	difference() {
		union() {
			gear(number_of_teeth=numTeeth,
					diametral_pitch=ogDiamPitch,
					gear_thickness=thick,
					rim_thickness=thick,
					hub_thickness=thick,
					bore_diameter=0,
					backlash=backlash,
					clearance=clearance,
					pressure_angle=pressAng);
		}
	}
}
//------------------------------------------------
//------------------------------------------------