// Engrenagens Telescopio Greyka
//
// Reducao Eixo AR (Polar): 138:1
// engrenagem no eixo do pinhao de 140 dentes e diam externo de 48.8mm
//
// Reducao Eixo Declinacao: 88:1
// (tambem sem-fim e coroa)
// 
// Eixos dos pinhoes de 6mm e engrenagem secundaria do eixo polar com montagem conica.
//
// Engrenagens diretamente no motor, 11 dentes, modulo 1mm 
// e motor passo 200 steps/rev e 16 microsteps/step
//     AR :  2.9:1  e 50mm (?) diam
//     DEC: 4.54:1
//
// Diametro do eixo do motor: 5mm
//
// Marcos Ackel 2017-06-06 - marcos.ackel@ceamig.org.br

use <./MCAD/involute_gears.scad>;
// Parametros:
// modulo: 1mm
// Pressure Angle = 20 degrees
// Calc: Outside Diameter = (numTeeth +2) * modulo
// Calc: Root Diameter = outDiameter - 2*(modulo * iif(modulo >=1.25, 2.25, 2.4))
//------------------------------------------------
$fa = 0.5;
$fs = 0.5;

modulo = 1;  // 1 mm modulo
height = 8;  // Gear Width
//outRadius = (modulo * (numTeeth + 2)) / 2;
vertClearance = 0;
clearance = 0.2;
borda = 4;
diamShaft = 10; // diametro do furo central
backlash = 0.1;
pressAng = 20;  // pressure angle

folga = 0.3;    // folga para o eixo
diamFuro = 3/8 * 25.4 / cos(30) + folga;  // peca de latao/bronze: diametro entre faces
diamEixoMotor = 5;
//------------------------------------------------


//gearAR();
gearDEC();
//gearMotor();

module gearAR() {
	// ATENCAO: Usamos a mesma engrenagem de DEC para AR tambem.
	// Entao a relacao de Ticks/rev ficou:
	// Ticks/rev: 200 * 16 * 4.54545 * 138 = 2007270.72
	// Inicialmente projetado:
	// Para ter relacao de 2.9:1, com a engrenagem da caixa com 11 dentes,
	// tem que ter 11*2.9 = 31.9 dentes => 32 dentes (relacao 2.9091:1)
	// Ticks/rev: 200 * 16 * 2.90909 * 138 = 1284654.14
	numTeeth = 32;
	ogDiamPitch = 1/modulo;
	raio = (modulo * (numTeeth + 2))/2;
	echo(str("<b>Diametro externo (AR) = ",raio*2,"mm</b>"));

	difference() {
		// engrenagem:
		gear(number_of_teeth=numTeeth,
					diametral_pitch=ogDiamPitch,
					gear_thickness=height,
					rim_thickness=height,
					hub_thickness=height,
					bore_diameter=0,
					backlash=backlash,
					clearance=clearance,
					pressure_angle=pressAng);
		// furo:
		rotate([0,0,30]) cylinder(h=2*height, d=diamFuro, $fn=6);
		// rebaixo:
		union() {
			difference() {
				translate([0,0,height/2]) cylinder(h=height, d1=24,d2=30);
				translate([0,0,height/2]) cylinder(h=height, d1=18,d2=12);
			}
		}
	}
}

module gearDEC() {
	// Para ter relacao de 4.54:1, com a engrenagem da caixa com 11 dentes,
	// tem que ter 11*4.54 = 49.94 dentes => 50 dentes (relacao 4.54545:1)
	// Ticks/rev: 200 * 16 * 4.54545 * 88 = 1279988.72
	numTeeth = 50;
	ogDiamPitch = 1/modulo;
	raio = (modulo * (numTeeth + 2))/2;
	echo(str("<b>Diametro externo (DEC) = ",raio*2,"mm</b>"));

	difference() {
		// engrenagem:
		gear(number_of_teeth=numTeeth,
					diametral_pitch=ogDiamPitch,
					gear_thickness=height,
					rim_thickness=height,
					hub_thickness=height,
					bore_diameter=0,
					backlash=backlash,
					clearance=clearance,
					pressure_angle=pressAng);
		// furo:
		rotate([0,0,30]) cylinder(h=2*height, d=diamFuro, $fn=6);
		// rebaixo:
		union() {
				difference() {
					translate([0,0,height/2]) cylinder(h=height, d1=40,d2=50);
					translate([0,0,height/2]) cylinder(h=height, d1=18,d2=12);
				}
		}
	}
}


module gearMotor()
{
	nTeeth = 11;
	height = 12;
		
	// outerGear:
	diamPitch = 1/modulo;
	raio = (modulo * (nTeeth + 2))/2;
	echo(str("<b>Diametro externo (Motor) = ",raio*2,"mm</b>"));

	difference() {
		union() {
			gear(number_of_teeth=nTeeth,
					diametral_pitch=diamPitch,
					gear_thickness=height,
					rim_thickness=height,
					hub_thickness=height,
					bore_diameter=diamEixoMotor+0.2,
					backlash=backlash,
					clearance=clearance,
					pressure_angle=pressAng);
			// saia:
			//translate([0,0,0]) cylinder(r=raio,h=2);
		}
	}
}
