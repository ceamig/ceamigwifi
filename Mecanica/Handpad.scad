// Projeto CeamigWiFi
//
// Caixa para o controle (Handpad)
//
// Marcos Ackel 2017-09-01
//

$fa=0.5;
$fs=0.5;

use <./MCAD/boxes.scad>;

// Para Imprimir:
//caixa();
base();
//----------------

// Montagem:
//montagem();

// Parametros:
larg = 60;
comp = 90;
altura  = 26;
esp  = 2.5;
d = 0.1;

df = 7.0;	// diametro furos botoes

//----------------------------------------
alt = altura - esp;
//----------------------------------------

module montagem() {
	caixa();
	translate([0,0,-(esp+d)]) base();
}
//----------------------------------------

module caixa() {
	difference() {
		translate([0,comp/2,0]) roundedBox( [larg,comp,2*alt], 2*esp, false );
		// retira o interior:
		translate([0,comp/2,0]) roundedBox( [larg-2*esp,comp-2*esp,2*(alt-esp)], esp, false );
		// retira metade inferior da caixa:
		translate([0,comp/2,-(alt+2)/2 ]) cube( [larg+d, comp+d, alt+2], center=true );
		// retira o encaixe da borda:
		translate([0,(comp)/2,0]) roundedBox( [larg-2*(esp-0.8),comp-2*(esp-0.8), 2*(esp+2*d)], 1.5*esp, true );
		
		
		// furos para os botoes:
		translate([ 16,70,alt]) cylinder( d=df, h=3*esp, center=true );   // R
		translate([-16,70,alt]) cylinder( d=df, h=3*esp, center=true );  // L
		translate([  0,53,alt]) cylinder( d=df, h=3*esp, center=true );	// N
		translate([ 18,35,alt]) cylinder( d=df, h=3*esp, center=true );	// E
		translate([  0,35,alt]) cylinder( d=df, h=3*esp, center=true );	// C
		translate([-18,35,alt]) cylinder( d=df, h=3*esp, center=true );	// W
		translate([  0,17,alt]) cylinder( d=df, h=3*esp, center=true );	// S
		
		// conector:
		translate([-20,-d,0]) cube([14.5,13.0,10.0]);
		
		// furos parafusos fechamento:
		translate([12,0-d,5]) rotate([-90,0,0]) {
					cylinder(d=3.0, h=6*esp);
					cylinder(d1=5.0,d2=3.0, h=1.5); }

		translate([0,comp+d,5]) rotate([-90,0,180]) {
					cylinder(d=3.0, h=6*esp);
					cylinder(d1=5.0,d2=3.0, h=1.5); }

		// texto:
		translate([0,comp-12,alt-0.55]) linear_extrude(esp) text("CeamigWiFi", 5, font="Liberation Sans:style=Bold", halign="center");
		// B.Riedel:
		//translate([0,comp-12,alt-0.55]) linear_extrude(esp) text("B. Riedel", 5, font="Liberation Sans:style=Bold", halign="center");
					
		// texto NSEW
		translate([0,comp-30,alt-0.55]) linear_extrude(esp) text("N", 4.2, font="Liberation Sans:style=Bold", halign="center");
		translate([0,comp-84,alt-0.55]) linear_extrude(esp) text("S", 4.2, font="Liberation Sans:style=Bold", halign="center");
		translate([0-larg/2+12,comp-48,alt-0.55]) linear_extrude(esp) text("W", 4.2, font="Liberation Sans:style=Bold", halign="center");
		translate([0+larg/2-12,comp-48,alt-0.55]) linear_extrude(esp) text("E", 4.2, font="Liberation Sans:style=Bold", halign="center");
		
		// Para visualizar encaixe tampa:
		//translate([-larg,-5,0]) cube([2*larg,20,2*alt]);
		//translate([-larg,comp-40,0]) cube([2*larg,45,2*alt]);  // transversal
		//translate([-larg/2-25,-5,0]) cube([30,2*comp,2*alt]);   // longitudinal
		//translate([-larg/2-10,-5,0]) cube([50,2*comp,2*alt]);   // longitudinal
		
	}
	// apoio conector:
	translate([-(20+esp),0,esp]) cube([esp,8.0,10.0]);
	translate([-(20+esp),0,10+0*esp]) cube([15+2*esp,8.0,esp]);
	translate([-5+esp,0,10+esp]) rotate([0,-90,0]) triangle(8, 8, 15+2*esp);
	
}
//----------------------------------------

module base()
{
	difference() {
		union() {
			translate([0,comp/2,esp/2]) roundedBox( [larg, comp, esp], 2*esp, true );
			
			// barrinhas comprimento:
			translate([ (larg/2-2*esp+0.6),20/2,esp]) cube([esp,comp-20,esp]);
			translate([-(larg/2-1*esp+0.6),20/2,esp]) cube([esp,comp-20,esp]);
			// barrinhas largura:
			translate([ -(larg-20)/2,comp-(2*esp-0.7),esp]) cube([larg-20,esp,esp]);
			translate([ -(larg-60)/2,     (1*esp-0.7),esp]) cube([larg-40,esp,esp]);

			// cubos para parafusar:
			translate([12,2.2*esp,5]) rotate([90,0,0]) roundedBox([12,10-d,2.0*esp], 1, true);
			translate([0,comp-2.2*esp,5]) rotate([90,0,0]) roundedBox([12,10-d,2.0*esp], 1, true);
			// aba para suporte:
			translate([0, comp-2*d, 0]) triangleEq(larg-6, 160, esp);

		}
		// furos parafusos:
		translate([12,0,5+esp]) rotate([-90,0,0]) cylinder(d=2.0, h=6*esp);
		translate([0,comp-10,5+esp]) rotate([-90,0,0]) cylinder(d=2.0, h=5*esp);
		// furo da aba:
		translate([0,comp+4,-1]) rotate([0,0,0]) cylinder(d=4.0, h=5*esp);
		
				// texto:
		translate([0,0.17*comp,+0.55]) rotate([0,180,0]) linear_extrude(esp) text("CeamigWiFi", 5, font="Liberation Sans:style=Bold", halign="center");
	}
}
//----------------------------------------


/**
 * Standard right-angled triangle
 *
 * @param number o_len Lenght of the opposite side
 * @param number a_len Lenght of the adjacent side
 * @param number depth How wide/deep the triangle is in the 3rd dimension
 * @todo a better way ?
 */
module triangle(o_len, a_len, depth)
{
    linear_extrude(height=depth)
    {
        polygon(points=[[0,0],[a_len,0],[0,o_len]], paths=[[0,1,2]]);
    }
}
//----------------------------------------

/**
 * Triangulo Equilatero
 *
 * @param number o_len Lenght of the opposite side
 * @param number angle Angle oposed of the side (form >0 to 180 degrees)
 * @param number depth How wide/deep the triangle is in the 3rd dimension
 * @todo a better way ?
 */
module triangleEq(o_len, angle, depth)
{
    linear_extrude(height=depth)
    {
		opPt = [0, o_len/tan(angle/2)];
        polygon(points=[[-o_len/2,0],[+o_len/2,0],opPt], paths=[[0,1,2]]);
    }
}
//----------------------------------------
//----------------------------------------
