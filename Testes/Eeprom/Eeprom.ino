 
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <String.h>


typedef struct ConfigHw_st {
	// Dispositivo:	
	char deviceName[25+1];
	// Rede:
	IPAddress netIPAddress; //char netIPAddress[15+1];  //IPAddress netIPAddress;
	char sp1[2];
	IPAddress netSubnetMask; //char netSubnetMask[15+1];  //IPAddress netSubnetMask;
	char sp2[2];
	uint16_t  netPort;

};


template <class T> bool writeToEEPROM(int ee, const T& value);
template <class T> bool readFromEEPROM(int ee, T& value);

template <class T> bool writeToEEPROM(int ee, const T& value)
{
    const byte* p = (const byte*)(const void*)&value;
    unsigned int i;
    Serial.printf("sizeof: %d\n", sizeof(value) );
    for (i = 0; i < sizeof(value); i++) {
          Serial.printf("%02X ", *p);
          EEPROM.write(ee++, *p++);
    }
    Serial.printf("\n");
    //EEPROM.commit();
    if (!EEPROM.commit())
    	Serial.printf("Erro gravacao EEPROM.\n");
    else
    	Serial.printf("Commit ok.\n");
    return (i == sizeof(value));
}
//----------------------------------------------------

template <class T> bool readFromEEPROM(int ee, T& value)
{
    byte* p = (byte*)(void*)&value;
    unsigned int i;
    for (i = 0; i < sizeof(value); i++)
          *p++ = EEPROM.read(ee++);
    return (i == sizeof(value));
}

//----------- Estruturas de configuracao para gravar em EEPROM
// Preferivel usar EEPROM pois nao sobrescreve os dados de configuracao quando grava o SSPI.
const char cfgAssinatura[5] = "0607";       // Maximo: 4 bytes
// Assinatura para garantir que as estruturas de configuracao estao corretas e foram inicializadas.
const unsigned int ADDR_EEPROM_SIGN = 0;
const unsigned int ADDR_EEPROM_CONFIGHW = ADDR_EEPROM_SIGN + sizeof(cfgAssinatura);

// Parametros defaut de configuracao: (podem ser mudados pela pagina de configuracao web)
struct ConfigHw_st  configHw  =  { 
		"CeamigWiFi123456789012345",
		IPAddress(10,0,0,1), //"10.0.0.1", //IPAddress(10,0,0,1),
		"1",
		IPAddress(255,255,255,0), //"255.255.255.0", //IPAddress(255,255,255,0),
		"2",
		4030,
	};


const unsigned int EEPROM_SIZE = sizeof(cfgAssinatura) + sizeof(configHw);

const uint8_t PinBeep = 13;

void tone(int freq, int duracao)
{
	tone( PinBeep, freq, duracao );
	//analogWriteFreq(freq);   // freq PWM
	//analogWrite(PinBeep, 512);
	//delay(duracao);
	//analogWrite(PinBeep, 0);
}


void initHardware()
{
	//pinMode(LED_PIN, OUTPUT);
	pinMode(PinBeep, OUTPUT);
	digitalWrite(PinBeep, LOW);
	pinMode(A0, INPUT);
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)
}





void setup()
{
  initHardware();
  delay(1000);

  Serial.println();

//  Serial.println("Configurando Ponto de Acesso...");
//  configWiFi(&configHw, true);

  EEPROM.begin(EEPROM_SIZE);


//writeToEEPROM(ADDR_EEPROM_SIGN, "0X0X");


  // Verifica assinatura na configuracao da EEPROM e carrega os dados armazenados:
  if (VerificarAssinatura()) {
  	Serial.printf("OK assinatura\n");
    //GravarParametrosHw();  Serial.println("Gravar OK"); // ATENCAO: executar somente para gravar inicialmente os parametros
    // teste:

    // Assinatura Ok. Ler parametros:
    LerParametrosHw();
  } else {
  	Serial.println("Assina NOK");

    // Assinatura incorreta. Gravar parametros default:

    //writeToEEPROM(ADDR_EEPROM_SIGN, "A6A7");
	//writeToEEPROM(ADDR_EEPROM_SIGN, "B6B7");

    Serial.println("Gravar Assinatura...");
	//writeToEEPROM(ADDR_EEPROM_SIGN, "0607");
	//writeToEEPROM(ADDR_EEPROM_SIGN, cfgAssinatura);
    GravarAssinatura();
    
    Serial.println("Gravar Parms...");
    GravarParametrosHw();
    Serial.println("Reler parms...");
    // Le novamente para ter parametros validos na var cfg:
    if (!LerParametrosHw()) {
    	Serial.println("Erro Assinatura EEPROM.");
    	tone(3000,500); // erro!
    } else {
    	Serial.println("Gravacao de Parametros Default na EEPROM OK.");
	    tone(3000, 200);  // 
    }
  }

  

  // LER DA CONFIGURACAO
  Serial.println();
  Serial.println("\n=== Config finalizada: ===");


  //Serial.printf("[0]: %d\n", configHw.netIPAddress[0]);
  //Serial.printf("[1]: %d\n", configHw.netIPAddress[1]);
  //Serial.printf("[2]: %d\n", configHw.netIPAddress[2]);
  //Serial.printf("[3]: %d\n", configHw.netIPAddress[3]);

  Serial.printf("Nome: %s\n", configHw.deviceName);
  
  //Serial.print(configHw.netIPAddress.toString());  // NAO SEI POR QUE NAO FUNCIONA!!! ??
  IPAddress ip = configHw.netIPAddress;
  IPAddress sm = configHw.netSubnetMask;
  //Serial.print(ip.toString() );  //OK!
  Serial.printf("netIPAddress : %s\n", ip.toString().c_str() );
  Serial.printf("netSubnetMask: %s\n", sm.toString().c_str() );
  Serial.printf("Port: %d\n", configHw.netPort );


  tone(3600, 100);
  delay(100);
  tone(3600, 100);
}
//--------------------------------------------------------------


bool GravarParametrosHw()
{
  //for (unsigned int i = 0; i < sizeof(configHw_st); i++)
  //	EEPROM.write(ADDR_EEPROM_CONFIGHW + i, *((char*)&configHw + i));
  return writeToEEPROM(ADDR_EEPROM_CONFIGHW, configHw);
}
// ---------------------------------------------------------------------------

bool LerParametrosHw(void)
{
  //for (unsigned int i = 0; i < sizeof(configHw_st); i++)
  //	*((char*)&configHw + i) = EEPROM.read(ADDR_EEPROM_CONFIGHW + i);
  return readFromEEPROM(ADDR_EEPROM_CONFIGHW, configHw);
}
// ---------------------------------------------------------------------------

boolean VerificarAssinatura()
{
  char tmp[5] = "....";
  readFromEEPROM(ADDR_EEPROM_SIGN, tmp);
  if ( tmp[0] == cfgAssinatura[0] && tmp[1] == cfgAssinatura[1] &&
       tmp[2] == cfgAssinatura[2] && tmp[3] == cfgAssinatura[3] )
    return true;
  else
    return false;
}
//-----------------------------------------------------

boolean GravarAssinatura()
{
  //writeToEEPROM(ADDR_EEPROM_SIGN, cfgAssinatura);
  return writeToEEPROM(ADDR_EEPROM_SIGN, "0607");
}
//-----------------------------------------------------


void loop()
{

  //	if (G_debug != 0) {
  //		Serial.print("Debug: "); Serial.println(G_debug);
  //	}
  delay(100);

  delay(10);
}
//--------------------------------------------------------------
