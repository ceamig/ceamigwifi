#include <ESP8266WiFi.h>

// ATENCAO: GPIO0 (FLASH) -> low -> para upload 
//                           high -> boot normal
//          CH_PD -> high

// definicoes WiFi:
const char WiFiSENHA[] = "ceamig12";   // minimo 8 chars
const char WiFiSSID[]  = "CEAMIG";

// Definicoes Pinos I/O
const int LED_PIN = 5;    // GPIO pino 5

WiFiServer server(4030);

void setup() 
{
	initHardware();
	//setupWiFi();
	//server.begin();
	configWiFi();
}

// serial end ethernet buffer size
#define BUFFER_SIZE 128

#define min(a,b) ((a)<(b)?(a):(b))

WiFiClient cliente;
uint8_t net_buf[BUFFER_SIZE]={0};

void loop() 
{
	size_t bytes_read;

	// Verifica se um cliente conectou
	cliente = server.available();
	if (!cliente) {
		delay(1);
		return;
	}
	
	if (!cliente.available()){
		delay(1);
		return;
	}
	if(cliente.connected()) {
    	// check the network for any bytes to send to the serial
    	int count = cliente.available();
    	if (count > 0) {
	    	Serial.println("");
	        Serial.print("!");
	        delay(1);
			bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
			delay(1);
			
	        Serial.println(bytes_read, DEC);
	        delay(1);
			if (net_buf[0] == ':') {
				ProcessaLX200( net_buf );
				Serial.print("X");
			}	
			delay(10);	
	      	Serial.write(net_buf, bytes_read);
	      	Serial.flush();
		}
		delay(10);
	//} else {
    //	cliente.stop();
  	}
	delay(1);
}

void configWiFi()
{
	delay(1000);
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)
	Serial.println();
	Serial.print("Configurando Ponto de Acesso...");
	/* You can remove the password parameter if you want the AP to be open. */
	WiFi.softAP(WiFiSSID);

	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	server.begin();
	Serial.println("Server iniciado: Porta 4030");
}

void setupWiFi()
{
	WiFi.mode(WIFI_AP);
	
	// Para tentar conseguir um numero unico: anexar os dois
	// ultimos bytes do MAC addres (em HEXA):
	//uint8_t mac[WL_MAC_ADDR_LENGTH];
	//WiFi.softAPmacAddress(mac);
	//String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
	//               String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
	//macID.toUpperCase();
	//String AP_NameString = "CEAMIG-WIFI " + macID;
	//String AP_NameString = "CEAMIG WIFI 1234";	
	String AP_NameString = "SkyFi-1234567890";  // minimo 16 chars (?)
	
	char AP_NameChar[AP_NameString.length() + 1];
	memset(AP_NameChar, AP_NameString.length() + 1, 0);
	
	for (int i=0; i<AP_NameString.length(); i++)
		AP_NameChar[i] = AP_NameString.charAt(i);
	
	WiFi.softAP(AP_NameChar);//, WiFiSENHA);
}

void initHardware()
{
	pinMode(LED_PIN, OUTPUT);
	digitalWrite(LED_PIN, LOW);
	Serial.begin(115200);
	delay(200);
}


void ProcessaLX200( uint8_t *net_buf ) {
	if (memcmp( net_buf+1, "St", 2)==0 ) {
		Serial.print("L");
		delay(1);
		cliente.print('1');
	}
	if (memcmp( net_buf+1, "Sg", 2)==0 ) {
		Serial.print("O");
		delay(1);
		cliente.print('1');
	}
	if (memcmp( net_buf+1, "GR#", 3)==0 ) {
		//Serial.print("G");
		delay(1);
		cliente.print("06:30:00#");
	}
	if (memcmp( net_buf+1, "GD#", 3)==0 ) {
		Serial.print("D");
		delay(1);
		cliente.print("-15*30'00#");
	}
	if (memcmp( net_buf+1, "RS#", 3)==0 ) {
		Serial.print("R");
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "SG", 2)==0 ) {
		Serial.print("T");
		delay(1);
		cliente.print('1');
	}
	if (memcmp( net_buf+1, "SL", 2)==0 ) {
		Serial.print("t");
		delay(1);
		cliente.print('1');
	}
	if (memcmp( net_buf+1, "SC", 2)==0 ) {
		Serial.print("t");
		delay(10);
		//cliente.print('1');
		//delay(50);
		cliente.print("1Updating Planetary Data#                              #");
		//cliente.print("                              #");
	}
	if (memcmp( net_buf+1, "Sw", 2)==0 ) {
		cliente.print('1');
	}
	if (memcmp( net_buf+1, "Me#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Mn#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Ms#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Mw#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Qe#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Qn#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Qs#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Qw#", 3)==0 ) {
		//cliente.print('1');
	}
	if (memcmp( net_buf+1, "Q#", 2)==0 ) {
		//cliente.print('1');
	}
}

