

#include <ESP8266WiFi.h>
#include "Wire.h"

const uint8_t SDA_PIN = 2;
const uint8_t SCL_PIN = 4;

// Botoes (I2C, PCF8574a em 0x20)
const uint8_t KEYPAD_ADDR = 0x38;

void setup()
{
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)

	Wire.begin( SDA_PIN, SCL_PIN );
	Wire.beginTransmission(KEYPAD_ADDR); // transmit to device (0x38)
                              // device address is specified in datasheet
	Wire.write( 0 );             // sends value byte  
	Wire.endTransmission();     // stop transmitting

	Serial.println("");
	Serial.println("Wire inicializado...");
}

unsigned long last = -1;
byte botoes = 0;
byte botoesOld = 0;

void loop()
{
	
	unsigned long agora = millis();

	if (agora - last > 30 ) {

		Wire.requestFrom(KEYPAD_ADDR, (uint8_t) 1);  // get one byte

		byte nAv = Wire.available();
		if ( nAv >= 1) {
			botoes = Wire.read();
			if ( botoes != botoesOld ) {
				Serial.println(botoes, BIN);
				botoesOld = botoes;
			}
		}
		last = agora;
	}

}

