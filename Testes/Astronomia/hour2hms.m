function aHms = hour2hms( hour )

hh = fix( hour );
m = abs(hour - hh) * 60;
mm = fix( m );
ss = (m - mm) * 60;

aHms = [hh, mm, ss];

end