
function LMST = CalcTempoSideral( dateNum, long, timeZone )
%OK
% Exemplo: hourToString( CalcTempoSideral( datenum([2017,03,18,14,00,00]), 43.94567, -3) )
% Resultado: 01:49:59.1
% Belo Horizonte: timeZone = -3 h

% http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?bibcode=1983IAPPP..13...16F&db_key=AST&page_ind=0&plate_select=NO&data_type=GIF&type=SCREEN_GIF&classic=YES
% http://aa.usno.navy.mil/faq/docs/GAST.php

% outra ref: http://www2.arnes.si/~gljsentvid10/sidereal.htm

dateVec = datevec(dateNum);
year = dateVec(1);
month = dateVec(2);
day = dateVec(3);
hora = dateVec(4);
min = dateVec(5);
seg = dateVec(6);

% As 0:00:00 Greenwich:
JDN0 = 367*year - floor( 7*(year + floor((month+9)/12))/4 ) + floor(275*month/9) + day + 1721013.5;

dfrac  = (hora + min/60 + seg/3600)/24;

JDN = JDN0 + dfrac;
LJDN = JDN - timeZone/24;

% from 2000 Jan 1, 12h UT (Julian Date 245545.0):
D = LJDN - 2451545.0;

GMST = 18.697374558 + 24.06570982441908 * D;
LGMST = GMST - long/15;  % logitude in hours (360/24 = 15h)

LMST = mod(LGMST, 24);

end