% vdmsToString:

function strDegrees = vdmsToString( vecdms, secPrecision )

dd = vecdms(1);
mm = vecdms(2);
ss = vecdms(3);
if nargin < 2
    secPrecision = 2;  % two decimal places
end
cSecPrecision1 = num2str(floor(secPrecision+3));   % 2 digits for the seconds plus decimal point
cSecPrecision2 = num2str(floor(secPrecision));
strDegrees = sprintf( ['%02d� %02d'' %0' cSecPrecision1 '.' cSecPrecision2 'f"'], dd, mm, ss);

end
