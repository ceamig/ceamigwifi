% hourToString:

function strHour = hourToString( hour, secPrecision )

hh = floor( hour );
m = (hour - hh) * 60;
mm = floor( m );
ss = (m - mm) * 60;
if nargin < 2
    secPrecision = 2;  % two decimal places
end
cSecPrecision1 = num2str(floor(secPrecision+3));   % 2 digits for the seconds plus decimal point
cSecPrecision2 = num2str(floor(secPrecision));
strHour = sprintf( ['%02d:%02d:%0' cSecPrecision1 '.' cSecPrecision2 'f'], hh, mm, ss);

end