function degrees = dms2degree( dd, mm, ss )

degrees = abs(dd) + (mm/60) + (ss/3600);

if dd < 0
    degrees = -degrees;
end

end