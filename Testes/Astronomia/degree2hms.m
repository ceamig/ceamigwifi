function aHms = degree2hms( angleInDegrees )

h = angleInDegrees / 15;
hh = floor( h );
m = (h - hh) * 60;
mm = floor( m );
ss = (m - mm) * 60;

aHms = [hh mm ss];

end