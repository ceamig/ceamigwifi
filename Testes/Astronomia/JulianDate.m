function JDN = JulianDate(year, month, day, hora, min, seg)

% Julian Date Number at 0h UT
% Veja: 
% http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?bibcode=1983IAPPP..13...16F&db_key=AST&page_ind=0&plate_select=NO&data_type=GIF&type=SCREEN_GIF&classic=YES

% As 0:00:00 Greenwich:
JDN0 = 367*year - floor( 7*(year + floor((month+9)/12))/4 ) + floor(275*month/9) + day + 1721013.5;

% Em Greewich:
JDN = JDN0 + (hora + min/60 + seg/3600)/24;

end