function LJDN = LocalJulianDate( dateNum, timeZone )
%OK

% Belo Horizonte: timezone = -3

% Julian Date Number at 0h UT
% Veja: 
% http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?bibcode=1983IAPPP..13...16F&db_key=AST&page_ind=0&plate_select=NO&data_type=GIF&type=SCREEN_GIF&classic=YES

dateVec = datevec(dateNum);
year = dateVec(1);
month = dateVec(2);
day = dateVec(3);
hora = dateVec(4);
min = dateVec(5);
seg = dateVec(6);

% As 0:00:00 Greenwich:
JDN0 = 367*year - floor( 7*(year + floor((month+9)/12))/4 ) + floor(275*month/9) + day + 1721013.5;

JDN = JDN0 + (hora + min/60 + seg/3600)/24;

LJDN = JDN - timeZone/24;

end