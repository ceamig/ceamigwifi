function aHorizontal = Equatorial2Horizontal(ra, dec, lat, lst)
% ra and lst in hours
% dec and lat in degrees
% http://star-www.st-and.ac.uk/~fv/webnotes/chapter7.htm

% Exemplo: Equatorial2Horizontal(hms2hour(5,17,57.07), dms2degree(46,00,48.5), -19.93975, hms2hour(1,49,59))
% Resultado:  33.644888028483159   9.011744393978592
% Exemplo: Equatorial2Horizontal(hms2hour(6,24,19.87), dms2degree(-52,42,27.9), -19.93975, hms2hour(1,49,59))
% Resultado:    140.0089356221671   28.6386800446765


% Local Hour Angle
H = (lst - ra) * 15;   % (lst - ra) is in hours. Need it in degrees, not in hours

sinAlt = sind(dec) * sind(lat) + cosd(dec) * cosd(lat) * cosd(H);

alt = asind( sinAlt );

% AZimuth:
sinAz =  -sind(H) * cosd(dec) / cosd( alt );
cosAz = ( sind(dec) - sind(lat) * sinAlt ) / ( cosd(lat) * cosd(alt) );

azim = asind( sinAz );
if cosAz < 0
    azim = 180 - azim;
end

aHorizontal = [azim, alt];

end