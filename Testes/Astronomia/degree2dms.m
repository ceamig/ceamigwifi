function aDms = degree2dms( angleInDegrees )

dd = fix( angleInDegrees );
m = abs(angleInDegrees - dd) * 60;
mm = fix( m );
ss = (m - mm) * 60;

aDms = [dd mm ss];

end