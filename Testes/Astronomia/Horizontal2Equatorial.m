function aEquatorial = Horizontal2Equatorial(alt, azm, lat, lst)
% ra and lst in hours
% dec and lat in degrees
% http://star-www.st-and.ac.uk/~fv/webnotes/chapter7.htm

% Exemplo: e = Horizontal2Equatorial(dms2degree(28,38,19.25), dms2degree(140,00,31.5), -19.93975, hms2hour(1,49,59.912664585279458))
% Resultado:  6.405521485905293 -52.707587515950706


sinDec = sind(alt) * sind(lat) + cosd(alt) * cosd(lat) * cosd(azm);

dec = asind( sinDec );

cosDec = cosd( dec );

sinH   = -sind(azm) * cosd(alt) / cosDec;
cosH   = ( sind(alt) - sinDec * sind(lat) ) / ( cosDec * cosd(lat) );

H = asind(sinH);
if cosH < 0
    H = 180 - H;
end    

ra = lst - H/15;

aEquatorial = [ra, dec];

end