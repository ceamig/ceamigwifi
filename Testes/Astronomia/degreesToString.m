% degreesToString:

function strDegrees = degreesToString( degrees, secPrecision )

dd = floor( degrees );
m = (degrees - dd) * 60;
mm = floor( m );
ss = (m - mm) * 60;
if nargin < 2
    secPrecision = 2;  % two decimal places
end
cSecPrecision1 = num2str(floor(secPrecision+3));   % 2 digits for the seconds plus decimal point
cSecPrecision2 = num2str(floor(secPrecision));
strDegrees = sprintf( ['%02d� %02d'' %0' cSecPrecision1 '.' cSecPrecision2 'f"'], dd, mm, ss);

end