// compilado com VS 2015 community edition

// PARA DEBUG DAS FUNCOES


// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "stdio.h"
#include <string>
#include "stdlib.h"

#define String string
#define byte unsigned char

//------------------
class dms
{
private:
	int _dd;
	int _mm;
	double _ss;
public:
	dms() {};;
	dms(int dd, int mm, double ss) { _dd = dd; _mm = mm; _ss = ss; };
	dms(double degree);
	int degree(void) { return _dd; };
	int minute(void) { return _mm; };
	double second(void) { return _ss; }
	double toDegree(void) {  return (_dd>=0)? (_dd+_mm/60.0+_ss/3600.0) : (_dd-_mm/60.0-_ss/3600.0); };
	std::string toString(byte secPrecision = 1);
};

class hms
{
private:
	int _hh;
	int _mm;
	double _ss;
public:
	hms();
	hms(int hh, int mm, double ss) { _hh = hh; _mm = mm; _ss = ss; };
	hms(double hour);
	int hour(void) { return _hh; };
	int minute(void) { return _mm; };
	double second(void) { return _ss; }
	double toHour(void) { return _hh + _mm / 60.0 + _ss / 3600.0; };
	std::string toString(byte secPrecision = 1);
};

class Coord
{
private:
	double _x;
	double _y;
public:
	Coord(double x, double y) { _x = x; _y = y; };
	double getX(void) { return _x; };
	double getY(void) { return _y; };
	void setX(double x) { _x = x; };
	void setY(double y) { _y = y; };
};

class CoordEquatorial;

class CoordAltAz
{
private:
	double _alt;
	double _az;
public:
	CoordAltAz() { _alt = 0; _az = 0; };
	CoordAltAz(double alt, double az) { _alt = alt; _az = az; };
	CoordEquatorial toEquatorial(double latitude, double localSiderealTime);
	double getAlt(void) { return _alt; };
	double getAz(void) { return _az; };
	std::string toString(byte secPrecision = 1);
};


class CoordEquatorial
{
private:
	double _ra;
	double _dec;
public:
	CoordEquatorial() { _ra = 0.0; _dec = 0.0; };
	CoordEquatorial(double ra, double dec) { _ra = ra; _dec = dec; };
	CoordAltAz toHorizontal(double latitude, double localSiderealTime);
	bool setRA(double ra);
	bool setRA(short hh, short mm, double ss);
	bool setDec(double dec);
	bool setDec(short dd, short mm, double ss);
	double getRA(void) { return _ra; };
	double getDec(void) { return _dec; };
	std::string decToString(void);   // formato: sDD*MM'SS#  (para LX200)
	std::string raToString(void);    // formato: HH:MM:SS@   (para LX200)
};
//--------------------------


#include <math.h>

dms::dms(double degree)
{
	_dd = (int)degree;
	double m = abs(degree - _dd) * 60.0;
	_mm = (int)m;
	_ss = (m - _mm) * 60.0;
}

std::string dms::toString(byte secPrecision)
{
	char buf[20] = { 0 };
	byte secPrecision1 = secPrecision + 3;  // 2 digitos para os segundos mais o ponto decimal
	sprintf(buf, "%02d %02d'%0*.*f\"", _dd, _mm, secPrecision1, secPrecision, _ss);
	std::string ret(buf);
	return ret;
}

hms::hms(double hour)
{
	_hh = (int)hour;
	double m = (hour - _hh) * 60.0;
	_mm = (int)m;
	_ss = (m - _mm) * 60.0;

}

std::string hms::toString(byte secPrecision)
{
	char buf[20] = { 0 };
	byte secPrecision1 = secPrecision + 3;  // 2 digitos para os segundos mais o ponto decimal
	sprintf(buf, "%02d:%02d:%0*.*f", _hh, _mm, secPrecision1, secPrecision, _ss);
	std::string ret(buf);
	return ret;
}

CoordEquatorial CoordAltAz::toEquatorial(double latitude, double localSiderealTime)
{
	// ra and lst in hours
	// dec and lat in degrees
	// http://star-www.st-and.ac.uk/~fv/webnotes/chapter7.htm

	double toRad = 3.14159265358979 / 180.0;   // constante PI / 180	
	double latRad = latitude * toRad;
	double altRad = _alt * toRad;
	double azmRad = _az  * toRad;
	double cosLat = cos(latRad);
	double sinLat = sin(latRad);
	double sinDec = sin(altRad) * sinLat + cos(altRad) * cosLat * cos(azmRad);
	sinDec = (sinDec > 1.0)? 1.0 : sinDec;
	sinDec = (sinDec < -1.0)? - 1.0 : sinDec;
	double decRad = asin(sinDec);
	double dec = decRad / toRad;
	double cosDec = cos(decRad);
	double sinH = -sin(azmRad) * cos(altRad) / cosDec;
	double cosH = (sin(altRad) - sin(decRad) * sinLat) / (cosDec * cosLat);
	sinH = (sinH > 1.0) ? 1.0 : sinH;
	sinH = (sinH < -1.0) ? -1.0 : sinH;
	double HRad = asin(sinH);
	double H = HRad / toRad;  
	if (cosH < 0)
		H = 180.0 - H;

	double ra = (localSiderealTime - H/15.0);  // 15.0:  to convert degrees in hours.

	return CoordEquatorial(ra, dec);
}

std::string CoordAltAz::toString(byte secPrecision)
{
	// formato: HH:MM:SS@   (para LX200)
	//char buf[20] = { 0 };
	//hms ra(_ra);
	//sprintf(buf, "%02d:%02d:%02.0f#", ra.hour(), ra.minute(), ra.second());
	//std::string ret(buf);
	std::string ret("TESTE");
	return ret;
}



CoordAltAz CoordEquatorial::toHorizontal(double latitude, double localSiderealTime)
{
	// ra and lst in hours
	// dec and lat in degrees
	// http://star-www.st-and.ac.uk/~fv/webnotes/chapter7.htm

	// Local Hour Angle
	double H = (localSiderealTime - _ra) * 15;   // (lst - ra) is in hours. Need it in degrees, not in hours

	double toRad = 3.14159265358979 / 180.0;   // constante PI / 180

	double HRad = H * toRad;  // em radianos
	double latRad = latitude * toRad;
	double decRad = _dec * toRad;

	double sinAlt = sin(decRad) * sin(latRad) + cos(decRad) * cos(latRad) * cos(HRad);

	double altRad = asin(sinAlt);
	double alt = altRad / toRad;

	// AZimuth:
	double sinAz = -sin(HRad) * cos(decRad) / cos(altRad);
	double cosAz = (sin(decRad) - sin(latRad) * sinAlt) / (cos(latRad) * cos(altRad));

	double azimRad = asin(sinAz);
	double azim = azimRad / toRad;
	if (cosAz < 0)
		azim = 180 - azim;

	return CoordAltAz(alt, azim);
}

std::string CoordEquatorial::decToString(void)
{
	// formato: sDD*MM'SS#  (para LX200)
	char buf[20] = { 0 };
	dms dec(_dec);
	sprintf(buf, "%+02d*%02d'%02.0f#", dec.degree(), dec.minute(), dec.second());
	std::string ret(buf);
	return ret;
}
std::string CoordEquatorial::raToString(void)
{
	// formato: HH:MM:SS@   (para LX200)
	char buf[20] = { 0 };
	hms ra(_ra);
	sprintf(buf, "%02d:%02d:%02.0f#", ra.hour(), ra.minute(), ra.second());
	std::string ret(buf);
	return ret;
}
bool CoordEquatorial::setRA(double ra)
{
	bool ret = false;
	if (ra >= 0.0 && ra <= 24.0) {
		_ra = ra;
		ret = true;
	}
	return ret;
}
bool CoordEquatorial::setRA(short hh, short mm, double ss)
{
	bool ret = false;
	if (hh >= 0 && hh <= 24 && mm >= 0 && mm <= 59 && ss >= 0 && ss < 60.0) {
		_ra = hh*15.0 + mm / 60.0 + ss / 3600.0;
		ret = true;
	}
	return ret;
}
bool CoordEquatorial::setDec(double dec)
{
	bool ret = false;
	if (dec >= -90 && dec <= +90) {
		_dec = dec;
		ret = true;
	}
	return ret;
}
bool CoordEquatorial::setDec(short dd, short mm, double ss)
{
	bool ret = false;
	if (dd >= -90 && dd <= +90 && mm >= 0 && mm < 60 && ss >= 0 && ss < 60.0) {
		_dec = dd + mm / 60.0 + ss / 3600.0;
		ret = true;
	}
	return ret;
}

//----------------------------------


#include <time.h>
//----------------------------------

double CalcSiderealTime(time_t dateTime, double longitude, int timeZone)
{
	// Belo Horizonte: timeZone = -3 h

	// http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?bibcode=1983IAPPP..13...16F&db_key=AST&page_ind=0&plate_select=NO&data_type=GIF&type=SCREEN_GIF&classic=YES
	// http://aa.usno.navy.mil/faq/docs/GAST.php

	// outra ref: http://www2.arnes.si/~gljsentvid10/sidereal.htm
	struct tm * dt;
	dt = localtime(&dateTime);
	int ano = dt->tm_year + 1900; // .year; // year(dateTime);
	int mes = dt->tm_mon + 1;
	int dia = dt->tm_mday;
	int hora = dt->tm_hour;
	int min = dt->tm_min;
	int seg = dt->tm_sec;

	// Greenwich 00:00:
	double JDN0 = 367 * ano - (7 * (ano + ((mes + 9) / 12)) / 4) + (275 * mes / 9) + dia + 1721013.5;
	double dfrac = (hora + min / 60.0 + seg / 3600.0) / 24.0;

	double JDN = JDN0 + dfrac;
	double LJDN = JDN - timeZone / 24.0;

	// desde 2000, jan1, 12h UT (Julian Date 2451545.0):
	double D = LJDN - 2451545.0;

	double GMST = 18.697374558 + 24.06570982441908 * D;

	double LGMST = GMST - longitude / 15.0;  // logitude in hours (360/24 = 15h)

	double LMST = fmod(LGMST, 24);

	//return (SiderealTime_t) LMST;
	return LMST;

}
//-------------------------




int main()
{
	printf("Marcos\n");
	hms h1(6, 24, 19.87);
	dms d1(-52, 42, 27.9);
	double dd1 = d1.toDegree();
	CoordEquatorial canopus(h1.toHour(), d1.toDegree());

	double latitude = -19.939750;
	double longitude = 43.945670;

	printf(" -- Montagem coordenadas equatoriais:\n");
	double ra  = canopus.getRA();
	double dec = canopus.getDec();
	std::string sra = canopus.raToString();
	std::string sde = canopus.decToString();

	printf(" Canopus       : %s  %s\n", sra.c_str(), sde.c_str());
	printf(" Valor esperado: 06:24:20#  -52*42:28#\n\n");

	printf(" -- Calculdo de Tempo Sideral:\n");
	struct tm dt;
	dt.tm_year = 2017 - 1900;
	dt.tm_mon = 3 - 1;  // marco
	dt.tm_mday = 18;
	dt.tm_hour = 14;
	dt.tm_min = 0;
	dt.tm_sec = 0;
	time_t t = mktime(&dt);

	double st = CalcSiderealTime(t, longitude, -3);
	hms nst(st);

	printf(" Sideral Time  : %.16f %s\n", st, nst.toString().c_str());
	printf(" Valor esperado: 1.8333090734959114\n\n");

	dt.tm_year = 2017 - 1900;
	dt.tm_mon = 3 - 1;  // marco
	dt.tm_mday = 25;
	dt.tm_hour = 21;
	dt.tm_min = 45;
	dt.tm_sec = 0;
	time_t t2 = mktime(&dt);

	double st2 = CalcSiderealTime(t2, longitude, -3);
	hms nst2(st2);

	printf(" Sideral Time2 : %.16f %s\n", st2, nst2.toString().c_str());
	//printf(" Valor esperado: 1.8333090734959114\n\n");



	printf(" -- Conversao de coordenada AltAz para Equatorial:\n");
	CoordAltAz canopusH = canopus.toHorizontal(latitude, st);


	double alt = canopusH.getAlt();
	double azm = canopusH.getAz();

	dms aAlt(alt);
	dms aAzm(azm);
	std::string salt = aAlt.toString();
	std::string sazm = aAzm.toString();


	printf(" Coordenadas Horizontais: %.13f  %.13f\n", azm, alt);
	printf("          Valor esperado: 140.0089362025309  28.6386867521183\n\n");
	printf(" Coordenadas Horizontais: %s  %s\n\n", sazm.c_str(), salt.c_str() );

	//printf("CanopusInteiros Parsed (%d): %d  %d  %d\n\n", n, parms[0], parms[1], parms[2]);

	printf(" -- Conversao de coordenata Equatorial para AltAz:\n");
	CoordAltAz coord(alt, azm);

	CoordEquatorial equat = coord.toEquatorial(latitude, st);
	double nra = equat.getRA();
	double nde = equat.getDec();
	printf(" Coordenas Equatoriais: %.15f  %.15f\n", nra, nde);
	printf("     Valores esperados: 6.405521485905293  -52.707587515950706\n");

	hms aRa(nra);
	dms aDe(nde);
	std::string sra2 = aRa.toString();
	std::string sde2 = aDe.toString();
	printf(" Coordenadas Equatoriais: %s  %s\n\n", sra2.c_str(), sde2.c_str());

	CoordAltAz coord2(18.07, 180);
	CoordEquatorial eq = coord2.toEquatorial(-18.07, st);
	printf(" Coordenadas Equatoriais: %.10f  %.10f\n\n", eq.getRA(), eq.getDec());


	getchar();
	return 0;
}

