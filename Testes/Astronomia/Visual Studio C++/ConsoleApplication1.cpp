// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

char * strpbrk2(const char* s1, const char* s2) //register const char *s1, *s2;
{
	register const char *scanp;
	register int c, sc;

	while ((c = *s1++) != 0) {
		for (scanp = s2; (sc = *scanp++) != 0;)
			if (sc == c)
				return ((char *)(s1 - 1));
	}
	return (NULL);
}


int parseInt(char* cmd, int numInts, char* cSeparators, int intsParsed[])
{
	//char buf[50] = { 0 };
	//strcpy(buf, cmd); // para poder modificar o cmd

	char* ptr;
	int nIntsParsed = 0;
	char tmpChr;
	char* iniPtr = cmd;
	while ((ptr = strpbrk2(iniPtr, cSeparators)) != NULL && nIntsParsed < numInts) {
		tmpChr = *ptr;
		*ptr = '\0';
		intsParsed[nIntsParsed] = atoi(iniPtr);
		*ptr = tmpChr;
		iniPtr = ptr + 1;
		nIntsParsed++;
	}
	return nIntsParsed;
}


int main()
{
	printf("Marcos\n");
	char cmd[] = ":Sr-26*27:28#";
	int parms[3] = { 0 };
	int n = parseInt(cmd+3, 3, "*:#", parms);

	printf("Inteiros Parsed (%d): %d  %d  %d\n\n", n, parms[0], parms[1], parms[2]);

	char cmd2[] = ":SG-03.0#";
	n = parseInt(cmd2 + 3, 3, ".#", parms);
	printf("Inteiros Parsed (%d): %d  %d  \n\n", n, parms[0], parms[1]);

		
	getchar();
    return 0;
}

