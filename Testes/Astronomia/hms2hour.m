function [hour] = hms2hour( hh, mm, ss )

hour = hh + mm/60 + ss/3600;

end