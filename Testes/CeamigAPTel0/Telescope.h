
#ifndef TELESCOPE_H
#define TELESCOPE_H

#include <AccelStepper.h>

extern AccelStepper MotorRA;
extern AccelStepper MotorDEC;

// Definicoes Pinos I/O
const int LED_PIN = 5;    // GPIO pino 5
const int MotorRA_PinSTP = 13;    // Motor de RA
const int MotorRA_PinDIR = 12;
const int MotorRA_PinSP1 = 14;
const int MotorRA_PinSP2 = 16;
const int MotorDEC_PinSTP =  0;    //15 Motor de DEC
const int MotorDEC_PinDIR =  4;  //0 2;
const int MotorDEC_PinSP1 =  2;
const int MotorDEC_PinSP2 =  5;

typedef enum { GUIDE, CENTER, FIND, SLEW } TelSpeed;
typedef enum { MOTORRA, MOTORDEC } TelMotor;

// Max Velocidade do Motor: em steps/segundo
extern float MotorMaxSpeed;
// Max Acceleracao Motor: em steps/segundo/segundo
extern float MotorMaxAcceleration;

bool ProcessaLX200( char *net_buf );

#endif
