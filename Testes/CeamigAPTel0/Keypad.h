

#ifndef KEYPAD_H
#define KEYPAD_H

#include <OneButtonAnalog.h>

extern OneButtonAnalog btN;  // Norte
extern OneButtonAnalog btS;  // Sul
extern OneButtonAnalog btW;  // Oeste (West)
extern OneButtonAnalog btE;  // Leste (East)
extern OneButtonAnalog btC;  // Centro
extern OneButtonAnalog btL;  // Esquerda (Left)
extern OneButtonAnalog btR;  // Direita  (Right)

void InitKeypad(void);
void processaKeypad();
void ticksBotoes(int analogVal);

#endif
