// ATENCAO: GPIO0 (FLASH) -> low -> para upload 
//                           high -> boot normal
//          CH_PD -> high

#include <ESP8266WiFi.h>
#include <OneButtonAnalog.h>
#include <String.h>
#include "Telescope.h"
#include "Keypad.h"


// definicoes WiFi:
const char WiFiSENHA[] = "ceamig12";   // minimo 8 chars
const char WiFiSSID[]  = "CEAMIG";



WiFiServer server(4030);




void setup() 
{
	initHardware();
	configWiFi();

	InitKeypad();
	InitTelescope();
	
	//InitMotors();
	//user_init();  // interrupcao
	
	//setupWiFi();
	//server.begin();
}


void delayRun( byte msec )
{
	delay( msec );
}

// serial end ethernet buffer size
#define BUFFER_SIZE 128


#define min(a,b) ((a)<(b)?(a):(b))

WiFiClient cliente;

// loop -----------------------
void loop() 
{
	size_t bytes_read;
  	uint8_t net_buf[BUFFER_SIZE]={0};

	processaKeypad();
	
	// Verifica se um cliente conectou
	cliente = server.available();
	if (!cliente) {
		return;
	}
	cliente.setNoDelay(true);
	while(!cliente.available()){
		processaKeypad();
		delay(1);
	}
	if(cliente.connected()) {
    	// check the network for any bytes to send to the serial
    	int count = cliente.available();
//   	Serial.println("");
//	    Serial.print("!");
//	    Serial.print(count, DEC);
    	if (count > 0) {
//	        Serial.print("~");
			bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
//	        Serial.print(bytes_read, DEC); Serial.print(" ");
//	      	Serial.write(net_buf, bytes_read); Serial.println("");
	      	for ( byte idx = 0; idx < bytes_read; idx++) {
				if (net_buf[idx] == ':') {
//					Serial.print(" x");
					bool cmdOk = ProcessaLX200( (char*) (net_buf+idx) );
					if (!cmdOk) {
						// comando desconhecido. Print:
						Serial.write(net_buf, bytes_read);
						Serial.println("");
					}
					// vai ate' a '#' (final de comando LX200):
					while (net_buf[idx] != '#' && idx < bytes_read)
						idx++;
				}
			}	
			delay(10);
	      	//Serial.flush();
		}
  	}

}

void configWiFi()
{
	delay(2000);
	Serial.println();
	Serial.print("Configurando Ponto de Acesso...");
	/* You can remove the password parameter if you want the AP to be open. */
	WiFi.softAP(WiFiSSID);

	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	server.begin();
	server.setNoDelay(true);
	Serial.println("Server iniciado: Porta 4030");
}

void setupWiFi()
{
	// Para tentar conseguir um numero unico: anexar os dois
	// ultimos bytes do MAC addres (em HEXA):
	//uint8_t mac[WL_MAC_ADDR_LENGTH];
	//WiFi.softAPmacAddress(mac);
	//String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
	//               String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
	//macID.toUpperCase();
	//String AP_NameString = "CEAMIG-WIFI " + macID;
	//String AP_NameString = "CEAMIG WIFI 1234";	
	//String AP_NameString = "SkyFi-1234567890";  // minimo 16 chars (?)
	
	//char AP_NameChar[AP_NameString.length() + 1];
	//memset(AP_NameChar, AP_NameString.length() + 1, 0);
	
	//for (int i=0; i<AP_NameString.length(); i++)
	//	AP_NameChar[i] = AP_NameString.charAt(i);
	
	//WiFi.softAP(AP_NameChar);//, WiFiSENHA);
}

void initHardware()
{
	//pinMode(LED_PIN, OUTPUT);
	pinMode(MotorRA_PinSTP, OUTPUT);
	pinMode(MotorRA_PinDIR, OUTPUT);
	pinMode(MotorRA_PinSP1, OUTPUT);
	pinMode(MotorRA_PinSP2, OUTPUT);
	pinMode(MotorDEC_PinSTP, OUTPUT);
	pinMode(MotorDEC_PinDIR, OUTPUT);
	pinMode(MotorDEC_PinSP1, OUTPUT);
	pinMode(MotorDEC_PinSP2, OUTPUT);
	pinMode(A0, INPUT);
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)
}



