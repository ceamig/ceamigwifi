// Keypad.ino

#include "Keypad.h"
#include "Telescope.h"

// Botoes:
OneButtonAnalog btN(135, 165, true);  // Norte
OneButtonAnalog btS(600, 640, true);  // Sul
OneButtonAnalog btW(260, 295, true);  // Oeste (West)
OneButtonAnalog btE(540, 590, true);  // Leste (East)
OneButtonAnalog btC( 15,  35, true);  // Centro
OneButtonAnalog btL( 65,  85, true);  // Esquerda (Left)
OneButtonAnalog btR( 35,  55, true);  // Direita  (Right)

const int defClickTicks = 50;
const int defPressTicks = 100;

void InitKeypad(void)
{
	OneButtonAnalog::setDefaultClickTicks(50);   // isso efetivamente desabilita a detecao de double-click
	OneButtonAnalog::setDefaultPressTicks(60);
	btN.setClickTicks( defClickTicks ); btN.setPressTicks( defPressTicks );
	btS.setClickTicks( defClickTicks ); btS.setPressTicks( defPressTicks );
	btW.setClickTicks( defClickTicks ); btW.setPressTicks( defPressTicks );
	btE.setClickTicks( defClickTicks ); btE.setPressTicks( defPressTicks );
	btC.setClickTicks( defClickTicks ); btC.setPressTicks( defPressTicks );
	btL.setClickTicks( defClickTicks ); btL.setPressTicks( defPressTicks );
	btR.setClickTicks( defClickTicks ); btR.setPressTicks( defPressTicks );

	btN.attachLongPressStart(btN_ClickProc);
	btS.attachLongPressStart(btS_ClickProc);
	btW.attachLongPressStart(btW_ClickProc);
	btE.attachLongPressStart(btE_ClickProc);
	btC.attachLongPressStart(btC_ClickProc);
	btL.attachLongPressStart(btL_ClickProc);
	btR.attachLongPressStart(btR_ClickProc);
	btN.attachLongPressStop(btN_ClickProc);
	btS.attachLongPressStop(btS_ClickProc);
	btW.attachLongPressStop(btW_ClickProc);
	btE.attachLongPressStop(btE_ClickProc);
	btC.attachLongPressStop(btC_ClickProc);
	btL.attachLongPressStop(btL_ClickProc);
	btR.attachLongPressStop(btR_ClickProc);
	//btCtrl.attachDoubleClick(btCtrlDoubleClickProc);   // alternativo para iniciar os fases
	//btCtrl.attachDuringLongPress(btCtrlDuringProc);	
}

unsigned long lastRead=0;
unsigned long lastReadPrt=0;

void processaKeypad(void)
{
	unsigned long now = millis();
	if ( now - lastRead > 5 ) {
		int adcRead = analogRead(A0);
		ticksBotoes( adcRead );
		if (adcRead > 3) {
			Serial.print("adc="); Serial.println(adcRead);
		}
			
		//if ( now - lastReadPrt > 250 ) {
		//	Serial.print("adc="); Serial.println(adcRead);
		//	lastReadPrt = now;
		//}
		lastRead = now;
	}
	
}

void ticksBotoes(int analogVal)
{
	btN.tick(analogVal);
	btS.tick(analogVal);
	btW.tick(analogVal);
	btE.tick(analogVal);
	btC.tick(analogVal);
	btL.tick(analogVal);
	btR.tick(analogVal);
}

void btN_ClickProc()
{
	//Serial.println("Botao N");
	if (btN.isLongPressed()) {
		MotorDEC.enableOutputs();
		MotorDEC.moveTo(MotorDEC.currentPosition()+10000);
	} else {
		MotorDEC.stop();
	}
}
void btS_ClickProc()
{
	//Serial.println("Botao S");
	if (btS.isLongPressed()) {
		MotorDEC.enableOutputs();
		MotorDEC.moveTo(MotorDEC.currentPosition()-10000);
	} else {
		MotorDEC.stop();
	}
}
void btW_ClickProc()
{
	//Serial.println("Botao W");
	if (btW.isLongPressed()) {
		MotorRA.enableOutputs();
		MotorRA.moveTo(MotorDEC.currentPosition()+10000);
	} else {
		MotorRA.stop();
	}

}
void btE_ClickProc()
{
	//Serial.println("Botao E");
	if (btE.isLongPressed()) {
		MotorRA.enableOutputs();
		MotorRA.moveTo(MotorDEC.currentPosition()-10000);
	} else {
		MotorRA.stop();
	}
}
void btC_ClickProc()
{
	Serial.println("Botao C");
}
void btL_ClickProc()
{
	Serial.println("Botao L");
}
void btR_ClickProc()
{
	Serial.println("Botao R");
}


