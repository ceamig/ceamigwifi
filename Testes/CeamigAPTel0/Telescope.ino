// Telescope.ino

#include "Telescope.h"


extern "C" {
	#include "user_interface.h"
}

os_timer_t myTimer;

AccelStepper MotorRA (AccelStepper::DRIVER, MotorRA_PinSTP, MotorRA_PinDIR);
AccelStepper MotorDEC(AccelStepper::DRIVER, MotorDEC_PinSTP, MotorDEC_PinDIR);

// Max Velocidade do Motor: em steps/segundo
float MotorMaxSpeed = 700;
// Max Acceleracao Motor: em steps/segundo/segundo
float MotorMaxAcceleration = 1000;



void InitMotors()
{
	MotorRA.setMaxSpeed(MotorMaxSpeed);
	MotorRA.setAcceleration(MotorMaxAcceleration);	
	MotorDEC.setMaxSpeed(MotorMaxSpeed);
	MotorDEC.setAcceleration(MotorMaxAcceleration);	
}

void SetMotorSpeed( TelMotor motor, TelSpeed speed )
{
	byte sp1, sp2;
	if (speed == SLEW)
		sp1 = LOW, sp2 = LOW;
	else if (speed = FIND)
		sp1 = LOW, sp2 = HIGH;
	else if (speed = GUIDE)
		sp1 = HIGH, sp2 = LOW;
	else if (speed = GUIDE)
		sp1 = HIGH, sp2 = HIGH;
	else
		return;
		
	if (motor == MOTORRA) {
		digitalWrite( MotorRA_PinSP1, sp1);
		digitalWrite( MotorRA_PinSP2, sp2);
	} else if (motor == MOTORDEC) {
		digitalWrite( MotorRA_PinSP1, sp1);
		digitalWrite( MotorRA_PinSP2, sp2);
	} 
}


void InitTelescope() 
{
	InitMotors();
	InitUser();
}


String sTelRA  = "06:30:00#";
String sTelDec = "-15*30'00#";
String sTelTargetRA  = "06:30:00#";
String sTelTargetDec = "-15*30'00#";

void setTelRA( String ra )
{
	sTelRA = ra;
}
void setTelDec( String dec )
{
	sTelDec = dec;
}

String getTelRA()
{
	return sTelRA;
}
String getTelDec()
{
	return sTelDec;
}
void setTelTargetRA( String ra )
{
	sTelTargetRA = ra;
}
void setTelTargetDec( String dec )
{
	sTelTargetDec = dec;
}

String getTelTargetRA()
{
	return sTelTargetRA;
}
String getTelTargetDec()
{
	return sTelTargetDec;
}

String makeString( char* str, byte len )
{
	char buf[100] = {0};
	memcpy(buf, str, len);
	String ret = String( buf );
	return ret;
}
String makeStringFromCmd( char* cmd )
{
	int len = 0;
	// procura o final do comando ('#')
	//char* pos = (char*) memchr( cmd, '#', strlen(cmd) );
	int lenCmd = strlen( cmd );
	//Serial.print( "cmd=" ); Serial.println(cmd);
	//Serial.print("lenCmd=");Serial.println(lenCmd); Serial.flush();
	char *pos = cmd;
	while ( *pos != '#' && pos-cmd < lenCmd ) {
		pos++;
	}
	if (pos - cmd < lenCmd) {
		len = pos - cmd + 1;
	}
	String ret = makeString( cmd, len );
	//Serial.print("len="); Serial.println(len);
	//Serial.print("mks="); Serial.println(ret); 
	return ret;
}


// start of timerCallback
void timerCallback(void *pArg) 
{
      MotorRA.run();
      MotorDEC.run();
} // End of timerCallback

void InitUser(void) {
 /* os_timer_setfn - Define a function to be called when the timer fires

		void os_timer_setfn( os_timer_t *pTimer, os_timer_func_t *pFunction, void *pArg)
		
  	Define the callback function that will be called when the timer reaches zero. 
  	The pTimer parameters is a pointer to the timer control structure.

	The pFunction parameters is a pointer to the callback function.

	The pArg parameter is a value that will be passed into the called back function. 
	The callback function should have the signature:  void (*functionName)(void *pArg)

	The pArg parameter is the value registered with the callback function.
*/

	os_timer_setfn(&myTimer, timerCallback, NULL);

/* os_timer_arm -  Enable a millisecond granularity timer.
     
		void os_timer_arm( os_timer_t *pTimer, uint32_t milliseconds, bool repeat)

	Arm a timer such that is starts ticking and fires when the clock reaches zero.

	The pTimer parameter is a pointed to a timer control structure.
	The milliseconds parameter is the duration of the timer measured in milliseconds. 
	The repeat parameter is whether or not the timer will restart once it has reached zero.
*/
	os_timer_arm(&myTimer, 1, true);
} // End of InitUser()



bool ProcessaLX200( char *net_buf ) 
{
	bool cmdOk = true;
	if (memcmp( net_buf+1, "GR#", 3)==0 ) {
		//Serial.print("G");
		cliente.print( getTelRA() );
	} else	if (memcmp( net_buf+1, "GD#", 3)==0 ) {
		//Serial.print("D");
		cliente.print( getTelDec() );
	} else 	if (memcmp( net_buf+1, "CM#", 3)==0 ) {		// Align
		//cliente.print(" M31 EX GAL MAG 3.5 SZ178.0'#"); // autostar fixed string 
		Serial.print("CM#  ");
		Serial.flush();
		String newRA  = getTelTargetRA();
		String newDec = getTelTargetDec();
		setTelRA( newRA );   Serial.print( newRA ); Serial.print( " ~ " );
		setTelDec( newDec ); Serial.println( newDec );
		cliente.print( "Coordinates     matched.        #" ); 
	} else 	if (memcmp( net_buf+1, "MS#", 3)==0 ) {		// Slew to target
		Serial.print("MS#");
		String newRA  = getTelTargetRA();
		String newDec = getTelTargetDec();
		setTelRA( newRA );   Serial.print( newRA ); Serial.print( " ~ " );
		setTelDec( newDec ); Serial.println( newDec );
		cliente.print('0');  // slew is possible
	} else 	if (memcmp( net_buf+1, "Me#", 3)==0 ) {
		MotorRA.enableOutputs();
		MotorRA.setMaxSpeed(MotorMaxSpeed);
		MotorRA.setAcceleration(MotorMaxAcceleration);
		MotorRA.moveTo(MotorRA.currentPosition()-10000);
	} else 	if (memcmp( net_buf+1, "Mn#", 3)==0 ) {
		MotorDEC.enableOutputs();
		MotorDEC.setMaxSpeed(MotorMaxSpeed);
		MotorDEC.setAcceleration(MotorMaxAcceleration);
		MotorDEC.moveTo(MotorDEC.currentPosition()+10000);
	} else 	if (memcmp( net_buf+1, "Ms#", 3)==0 ) {
		MotorDEC.enableOutputs();
		MotorDEC.setMaxSpeed(MotorMaxSpeed);
		MotorDEC.setAcceleration(MotorMaxAcceleration);
		MotorDEC.moveTo(MotorDEC.currentPosition()-10000);
	} else 	if (memcmp( net_buf+1, "Mw#", 3)==0 ) {
		MotorRA.enableOutputs();
		MotorRA.setMaxSpeed(MotorMaxSpeed);
		MotorRA.setAcceleration(MotorMaxAcceleration);
		MotorRA.moveTo(MotorRA.currentPosition()+10000);
	} else 	if (memcmp( net_buf+1, "Qe#", 3)==0 ) {
		MotorRA.stop();
	} else 	if (memcmp( net_buf+1, "Qn#", 3)==0 ) {
		MotorDEC.stop();
	} else 	if (memcmp( net_buf+1, "Qs#", 3)==0 ) {
		MotorDEC.stop();
	} else 	if (memcmp( net_buf+1, "Qw#", 3)==0 ) {
		MotorRA.stop();
	} else 	if (memcmp( net_buf+1, "Q#", 2)==0 ) {
		MotorRA.stop();
		MotorDEC.stop();
	} else 	if (memcmp( net_buf+1, "RS#", 3)==0 ) {
		//Serial.print("R");
	} else if (memcmp( net_buf+1, "Sd", 2)==0 ) {  // set target DEC
		//Serial.print("Sd "); Serial.flush();
		String coord = makeStringFromCmd( net_buf+3 );
		coord.replace("*",":");
		//Serial.println( coord );
		setTelTargetDec( coord );
		cliente.print('1');
	} else if (memcmp( net_buf+1, "SC", 2)==0 ) {
		Serial.print("t");
		cliente.print("1Updating Planetary Data#                              #");
		//cliente.print("                #                #");
	} else 	if (memcmp( net_buf+1, "SG", 2)==0 ) {
		//Serial.print("T");
		cliente.print('1');
	} else if (memcmp( net_buf+1, "Sg", 2)==0 ) {
		Serial.print("O");
		cliente.print('1');
	} else if (memcmp( net_buf+1, "SL", 2)==0 ) {
		//Serial.print("t");
		cliente.print('1');
	} else if (memcmp( net_buf+1, "Sr", 2)==0 ) {  // set target RA
		//Serial.print("Sr "); Serial.flush();
		String coord = makeStringFromCmd( net_buf+3 );
		//Serial.println( coord );
		setTelTargetRA( coord );
		cliente.print('1');
	} else if (memcmp( net_buf+1, "St", 2)==0 ) {
		Serial.print("L");
		cliente.print('1');
	} else 	if (memcmp( net_buf+1, "Sw", 2)==0 ) {
		cliente.print('1');
	} else {
		cmdOk = false;   // comando desconhecido 
	}
	return cmdOk;
}

