#include <ESP8266WiFi.h>
#include "PortButton.h"
#include "Wire.h"

const uint8_t SDA_PIN = 2;
const uint8_t SCL_PIN = 4;
// Botoes (I2C, PCF8574a em 0x20 ou 0x38)
const uint8_t KEYPAD_ADDR = 0x38;
const bool PULL_DOWN = true;  // keypad: active low. Chip default = pullup

PortButtonSet keypad(KEYPAD_ADDR, SDA_PIN, SCL_PIN, false);

const bool KEY_ACTIVE_HIGH = true;
PortButton btN(keypad, 0, true);
/*PortButton btS(keypad, 1, true);
PortButton btE(keypad, 2, true);
PortButton btW(keypad, 3, true);
PortButton btC(keypad, 4, true);
PortButton btL(keypad, 5, true);
PortButton btR(keypad, 6, true);
*/
const int defClickTicks = 50;
const int defPressTicks = 100;


void setup()
{
	delay(2000);  //delay de inicializacao do ESP (necessario)	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)

	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)

	#ifdef ESP8266
	Serial.println("ESP8266");
	#endif	

	btN.setClickTicks( defClickTicks ); btN.setPressTicks( defPressTicks );
/*	btS.setClickTicks( defClickTicks ); btS.setPressTicks( defPressTicks );
	btW.setClickTicks( defClickTicks ); btW.setPressTicks( defPressTicks );
	btE.setClickTicks( defClickTicks ); btE.setPressTicks( defPressTicks );
	btC.setClickTicks( defClickTicks ); btC.setPressTicks( defPressTicks );
	btL.setClickTicks( defClickTicks ); btL.setPressTicks( defPressTicks );
	btR.setClickTicks( defClickTicks ); btR.setPressTicks( defPressTicks );
*/
	btN.attachLongPressStart(btN_ClickProc);
/*	btS.attachLongPressStart(btS_ClickProc);
	btW.attachLongPressStart(btW_ClickProc);
	btE.attachLongPressStart(btE_ClickProc);
	btC.attachLongPressStart(btC_ClickProc);
	btL.attachLongPressStart(btL_ClickProc);
	btR.attachLongPressStart(btR_ClickProc);
*/	btN.attachLongPressStop(btN_ClickProc);
/*	btS.attachLongPressStop(btS_ClickProc);
	btW.attachLongPressStop(btW_ClickProc);
	btE.attachLongPressStop(btE_ClickProc);
	btC.attachLongPressStop(btC_ClickProc);
	btL.attachLongPressStop(btL_ClickProc);
	btR.attachLongPressStop(btR_ClickProc);
*/
	Serial.println("");
	Serial.println("Keypad inicializado...");

	//digitalWrite(2, LOW);
	//delay(500);
	//digitalWrite(2, HIGH);

}

void loop()
{
	keypad.tick();
	delay(100);    // tem que ter um delay minimo para o ESP
}


void btN_ClickProc()
{
	//Serial.println("Botao N");
	if (btN.isLongPressed()) {
		Serial.println("Botao N down");
	} else {
		Serial.println("Botao N up");
	}
}

/*
void btS_ClickProc()
{
	//Serial.println("Botao S");
	if (btS.isLongPressed()) {
		Serial.println("Botao S down");
	} else {
		Serial.println("Botao S up");
	}
}
void btW_ClickProc()
{
	//Serial.println("Botao W");
	if (btW.isLongPressed()) {
		Serial.println("Botao W down");
	} else {
		Serial.println("Botao W up");
	}
}
void btE_ClickProc()
{
	Serial.println("Botao E");
	if (btE.isLongPressed()) {
		Serial.println("Botao E down");
	} else {
		Serial.println("Botao E up");
	}
}
void btC_ClickProc()
{
	//Serial.println("Botao C");
	if (btC.isLongPressed()) {
		// nothing (pressed)
		Serial.println("Botao C down");
	} else {
		// released:
		Serial.println("Botao C up");
	}
}
void btL_ClickProc()
{
	Serial.println("Botao L");
}

void btR_ClickProc()
{
	Serial.println("Botao R");
}


*/
