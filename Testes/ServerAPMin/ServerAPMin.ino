#include <ESP8266WiFi.h>

// ATENCAO: GPIO0 (FLASH) -> low -> para upload 
//                           high -> boot normal
//          CH_PD -> high

// definicoes WiFi:
const char WiFiSENHA[] = "ceamig12";   // minimo 8 chars

// Definicoes Pinos I/O
const int LED_PIN = 2;    // ESP-07: LED no GPIO2 - active low (LOW para ligar)

WiFiServer server(80);

void setup() 
{
	initHardware();
	setupWiFi();
	server.begin();
	Serial.println("Server inicializado");
}

void loop() 
{
	// Verifica se um cliente conectou
	WiFiClient cliente = server.available();
	if (!cliente) {
		return;
	}
	
	// Le a primeira linha da requisicao:
	String req = cliente.readStringUntil('\r');
	Serial.println(req);
	cliente.flush();
	
	// Verifica a requisicao:
	int val = -1;   // valor para requisicao invalida
	            
	if (req.indexOf("/led/0") != -1)
		val = 0; // desligar o led
	else if (req.indexOf("/led/1") != -1)
		val = 1; // ligar o led
	else if (req.indexOf("/msg") != -1) //2)
		val = 2; // Mostra mensagem na porta serial
	else if (req.indexOf("/g0/0") != -1)
		digitalWrite( 0, LOW);
	else if (req.indexOf("/g0/1") != -1)
		digitalWrite( 0, HIGH);
	else if (req.indexOf("/g2/0") != -1)
		digitalWrite( 2, LOW);
	else if (req.indexOf("/g2/1") != -1)
		digitalWrite( 2, HIGH);
	else if (req.indexOf("/g4/0") != -1)
		digitalWrite( 4, LOW);
	else if (req.indexOf("/g4/1") != -1)
		digitalWrite( 4, HIGH);
	else if (req.indexOf("/g5/0") != -1)
		digitalWrite( 5, LOW);
	else if (req.indexOf("/g5/1") != -1)
		digitalWrite( 5, HIGH);
	else if (req.indexOf("/g12/0") != -1)
		digitalWrite(12, LOW);
	else if (req.indexOf("/g12/1") != -1)
		digitalWrite(12, HIGH);
	else if (req.indexOf("/g13/0") != -1)
		digitalWrite(13, LOW);
	else if (req.indexOf("/g13/1") != -1)
		digitalWrite(13, HIGH);
	else if (req.indexOf("/g14/0") != -1)
		digitalWrite(14, LOW);
	else if (req.indexOf("/g14/1") != -1)
		digitalWrite(14, HIGH);
	else if (req.indexOf("/g15/0") != -1)
		digitalWrite(15, LOW);
	else if (req.indexOf("/g15/1") != -1)
		digitalWrite(15, HIGH);
	else if (req.indexOf("/g16/0") != -1)
		digitalWrite(16, LOW);
	else if (req.indexOf("/g16/1") != -1)
		digitalWrite(16, HIGH);
	// qualquer outro valor, a requisao e' invalida (-1)
	
	// Ajusta o pino GPIO5 de acordo com a requisicao:
	if (val == 0 || val == 1)
		digitalWrite(LED_PIN, val);
	
	cliente.flush();
	
	// Prepara a resposta em HTML:
	String s = "HTTP/1.1 200 OK\r\n";
	s += "Content-Type: text/html\r\n\r\n";
	s += "<!DOCTYPE HTML>\r\n<html>\r\n";
	
	// Mostra o status do Led, se for o caso:
	if (val == 0 || val == 1) {
		s += "LED -> ";
		s += (val)?"LIGADO":"DESLIGADO";
	} else if (val == 2) { 
		// Mostra mensagem padrao:
		s += "CEAMIG - Servidor em modo AP";
		s += "<br>"; // vai p a prox linha
		s += "Servidor minimo.";
	} else {
		s += "Requisicao Invalida.<br> Tente /led/1, /led/0, or /msg";
	}
	s += "</html>\n";
	
	// Envia a resposta para o cliente
	cliente.print(s);
	delay(1);
	Serial.println("Cliente disconectado");
	
	// O cliente vai ser realmente desconectado quando a funcao
	// retorna e o objeto 'Cliente' for destruido 
}

void setupWiFi()
{
	WiFi.mode(WIFI_AP);
	
	// Para tentar conseguir um numero unico: anexar os dois
	// ultimos bytes do MAC addres (em HEXA):
	//uint8_t mac[WL_MAC_ADDR_LENGTH];
	//WiFi.softAPmacAddress(mac);
	//String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
	//               String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
	//macID.toUpperCase();
	//String AP_NameString = "CEAMIG-WIFI " + macID;
	//String AP_NameString = "CEAMIG WIFI 1234";	
	String AP_NameString = "SkyFi-1234567890";  // minimo 16 chars (?)
	
	char AP_NameChar[AP_NameString.length() + 1];
	memset(AP_NameChar, AP_NameString.length() + 1, 0);
	
	for (int i=0; i<AP_NameString.length(); i++)
		AP_NameChar[i] = AP_NameString.charAt(i);
	
	WiFi.softAP(AP_NameChar);//, WiFiSENHA);
}

void initHardware()
{
	Serial.begin(74880);
	pinMode(LED_PIN, OUTPUT);
	
	pinMode( 0, OUTPUT);
	pinMode( 2, OUTPUT);
	pinMode( 4, OUTPUT);
	pinMode( 5, OUTPUT);
	pinMode(12, OUTPUT);
	pinMode(13, OUTPUT);
	pinMode(14, OUTPUT);
	pinMode(15, OUTPUT);
	pinMode(16, OUTPUT);
	digitalWrite(LED_PIN, LOW);
}

