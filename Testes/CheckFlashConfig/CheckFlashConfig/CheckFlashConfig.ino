/* 
 ESP8266 CheckFlashConfig by Markus Sattler
 
 This sketch tests if the EEPROM settings of the IDE match to the Hardware

 Modified: Marcos Ackel 2021-07-13
 
 */

#include <ESP8266WiFi.h> 

ADC_MODE(ADC_VCC);   // to use ESP.getVcc()
// This line has to appear outside of any functions, for instance right after the #include lines of your sketch.


void setup(void) {


    Serial.begin(74880);


    uint32_t realSize = ESP.getFlashChipRealSize();
    uint32_t flashSize = ESP.getFlashChipSize();
    FlashMode_t flashMode = ESP.getFlashChipMode();

	Serial.printf("---------------\n");
	Serial.printf("Core Version:    %s\n", ESP.getCoreVersion());
    Serial.printf("SDK Version:     %s\n", ESP.getSdkVersion());
    Serial.printf("Boot version:    %d\n", ESP.getBootVersion());
	Serial.printf("\n");
    Serial.printf("Flash real id:   %08X\n", ESP.getFlashChipId());
    Serial.printf("Flash real size: %u\n", realSize);
	Serial.printf("\n");
    Serial.printf("Flash flash size  (ide): %u\n", flashSize);
    Serial.printf("Flash flash speed (ide): %u\n", ESP.getFlashChipSpeed());
    Serial.printf("Flash flash mode  (ide):  %s\n", (flashMode == FM_QIO ? "QIO" : flashMode == FM_QOUT ? "QOUT" : flashMode == FM_DIO ? "DIO" : flashMode == FM_DOUT ? "DOUT" : "UNKNOWN"));

    if(flashSize != realSize) {
        Serial.println("Flash Chip configuration wrong!\n");
    } else {
        Serial.println("Flash Chip configuration ok.\n");
    }
    Serial.printf("\n");
    Serial.printf("Supply voltage: %d/1000\n", ESP.getVcc());
    Serial.printf("---------------\n");


    Serial.printf("===================\n");
    IPAddress myadr = IPAddress(10,1,2,3);

    
	
	Serial.printf("IPAddress = %s\n", myadr.toString() );


    Serial.printf("==== FIM  ====\n");
}

void loop() 
{
	Serial.printf(".");
    delay(10000);
}
