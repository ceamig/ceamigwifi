// Keypad.ino

#include "Keypad.h"

// Keypad:
const uint8_t SDA_PIN = 2;
const uint8_t SCL_PIN = 4;
// Botoes (I2C, PCF8574a em 0x20 ou 0x38)
const uint8_t KEYPAD_ADDR = 0x38;
PortButtonSet keypad(KEYPAD_ADDR, SDA_PIN, SCL_PIN, true);

// Botoes:
PortButton btN(keypad, 0, true);	// Norte
PortButton btS(keypad, 1, true);	// Sul
PortButton btE(keypad, 2, true);	// Oeste (West)
PortButton btW(keypad, 3, true);	// Leste (East)
PortButton btC(keypad, 4, true);	// Centro
PortButton btL(keypad, 5, true);	// Esquerda (Left)
PortButton btR(keypad, 6, true);// Direita  (Right)	

const int defClickTicks = 50;
const int defPressTicks = 100;

KeypadSpeed_t _curKeypadSpeed = SLEW;

void InitKeypad(void)
{
	//keypad.setDefaultClickTicks(50);   // isso efetivamente desabilita a detecao de double-click
	//keypad.setDefaultPressTicks(60);
	btN.setClickTicks( defClickTicks ); btN.setPressTicks( defPressTicks );
	btS.setClickTicks( defClickTicks ); btS.setPressTicks( defPressTicks );
	btW.setClickTicks( defClickTicks ); btW.setPressTicks( defPressTicks );
	btE.setClickTicks( defClickTicks ); btE.setPressTicks( defPressTicks );
	btC.setClickTicks( defClickTicks ); btC.setPressTicks( defPressTicks );
	btL.setClickTicks( defClickTicks ); btL.setPressTicks( defPressTicks );
	btR.setClickTicks( defClickTicks ); btR.setPressTicks( defPressTicks );

	btN.attachLongPressStart(btN_ClickProc);
	btS.attachLongPressStart(btS_ClickProc);
	btW.attachLongPressStart(btW_ClickProc);
	btE.attachLongPressStart(btE_ClickProc);
	btC.attachLongPressStart(btC_ClickProc);
	btL.attachLongPressStart(btL_ClickProc);
	btR.attachLongPressStart(btR_ClickProc);
	btN.attachLongPressStop(btN_ClickProc);
	btS.attachLongPressStop(btS_ClickProc);
	btW.attachLongPressStop(btW_ClickProc);
	btE.attachLongPressStop(btE_ClickProc);
	btC.attachLongPressStop(btC_ClickProc);
	btL.attachLongPressStop(btL_ClickProc);
	btR.attachLongPressStop(btR_ClickProc);
	//btCtrl.attachDoubleClick(btCtrlDoubleClickProc);   // alternativo para iniciar os fases
	//btCtrl.attachDuringLongPress(btCtrlDuringProc);	
}


void processaKeypad(void)
{
	keypad.tick();
	MotorAzm.run();
	MotorAlt.run();
}

void btN_ClickProc()
{
	//Serial.println("Botao N");

	if (btN.isLongPressed()) {
	} else {
		// button Released:
	}

}
void btS_ClickProc()
{
	//Serial.println("Botao S");
	if (btS.isLongPressed()) {
	} else {
	}
}

void btW_ClickProc()
{
	//Serial.println("Botao W");
	if (btW.isLongPressed()) {
		
	} else {
		
	}
}
void btE_ClickProc()
{
	//Serial.println("Botao E");
	if (btE.isLongPressed()) {
		
	} else {
		
	}
}


void btC_ClickProc()
{
	//Serial.println("Botao C");

	if (btC.isLongPressed()) {
		// button Pressed:
	} else {
		// button released:
		if (_curKeypadSpeed == SLEW) {
			_curKeypadSpeed = FIND;
			tone(2600, 150);
		} else if (_curKeypadSpeed == FIND) {
			_curKeypadSpeed = CENTER;
			tone(800, 150);
		} else if (_curKeypadSpeed == CENTER) {
			_curKeypadSpeed = TRACK;
			tone(350, 150);
		} else if (_curKeypadSpeed == TRACK) {
			_curKeypadSpeed = SLEW;
			tone(4000, 150);
		}
		
	}
}

float _motorFullSpeed = 0; // vel em full step

void btL_ClickProc()
{
	//Serial.println("Botao L");
	if (btL.isLongPressed() ) {
		//Serial.println("L Long"); 
		_motorFullSpeed -= 200;
		if (_motorFullSpeed < 0) _motorFullSpeed = 0;
		MotorAzm.freeSpeedMoveTo(_motorFullSpeed, 10000000);
		Serial.print("-FSpeed: "); Serial.println(_motorFullSpeed); 
	}
}

void btR_ClickProc()
{
	Serial.println("Botao R");
	if (btR.isLongPressed() ) {
		tone(440, 200);
		//Serial.println("R Long"); 
		_motorFullSpeed += 200;
		if (_motorFullSpeed < 0) _motorFullSpeed = 0;
		MotorAzm.freeSpeedMoveTo(_motorFullSpeed, 10000000);
		Serial.print("+FSpeed: "); Serial.println(_motorFullSpeed); 
	}
}


