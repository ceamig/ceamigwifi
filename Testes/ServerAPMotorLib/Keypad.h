

#ifndef KEYPAD_H
#define KEYPAD_H

#include <PortButton.h>
#include "AstroMotor.h"

extern PortButtonSet keypad;
extern PortButton btN;  // Norte
extern PortButton btS;  // Sul
extern PortButton btW;  // Oeste (West)
extern PortButton btE;  // Leste (East)
extern PortButton btC;  // Centro
extern PortButton btL;  // Esquerda (Left)
extern PortButton btR;  // Direita  (Right)

void InitKeypad(void);
void processaKeypad();
void ticksBotoes(int analogVal);

#define KeypadSpeed_t MotorSpeed

#endif
