// CeamigAPTel - Ceamig Access Point Telescope
//
// ESP8266 Boot mode:
//                 GPIO15 GPIO0 GPIO2    (boot mode - decimal)
// upload/UART       0      0     1         1
// normal boot       0      1     1         3
// SD-Card boot      1      0     0         4     (nao usado)

#include <ESP8266WiFi.h>
#include <String.h>
#include "Keypad.h"

int G_debug = 0;

// definicoes WiFi:
const char WiFiSSID[]     = "CEAMIG";
const char WiFiSENHA[]    = "ceamig17";		// minimo 8 chars
const int  WiFiPort       = 4030;			// porta de interface
const int  WiFiPortConfig = 80;				// porta para configuracoes (para web browser)

const uint8_t PinBeep = 13;

WiFiServer server(WiFiPort);					// server de interface com o telescopio

//-------------- motores:
const uint8_t MotorAlt_PinSTP   = 12;    // Motor de Altura
const uint8_t MotorAlt_PinDIR   = 14;
const uint8_t MotorAlt_PinSPEED = 16;
const uint8_t MotorAzm_PinSTP   = 15;    //15 Motor de Azimute
const uint8_t MotorAzm_PinDIR   =  0;  //0 2;
const uint8_t MotorAzm_PinSPEED =  5;

AstroMotor MotorAlt( MotorAlt_PinSTP, MotorAlt_PinDIR, MotorAlt_PinSPEED );
AstroMotor MotorAzm( MotorAzm_PinSTP, MotorAzm_PinDIR, MotorAzm_PinSPEED );
//--------------


void tone(int freq, int duracao)
{
	analogWriteFreq(freq);
	analogWrite(PinBeep, 512);
	delay(duracao);
	analogWrite(PinBeep, 0);
}
//--------------------------------------------------------------

void setup() 
{
	initHardware();
	delay(500);
	
	Serial.println();
	Serial.println("Configurando Ponto de Acesso...");
	configWiFi(true);
	//Serial.println("Server iniciado."); 
	//Serial.println(WiFiPort);

	InitKeypad();

	//Serial.print("Motor Max Speed: ");
	//Serial.println(telescope.getMaxSpeed(Telescope::MOTORAZM));
}
//--------------------------------------------------------------


// serial end ethernet buffer size
#define BUFFER_SIZE 128


#define min(a,b) ((a)<(b)?(a):(b))

WiFiClient cliente;

//--------------------------------------------------------------

void loop() 
{
	size_t bytes_read;
  	uint8_t net_buf[BUFFER_SIZE]={0};

//	if (G_debug != 0) {
//		Serial.print("Debug: "); Serial.println(G_debug);
//	}
	// trata requisicoes do server de configuracao:
	
	processaKeypad();
	delay(1);
	
	// Verifica se um cliente conectou
	cliente = server.available();
	if (cliente) {
		cliente.setNoDelay(true);
		while(!cliente.available()){
			processaKeypad();
			//Serial.print(".");
			delay(1);
		}
	}

	if (cliente && cliente.connected()) {
    	// check the network for any bytes to send to the serial
    	int count = cliente.available();
//   	Serial.println("");
//	    Serial.print("!");
//	    Serial.print(count, DEC);
    	if (count > 0) {
//	        Serial.print("~");
			bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
			// Para debug de comandos:
			//Serial.print(bytes_read, DEC); Serial.print(" ");
	      	//Serial.write(net_buf, bytes_read); Serial.println("");
			delay(10);
	      	//Serial.flush();
		}
		delay(1);
  	}
  	delay(1);
}
//--------------------------------------------------------------

void configWiFi(bool bPrintInfo)
{
	delay(2000);

	// Mac Address (para compor o SSID "unico":
	uint8_t mac[WL_MAC_ADDR_LENGTH];
	WiFi.softAPmacAddress(mac);
	IPAddress myIP = WiFi.softAPIP();
	// Anexar os dois ultimos digitos (em HEXA) do mac address ao SSID:
	String macBytes = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
					  String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
	macBytes.toUpperCase();
	String wifiSSIDCompl = String(WiFiSSID) + String("-") + macBytes;
	/* You can remove the password parameter if you want the AP to be open. */
	//WiFi.softAP(wifiSSIDCompl, WiFiSENHA);
	//WiFi.softAP(WiFiSSID);	
	
	WiFi.softAP(wifiSSIDCompl.c_str());	
	
	if (bPrintInfo) {
		Serial.print("SSID: ");
		Serial.println(wifiSSIDCompl.c_str());
		Serial.print("AP endereco IP: ");
		Serial.println(myIP);
		Serial.print("Porta: ");
		Serial.println(WiFiPort);
		Serial.print("Porta Configuracao: ");
		Serial.println(WiFiPortConfig);
	}
	server.begin();		// server de interface do telescopio
	server.setNoDelay(true);
}
//--------------------------------------------------------------

void initHardware()
{
	//pinMode(LED_PIN, OUTPUT);
	pinMode(PinBeep, OUTPUT);
	pinMode(A0, INPUT);
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)
}

//--------------------------------------------------------------
//--------------------------------------------------------------


