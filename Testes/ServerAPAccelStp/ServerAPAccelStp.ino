// ATENCAO: GPIO0 (FLASH) -> low -> para upload 
//                           high -> boot normal
//          CH_PD -> high

#include <ESP8266WiFi.h>
#include <AccelStepper.h>

// definicoes WiFi:
const char WiFiSENHA[] = "ceamig12";   // minimo 8 chars
const char WiFiSSID[]  = "CEAMIG";

// Definicoes Pinos I/O
const int BEEP_PIN = 13;
const uint8_t MotorAlt_PinSTP   = 12;    // Motor de Altura
const uint8_t MotorAlt_PinDIR   = 14;
const uint8_t MotorAlt_PinSPEED = 16;
const uint8_t MotorAzm_PinSTP   = 15;    //15 Motor de Azimute
const uint8_t MotorAzm_PinDIR   =  0;  //0 2;
const uint8_t MotorAzm_PinSPEED =  5;

WiFiServer server(4030);

void tone(int freq, int duracao)
{
	analogWriteFreq(freq);
	analogWrite(BEEP_PIN, 512);
	delay(duracao);
	analogWrite(BEEP_PIN, 0);
}
//--------------------------------------------------------------

AccelStepper MotorAzm (AccelStepper::DRIVER, MotorAzm_PinSTP, MotorAzm_PinDIR);
AccelStepper MotorAlt(AccelStepper::DRIVER, MotorAlt_PinSTP, MotorAlt_PinDIR);

// Max Velocidade do Motor: em steps/segundo
float MotorMaxSpeed = 700;
// Max Acceleracao Motor: em steps/segundo/segundo
float MotorMaxAcceleration = 500;



void InitMotors()
{
	MotorAzm.setMaxSpeed(MotorMaxSpeed);
	MotorAzm.setAcceleration(MotorMaxAcceleration);	
	MotorAlt.setMaxSpeed(MotorMaxSpeed);
	MotorAlt.setAcceleration(MotorMaxAcceleration);	
}

void setup() 
{
	initHardware();
	InitMotors();
	//setupWiFi();
	//server.begin();
	setupWiFi();
}

// serial end ethernet buffer size
#define BUFFER_SIZE 128

#define min(a,b) ((a)<(b)?(a):(b))

WiFiClient cliente;


int cnt = 0;
int dir = 1;
long last = 0;
bool first = true;

void loop() 
{
	size_t bytes_read;
  	uint8_t net_buf[BUFFER_SIZE]={0};

  	MotorAzm.run();
  	MotorAlt.run();


	if (cnt== 0 || MotorAzm.distanceToGo() == 0) {
		if (first) {
			first = false;
			last = millis();
		}
		if (cnt==0) {
			Serial.print("Speed:");Serial.print("0.0");Serial.print(" Interv:"); Serial.println( millis() - last );
			tone(400,150);
			delay(2000);
			tone(4000,150);
			last = millis();
		}
		cnt++;
		if (cnt == 5) {
			cnt=0; 
			dir *= -1; // inverte direcao
		} else {
			float speed = 200 * cnt;
			MotorAzm.setMaxSpeed(speed);
			Serial.print("Speed:");Serial.print(speed);Serial.print(" Interv:"); Serial.println( millis() - last );
			last = millis();
			MotorAzm.move(dir * 2000);
		}
	}

	// Verifica se um cliente conectou
	cliente = server.available();
	if (!cliente) {
		return;
	}
	cliente.setNoDelay(true);
	while(!cliente.available()){
		delay(1);
	  	MotorAzm.run();
  		MotorAlt.run();
	}
	if(cliente.connected()) {
    	// check the network for any bytes to send to the serial
    	int count = cliente.available();
    	if (count > 0) {
			bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
			delay(1);
    	}
  	}
  	delay(10);

}


void setupWiFi()
{
	delay(2000);

	// Mac Address (para compor o SSID "unico":
	uint8_t mac[WL_MAC_ADDR_LENGTH];
	WiFi.softAPmacAddress(mac);
	IPAddress myIP = WiFi.softAPIP();
	
	WiFi.softAP(WiFiSSID);	
	
	Serial.print("SSID: ");
	Serial.println(WiFiSSID);
	Serial.print("AP endereco IP: ");
	Serial.println(myIP);

	
	server.begin();		// server de interface do telescopio
	server.setNoDelay(true);
}

void initHardware()
{
	//pinMode(LED_PIN, OUTPUT);
	pinMode(MotorAzm_PinSTP, OUTPUT);
	pinMode(MotorAzm_PinDIR, OUTPUT);
	pinMode(MotorAzm_PinSPEED, OUTPUT);
	pinMode(MotorAlt_PinSTP, OUTPUT);
	pinMode(MotorAlt_PinDIR, OUTPUT);
	pinMode(MotorAlt_PinSPEED, OUTPUT);

	Serial.begin(74880);
	delay(200);
}

