// ATENCAO: GPIO0 (FLASH) -> low -> para upload 
//                           high -> boot normal
//          CH_PD -> high

#include <ESP8266WiFi.h>
#include <AccelStepper.h>

// definicoes WiFi:
const char WiFiSENHA[] = "ceamig12";   // minimo 8 chars
const char WiFiSSID[]  = "CEAMIG";

// Definicoes Pinos I/O
const int LED_PIN = 5;    // GPIO pino 5
const int MotorRA_PinSTP = 13;    // Motor de RA
const int MotorRA_PinDIR = 12;
const int MotorRA_PinSP1 = 14;
const int MotorRA_PinSP2 = 16;
const int MotorDEC_PinSTP =  0;    //15 Motor de DEC
const int MotorDEC_PinDIR =  4;  //0 2;
const int MotorDEC_PinSP1 =  2;
const int MotorDEC_PinSP2 =  5;

WiFiServer server(4030);


AccelStepper MotorRA (AccelStepper::DRIVER, MotorRA_PinSTP, MotorRA_PinDIR);
AccelStepper MotorDEC(AccelStepper::DRIVER, MotorDEC_PinSTP, MotorDEC_PinDIR);

// Max Velocidade do Motor: em steps/segundo
float MotorMaxSpeed = 700;
// Max Acceleracao Motor: em steps/segundo/segundo
float MotorMaxAcceleration = 1000;

typedef enum { GUIDE, CENTER, FIND, SLEW } TelSpeed;
typedef enum { MOTORRA, MOTORDEC } TelMotor;

void InitMotors()
{
	MotorRA.setMaxSpeed(MotorMaxSpeed);
	MotorRA.setAcceleration(MotorMaxAcceleration);	
	MotorDEC.setMaxSpeed(MotorMaxSpeed);
	MotorDEC.setAcceleration(MotorMaxAcceleration);	
}

void SetMotorSpeed( TelMotor motor, TelSpeed speed )
{
	byte sp1, sp2;
	if (speed == SLEW)
		sp1 = LOW, sp2 = LOW;
	else if (speed = FIND)
		sp1 = LOW, sp2 = HIGH;
	else if (speed = GUIDE)
		sp1 = HIGH, sp2 = LOW;
	else if (speed = GUIDE)
		sp1 = HIGH, sp2 = HIGH;
	else
		return;
		
	if (motor == MOTORRA) {
		digitalWrite( MotorRA_PinSP1, sp1);
		digitalWrite( MotorRA_PinSP2, sp2);
	} else if (motor == MOTORDEC) {
		digitalWrite( MotorRA_PinSP1, sp1);
		digitalWrite( MotorRA_PinSP2, sp2);
	} 
}


void setup() 
{
	initHardware();
	InitMotors();
	//setupWiFi();
	//server.begin();
	configWiFi();
}

// serial end ethernet buffer size
#define BUFFER_SIZE 128

#define min(a,b) ((a)<(b)?(a):(b))

WiFiClient cliente;

void delayRun(uint8_t ms)
{
	int nus = ms * 1000;
	for ( int i = 0; i < nus; i+=500 ) {
		MotorRA.run();
		MotorDEC.run();
		delayMicroseconds(500);
	}	
}

void loop() 
{
	size_t bytes_read;
  	uint8_t net_buf[BUFFER_SIZE]={0};

  	MotorRA.run();
  	MotorDEC.run();

	// Verifica se um cliente conectou
	cliente = server.available();
	if (!cliente) {
		return;
	}
	cliente.setNoDelay(true);
	while(!cliente.available()){
		delayRun(1);
	  	MotorRA.run();
  		MotorDEC.run();
	}
	if(cliente.connected()) {
    	// check the network for any bytes to send to the serial
    	int count = cliente.available();
 //   	Serial.println("");
//	    Serial.print("!");
//	    Serial.print(count, DEC);
    	if (count > 0) {
//	        Serial.print("~");
	        delayRun(1);
			bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
			delayRun(1);
//	        Serial.print(bytes_read, DEC); Serial.print(" ");
	        delayRun(1);
//	      	Serial.write(net_buf, bytes_read);
	      	delayRun(1);
	      	for ( byte idx = 0; idx < bytes_read; idx++) {
				if (net_buf[idx] == ':') {
//					Serial.print(" x");
					delayRun(1);
					ProcessaLX200( net_buf+idx );
					delayRun(5);
				}
			}	
			//delay(5);	
	      	//Serial.flush();
		}
		//delay(50);
	//} else {
    //	cliente.stop();
  	}
  	delayRun(10);

//	// Envia a resposta para o cliente
//	cliente.print(s);
//	delay(1);
//	Serial.println("Cliente disconectado");
	
}

void configWiFi()
{
	delay(1000);
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)
	Serial.println();
	Serial.print("Configurando Ponto de Acesso...");
	/* You can remove the password parameter if you want the AP to be open. */
	WiFi.softAP(WiFiSSID);

	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	server.begin();
	server.setNoDelay(true);
	Serial.println("Server iniciado: Porta 4030");
}

void setupWiFi()
{
	WiFi.mode(WIFI_AP);
	
	// Para tentar conseguir um numero unico: anexar os dois
	// ultimos bytes do MAC addres (em HEXA):
	//uint8_t mac[WL_MAC_ADDR_LENGTH];
	//WiFi.softAPmacAddress(mac);
	//String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
	//               String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
	//macID.toUpperCase();
	//String AP_NameString = "CEAMIG-WIFI " + macID;
	//String AP_NameString = "CEAMIG WIFI 1234";	
	String AP_NameString = "SkyFi-1234567890";  // minimo 16 chars (?)
	
	char AP_NameChar[AP_NameString.length() + 1];
	memset(AP_NameChar, AP_NameString.length() + 1, 0);
	
	for (int i=0; i<AP_NameString.length(); i++)
		AP_NameChar[i] = AP_NameString.charAt(i);
	
	WiFi.softAP(AP_NameChar);//, WiFiSENHA);
}

void initHardware()
{
	//pinMode(LED_PIN, OUTPUT);
	pinMode(MotorRA_PinSTP, OUTPUT);
	pinMode(MotorRA_PinDIR, OUTPUT);
	pinMode(MotorRA_PinSP1, OUTPUT);
	pinMode(MotorRA_PinSP2, OUTPUT);
	pinMode(MotorDEC_PinSTP, OUTPUT);
	pinMode(MotorDEC_PinDIR, OUTPUT);
	pinMode(MotorDEC_PinSP1, OUTPUT);
	pinMode(MotorDEC_PinSP2, OUTPUT);
	//digitalWrite(LED_PIN, LOW);
	Serial.begin(74880);
	delay(200);
}

void ProcessaLX200( uint8_t *net_buf ) {
	if (memcmp( net_buf+1, "GR#", 3)==0 ) {
		//Serial.print("G");
		delayRun(1);
		cliente.print("06:30:00#");
	} else	if (memcmp( net_buf+1, "GD#", 3)==0 ) {
		//Serial.print("D");
		delayRun(1);
		cliente.print("-15*30'00#");
	} else if (memcmp( net_buf+1, "St", 2)==0 ) {
		Serial.print("L");
		delay(1);
		cliente.print('1');
	} else if (memcmp( net_buf+1, "Sg", 2)==0 ) {
		Serial.print("O");
		delay(1);
		cliente.print('1');
	} else 	if (memcmp( net_buf+1, "RS#", 3)==0 ) {
		Serial.print("R");
		//cliente.print('1');
	} else 	if (memcmp( net_buf+1, "SG", 2)==0 ) {
		Serial.print("T");
		delay(1);
		cliente.print('1');
	} else 	if (memcmp( net_buf+1, "SL", 2)==0 ) {
		Serial.print("t");
		delay(1);
		cliente.print('1');
	} else if (memcmp( net_buf+1, "SC", 2)==0 ) {
		Serial.print("t");
		delay(10);
		//cliente.print('1');
		//delay(50);
		cliente.print("1Updating Planetary Data#                              #");
		//cliente.print("                              #");
	} else 	if (memcmp( net_buf+1, "Sw", 2)==0 ) {
		cliente.print('1');
	} else 	if (memcmp( net_buf+1, "Me#", 3)==0 ) {
		MotorRA.enableOutputs();
		MotorRA.setMaxSpeed(MotorMaxSpeed);
		MotorRA.setAcceleration(MotorMaxAcceleration);
		MotorRA.moveTo(MotorRA.currentPosition()-10000);
	} else 	if (memcmp( net_buf+1, "Mn#", 3)==0 ) {
		MotorDEC.enableOutputs();
		MotorDEC.setMaxSpeed(MotorMaxSpeed);
		MotorDEC.setAcceleration(MotorMaxAcceleration);
		MotorDEC.moveTo(MotorDEC.currentPosition()-10000);
	} else 	if (memcmp( net_buf+1, "Ms#", 3)==0 ) {
		MotorDEC.enableOutputs();
		MotorDEC.setMaxSpeed(MotorMaxSpeed);
		MotorDEC.setAcceleration(MotorMaxAcceleration);
		MotorDEC.moveTo(MotorDEC.currentPosition()-10000);
	} else 	if (memcmp( net_buf+1, "Mw#", 3)==0 ) {
		MotorRA.enableOutputs();
		MotorRA.setMaxSpeed(MotorMaxSpeed);
		MotorRA.setAcceleration(MotorMaxAcceleration);
		MotorRA.moveTo(MotorRA.currentPosition()+10000);
	} else 	if (memcmp( net_buf+1, "Qe#", 3)==0 ) {
		//cliente.print('1');
		MotorRA.stop();
	} else 	if (memcmp( net_buf+1, "Qn#", 3)==0 ) {
		MotorDEC.stop();
		//cliente.print('1');
	} else 	if (memcmp( net_buf+1, "Qs#", 3)==0 ) {
		MotorDEC.stop();
		//cliente.print('1');
	} else 	if (memcmp( net_buf+1, "Qw#", 3)==0 ) {
		MotorRA.stop();
		//cliente.print('1');
	} else 	if (memcmp( net_buf+1, "Q#", 2)==0 ) {
		MotorRA.stop();
		MotorDEC.stop();
		//cliente.print('1');
	}
}

