# README - Projeto CeamigWiFi #
O projeto CeamigWiFi é um projeto open-source para inicialmente automatizar os telescópios feitos na oficina ATM (Amateur Telescope Maker) do Ceamig.

Estamos usando o processador ESP8266 como base, por ser rápido e contar com interface WiFi. Isso permite ligar praticamente todos os programas de planetários (Skysafari, Stelarium, Chartes du Ciel, The Sky, etc.) ao telescópio utilizando conexão sem fio pelo WiFi.

O projeto usa motores de passo para acionar o telescópio. As engrenagens e caixas de redução necessárias são também parte do projeto e são projetadas no OpenSCAD e impressas por impressoras 3D.

Uma das principais restrições do projeto é manter o preço bem baixo, abaixo de R$300 para todo o projeto de automação.

### Contato ###

marcos.ackel@ceamig.org.br

http://www.ceamig.org.br

***
***


This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact