// Telescope.ino

#include "Telescope.h"

// Para usar o timer do ESP8266:
extern "C" {
	#include "user_interface.h"
}
os_timer_t myTimer;
//--

const uint8_t MotorAlt_PinSTP   = 15;    // Motor de Altura
const uint8_t MotorAlt_PinDIR   =  0;
const uint8_t MotorAlt_PinSPEED =  5;
const uint8_t MotorAzm_PinSTP   = 12;    //15 Motor de Azimute
const uint8_t MotorAzm_PinDIR   = 14;  //0 2;
const uint8_t MotorAzm_PinSPEED = 16;


// Usados somente na inicializacao, sem configHw:
const double GearRatio = 191.0/11.0 * 4.8 * 4.8;   // #dentes engr Grande / #dentes engr Pequena * relacao caixa reducao

// Eixos: Declinacao/Altura
//        Ascensao Reta/Azimute

AstroMotor MotorAlt( MotorAlt_PinSTP, MotorAlt_PinDIR, MotorAlt_PinSPEED );
AstroMotor MotorAzm( MotorAzm_PinSTP, MotorAzm_PinDIR, MotorAzm_PinSPEED );

SiderealClock TelSiderealClock(0.0);	// sidereal clock for the telescope (usado no callback to timer)
									// nao pode ser membro da classe	

//----------------------------------------------------

Telescope::Telescope( TelModel model, TelType type ) 
	: _state(STOPPED), _debugFlag(false)
{
	_model = model;
	_type  = type;
	pinMode(MotorAlt_PinSPEED, OUTPUT);
	pinMode(MotorAlt_PinSPEED, OUTPUT);
	pinMode(MotorAzm_PinSPEED, OUTPUT);
	pinMode(MotorAzm_PinSPEED, OUTPUT);

	// Ajusta o relogio para iniciar no ano 2001 (por causa do algoritimo de CalcSiderealTime()
	// Nao ajusta para 2000 (valor inicio de CalcSiderealTime()) por causa do dia sideral iniciar as 12h UT
	// Se nao fizer esse ajusta, valores antes de ajustar a hora real dariam hora sideral negativa.
	setTime(0,0,0,1,1,2001); 
	
	_hasDate = _hasTime = _hasLongitude = _hasTimeZone = false;    // usado para ajustar o clock Sideral

	// erro eixos montagem: inicialmente zero (vao ser atualizados com o alinhamento por 2-estrelas
	_erroEixoDec = _erroEixoRA = 0.0;

	_curSpeed = TRACK;

	// Parametros provisorios (antes do initTelescope():
	//_RAStepsPerRev  = GearRatio * 200 * 16;   		// Motor Azm/RA, em 1/16 microsteps (motor: 200 steps/rev)
	//_RAStepSize  = 360 * 3600 / _RAStepsPerRev;		// tamanho de um step em segundos de arco
	//_DecStepsPerRev = _RAStepsPerRev;				// Motor de Alt/Dec
	//_DecStepSize = 360 * 3600 / _DecStepsPerRev;  
	
	//CoordEquatorial trackEq(0.0, decPolo); 
	//_trackTarget = trackEq;
	setState( STOPPED );
}
//----------------------------------------------------

void Telescope::initTelescope( ConfigHw_st *stConfig ) 
{
	_stConfig = stConfig;	// armazena parametros de configuracao

	_model = stConfig->telescopeModel;
	_type  = stConfig->telescopeType;
	
	initMotors();

	_RAStepsPerRev  = stConfig->RAStepsPerRev;
	_DecStepsPerRev = stConfig->DecStepsPerRev;
	_RAStepSize  = 360 * 3600  / _RAStepsPerRev;		// tamanho de um step em segundos de arco
	_DecStepSize = 360 * 3600 / _DecStepsPerRev;  
	
	// LONGITUDE E TIMEZONE vem do SkySafari (junto com hora/data)
	// POR ENQUANTO Pega dos dados da mem Flash (configHw).
	setLongitude( stConfig->longitude );
	setTimeZone(  stConfig->timeZone );
	double stnow = CalcSiderealTime( now(), _longitude, _timeZone );
	TelSiderealClock.setSiderealTime( stnow );	

	double decPolo = setLatitude( stConfig->latitude ); 

	CoordEquatorial trackEq(0.0, decPolo); 
	if (_type != ALTAZ) {
		//Serial.println(stnow,4);
		trackEq.setRA( stnow );
		double delta = (decPolo > 0)? -0.0001 : +0.0001;      // para nao fazer track diretamente no polo (zero/zero)
		trackEq.setDec( trackEq.getDec() + delta );
	}
	_trackTarget = trackEq;

	initUser();		// inicializa a rotina de interrupcao
	
	setState( TRACKING );   // inicia acompanhamento (supoe estar apontado para o polo)
}
//----------------------------------------------------

void Telescope::initMotors()
{
	_invertedAzimuth = _stConfig->inverteAzimute; 
	MotorAzm.setInvertedMotorDirection( _invertedAzimuth );  
	_invertedAltitude = _stConfig->inverteAltura;
	MotorAlt.setInvertedMotorDirection( _invertedAltitude );  

	float maxAzmSpeed = _stConfig->slewSpeedAzimute;  
	float maxAltSpeed = _stConfig->slewSpeedAltura;  
	float maxAzmAccel = _stConfig->aceleracaoAzimute;  
	float maxAltAccel = _stConfig->aceleracaoAltura;  
	
	setMaxSpeed(MOTORAZM, maxAzmSpeed);
	setAcceleration(MOTORAZM, maxAzmAccel);
	MotorAlt.setMicrostep( (_stConfig->isAltUSteps16) ? 16 : 32 ); 
	MotorAlt.setCurrentPosition( 0 );	
	
	setMaxSpeed(MOTORALT, maxAltSpeed);
	setAcceleration(MOTORALT, maxAltAccel);
	MotorAzm.setMicrostep( (_stConfig->isAzmUSteps16) ? 16 : 32 );
	MotorAzm.setCurrentPosition( 0 );	
}
//----------------------------------------------------
// setLatitude
// formato: -90 a +90
// returns: declinacao do Polo Celeste

double Telescope::setLatitude(double latitude)
{
	_latitude = latitude;
	//Serial.printf("Latitude: %.3f\n", _latitude);

	double decPolo = +90.0; // declinacao do polo celeste norte
	if (_latitude < 0)
		decPolo = -90.0;
		
	return decPolo;
}
//----------------------------------------------------

// setLongitude
// formato: 0 a 360 graus

void Telescope::setLongitude(double longitude)
{
	_longitude = longitude;
	//Serial.printf("Longitude: %.3f\n", _longitude);
}
//----------------------------------------------------

void Telescope::setTimeZone(double timeZone)
{
	_timeZone = timeZone;
	//Serial.printf("TimeZone: %.3f\n", _timeZone);
}
//----------------------------------------------------

void Telescope::setMaxSpeed(TelMotor motor, float speed)
{
	if (motor == MOTORAZM) {
		MotorAzm.setMaxSpeed( speed );
	} else if (motor == MOTORALT) {
		MotorAlt.setMaxSpeed( speed );
	}
}
//----------------------------------------------------

void Telescope::setAcceleration(TelMotor motor, float accel)
{
	if (motor == MOTORAZM) {
		MotorAzm.setAcceleration( accel );
	} else if (motor == MOTORALT) {
		MotorAlt.setAcceleration( accel );
	}
}
//----------------------------------------------------

float Telescope::getMaxSpeed(TelMotor motor)
{
	float spd = 0;
	if (motor == MOTORAZM) {
		spd = MotorAzm.getMaxSpeed();
	} else if (motor == MOTORALT) {
		spd = MotorAlt.getMaxSpeed();
	}
	return spd;
}
//----------------------------------------------------

float Telescope::getAcceleration(TelMotor motor)
{
	float acc = 0;
	if (motor == MOTORAZM) {
		acc = MotorAzm.getAcceleration();
	} else if (motor == MOTORALT) {
		acc = MotorAlt.getAcceleration();
	}
	return acc;
}
//----------------------------------------------------

Coord Telescope::horizontalToMotor( CoordHorizontal &coord )
{
	//Serial.println( coord.toString() );
	// O polo e' sempre coordenadas 0,0 dos motores AINDA NAO...
	double altPolo = (_latitude < 0.0)? -_latitude : _latitude;  // altura do polo sobre o horizonte [graus]
	if (_type != ALTAZ) altPolo = 90.0-0.001;		// montagens equatoriais (apontar o "zenite" menos um segundo de arco para o polo).
	
	double posMAlt = (coord.getAlt() - altPolo) * _DecStepsPerRev / 360.0;
	//double posMAlt = (20.0 + _latitude);   se colocar (20.0 - _latitude), o tempo aumenta (!!???)
	
	double azm = coord.getAzm();	// azimute sempre entre zero e 360 graus.
	double deslocAzm = 0;		// deslocamento do Azimute (para o zero dos motores coincidir com o polo)
	if (_latitude < 0.0) {
		deslocAzm = 180.0;      // hemisferio sul: motores: positivo clockwise, negativo, counterCW
	} else {
		deslocAzm = (azm > 180)? 360 : 0; // hemisferio norte: motores: positivo clockwise, negativo counterCW
	}
	//if (_type == GERMAN) deslocAzm = 0; //-90.0;
	//Serial.print(coord.getAz());Serial.print(" ");Serial.print(deslocAz);Serial.print(" ");Serial.println(DecStepsPerRev);
	double posMAzm  = (azm - deslocAzm) * _RAStepsPerRev / 360.0;
	Coord motorCoords(posMAzm, posMAlt);
	//Serial.println(motorCoords.toString());
	return motorCoords;
	
}
//----------------------------------------------------

CoordHorizontal Telescope::motorToHorizontal()
{
	double altPolo = (_latitude < 0.0)? -_latitude : _latitude; // altura do polo sobre o horizonte [graus]
	if (_type != ALTAZ) altPolo = 90.0-0.001;		// montagens equatoriais (apontar o "zenite" para o polo).

	double angMotAlt = (MotorAlt.currentPosition() * 360.0 / _DecStepsPerRev);	// ang motor altura
	double angAlt = angMotAlt + altPolo;
					//double addAzm = (angAlt > 90)? 180 : 0;
	double alt = (angAlt > 90)? 180-angAlt : angAlt;		// 180:  90-(angAlt-90) 
	
	double deslocAzm = (_latitude < 0)? 180 : 0; 
	double azm;
	double angMotAzm = (MotorAzm.currentPosition() * 360.0 / _RAStepsPerRev);
	//if (_type == GERMAN) deslocAz = -90.0;
	double addAzm = (angAlt > 90)? 180 : 0;
	if (_type == ALTAZ) {	
		azm =  angMotAzm + deslocAzm + addAzm;
	} else { 
		if (angMotAlt >= 0) {
			deslocAzm = (angMotAzm > 0)? 360 : 0;
			azm = deslocAzm - angMotAzm;
		} else {
			//double sinal = (angAlt > 0)? +1 : -1;
			azm = 180 - angMotAzm;
		} 
	}
	azm = (azm < 0)? azm+360 : azm; 			// azimute sempre entre 0 e 360 graus
	azm = (azm >= 360)? fmod(azm, 360) : azm;

	return CoordHorizontal( azm, alt );
}
//----------------------------------------------------

void Telescope::move( TelDirection dire, TelSpeed speed )
{
	setState( MOVING );
	clearOnTargetWarning();
	
	switch (dire) {
		case N:   // Norte ou UP
			MotorAlt.move( speed, AstroMotor::CLOCKWISE ); 
			//Serial.printf("mov(): Dir: %c  Speed: %d  MAltPos: %ld  MAzm: %ld\n", dire, speed, MotorAlt.currentPosition(), MotorAzm.currentPosition());
			break;
		case S:   // Sul ou Down
			MotorAlt.move( speed, AstroMotor::COUNTERCLOCKWISE );
			break;
		case E:		// East ou Right
			MotorAzm.move( speed, AstroMotor::CLOCKWISE );
			break;
		case W:		// West ou Left
			MotorAzm.move( speed, AstroMotor::COUNTERCLOCKWISE );
			break;
	}
}
//----------------------------------------------------

void Telescope::moveTo( CoordHorizontal azAlt, TelSpeed speed)
{
	//Serial.println( altAz.toString() );
	Coord mcoo = horizontalToMotor( azAlt );
	//Serial.println( mcoo.toString() );

	if (!_segundoSlew)
		clearOnTargetWarning();

	double distAlt = mcoo.getY() - MotorAlt.currentPosition();
	TelSpeed speedAlt = speed;
	if (abs(distAlt) < 360 * _DecStepSize)
		speedAlt = MotorSpeed::TRACK;
	MotorAlt.moveTo( speedAlt, mcoo.getY() );
	
	double RAStepsPerHalfRev = _RAStepsPerRev / 2.0;
	
	TelSpeed speedAzm = speed;
	double posAzm = MotorAzm.currentPosition();
	double newAzm = mcoo.getX();
	double distAzm = abs( newAzm - posAzm );
	if (distAzm > RAStepsPerHalfRev) {
		if ( posAzm < 0 )
			mcoo.setX( -2* RAStepsPerHalfRev + newAzm );
		else
			mcoo.setX(  2* RAStepsPerHalfRev + newAzm );
	}
	newAzm = mcoo.getX();
	distAzm = abs( newAzm - posAzm );
	if ( distAzm < 360 * _RAStepSize)
		speedAzm = MotorSpeed::TRACK;
	MotorAzm.moveTo( speedAzm, newAzm );
	if (_debugFlag) {
		Serial.println(newAzm);
	}
	//if ( speedAlt == MotorSpeed::SLEW || speedAzm == MotorSpeed::SLEW )
	//	setState( MOVING );
	//Serial.print(distAlt);Serial.print(" ");Serial.println(distAzm);
	
}
//----------------------------------------------------

void Telescope::moveTo( CoordEquatorial equat, TelSpeed speed )
{
	//Serial.println( equat.toString() );
	moveTo( convToHorizontal( equat ), speed );
}
//----------------------------------------------------

void Telescope::slewTo( CoordEquatorial equat )
{
	_trackTarget = equat;
	//Serial.print("_slewTarget: ");Serial.print(_slewTarget.toString());Serial.print(" = "); Serial.println(_slewTarget.toStringF());
	//CoordHorizontal coordToSlew = convToHorizontal(equat);
	//Serial.print("coordToSlew (Az,Alt): ");Serial.print(coordToSlew.toString());Serial.print(" = "); Serial.println(coordToSlew.toStringF());
	moveTo( convToHorizontal( equat ), SLEW );
	setState( MOVING );	
}
//----------------------------------------------------

bool Telescope::onTargetWarning(void)
{
	bool retOnTarget = false;
	if (MotorAlt.onTargetWarning() && MotorAzm.onTargetWarning() ) {
		// se foi um longo slew (mais de 10-15 segundos, o objeto pode ja' ter 
		// se movido no campo. Fazer outro slew, que agora deve ser bem curto, para 
		// centrar o objeto no campo.
		// Isso ocorre porque os motores estao se movendo para a posicao de motor
		// calculada e nao para a RA e DEC do objeto alvo.
		if (!_segundoSlew) {
			// segundo Slew:
			_segundoSlew = true;
			slewTo( _slewTarget );
		} else {
			// acabou o segundo slew. OK:
			_segundoSlew = false;
			retOnTarget = true;
		}
	}
	return retOnTarget;
}
//----------------------------------------------------
void Telescope::clearOnTargetWarning(void)
{
	MotorAlt.clearOnTargetWarning();
	MotorAzm.clearOnTargetWarning();
}
//----------------------------------------------------

CoordHorizontal Telescope::convToHorizontal( CoordEquatorial equat )
{
	//double st = CalcSiderealTime( now(), _longitude, _timeZone );
	double st = TelSiderealClock.getSiderealTime();
	double lat = _latitude;
	if ( _type != ALTAZ )
		lat = (_latitude < 0)? -90.0 : +90.0;
		
	CoordHorizontal azAlt = equat.toHorizontal( lat, st );
	if ( isnan( azAlt.getAzm() ) )
		azAlt.setAzm(0.0);
	return azAlt;
}
//----------------------------------------------------

CoordEquatorial Telescope::convToEquatorial( CoordHorizontal azAlt )
{
	//double st = CalcSiderealTime( now(), _longitude, _timeZone );
	double st = TelSiderealClock.getSiderealTime();
	double lat = _latitude;
	if ( _type != ALTAZ )
		lat = (_latitude < 0)? -90.0 : +90.0;

//	Serial.print("=== ");Serial.print(lat);Serial.print(" ");Serial.println(azAlt.toStringF(3));
	CoordEquatorial equat = azAlt.toEquatorial( lat, st );
//	Serial.print("... ");Serial.println(equat.toStringF(3));
	return equat;
}
//----------------------------------------------------

void Telescope::stop( TelDirection dire )
{
	switch (dire) {
		case N:
		case S:
			MotorAlt.stop();	// stop nao para imediatamente. Faz a desaceleracao para parar.
			//Serial.println("Halt N/S"); Serial.printf("MAlt: %ld  MAz: %ld\n", MotorAlt.currentPosition(), MotorAzm.currentPosition());
			break;
		case E:
		case W:
			MotorAzm.stop();
			break;
	}
	_trackTarget = getEquatorialPosition();
	setState( STOPPING );
}
//----------------------------------------------------

void Telescope::stop( TelMotor motor )
{
	if ( motor == MOTORALT ) {
		MotorAlt.stop();
	} else if ( motor == MOTORAZM ) {
		MotorAzm.stop();
	}
	_trackTarget = getEquatorialPosition();
	setState( STOPPING );
}
//----------------------------------------------------

void Telescope::stop( void )
{
	MotorAlt.stop();
	MotorAzm.stop();	
	setState( STOPPING );
}
//----------------------------------------------------

void Telescope::alignTelPosition( CoordEquatorial newPosEq )
{
	//long ini = micros();
	//double st = CalcSiderealTime( now(), _longitude, _timeZone );
	double st = TelSiderealClock.getSiderealTime();
	CoordHorizontal azAlt = convToHorizontal( newPosEq );
	Coord motorsPos = horizontalToMotor( azAlt );
	MotorAzm.setCurrentPosition( motorsPos.getX() );
	MotorAlt.setCurrentPosition( motorsPos.getY() );
	_trackTarget = newPosEq;
	//long dur = micros() - ini;
	//Serial.print("--- Micros2:"); Serial.println(dur);
	//Serial.print("Eq:");Serial.println(newPosEq.toString());
	//Serial.print("Lat:");Serial.println(_latitude);
	//Serial.print("AAz:");Serial.println(altaz.toString());
	//Serial.print("mx:");Serial.print( motorsPos.getX() );
	//Serial.print("my:");Serial.println( motorsPos.getY() );
}
//----------------------------------------------------

String Telescope::getStringTelRA()
{
	//String sra = getEquatorialPosition().raToString();
	//return sra;
	
	//long ini = micros();
	//tmElements_t tm;
	//breakTime( now(), tm);
	//char buf[50] = {0};
	//sprintf(buf, "tm=%d/%02d/%02d %02d:%02d:%02d \n", tm.Year, tm.Month, tm.Day, tm.Hour, tm.Minute, tm.Second);
	//Serial.print(buf); Serial.print("l:");Serial.print(_longitude);
	//Serial.print(" z:");Serial.print(_timeZone);
	//double st = CalcSiderealTime( now(), _longitude, _timeZone );
	//double st = TelSiderealClock.getSiderealTime();
	//Serial.print(" lo:"); Serial.print(_longitude);

	//CoordAltAz aaz = motorToAltAz();
	//Serial.print(" AltAz:"); Serial.println(aaz.toString());
	//Serial.print(" st:"); Serial.println(st,8);
	//Serial.print(" lat:"); Serial.println(_latitude,4);
	//CoordEquatorial eq = convToEquatorial( aaz );
	
	//Serial.print(" Eq:"); Serial.println(eq.toString());
	
	//String sra = eq.raToString();
	String sra = _positionEq.raToString();
	//long dur = micros() - ini;
	//Serial.print("Micros1:"); Serial.println(dur);
	//Serial.printf("RA: %s  ", sra);
	return sra;
	
}
//----------------------------------------------------

String Telescope::getStringTelDec()
{
	//String sde = getEquatorialPosition().decToString();
	
	//double st = TelSiderealClock.getSiderealTime(); //CalcSiderealTime( now(), _longitude, _timeZone );
	//String sde = convToEquatorial( motorToAltAz() ).decToString();
	String sde = _positionEq.decToString();		//Serial.printf("Dec: %s \n", sde);
	
	return sde;
}
//----------------------------------------------------

bool Telescope::spiralSearch( double diameterMinArc, bool direction )
{
	_spiralDiameter = diameterMinArc * 60.0;    // em segundos de arco
	_spiralDiameter *= 0.9;    //90%
	double toRad = 3.14159265358979 / 180.0;   // constante PI / 180
	double diamRad = _spiralDiameter / 3600 * toRad;			// diametro do campo desejado em radianos
	
	// converte para numero de microsteps:
	_spiralDiameter /= _RAStepSize;			// deveria ter um diametro para cada eixo. Se as relacoes forem diferentes vai rodar errado!

	double altRad = _positionHoriz.getAlt() * toRad;	// altura em radianos

	// Fator de Azimute: quanto mais perto do zenite, mais tem que deslocar (em steps) o motor de 
	// azimute para percorrer o diametro da espiral desejado. Entao tem que aumentar a velocidade
	// do motor de az para que a velocidade em az seja aprox. igual a vel de deslocamento em altura.
	// Usa a formula de distancia entre dois pontos:
	// cos(diamRad) = sin(alt1)*sin(alt2) + cos(alt1)*cos(alt2)*cos(az1-az2)   , para alt1 = alt2

	//_spiralAzmFactor = acos( ( cos(diamRad) - pow(sin(altRad),2) ) / pow(cos(altRad),2) ) / toRad;
	_spiralAzmFactor = acos( ( cos(diamRad) - pow(sin(altRad),2) ) / pow(cos(altRad),2) );
	_spiralAzmFactor /= toRad;   // para radianos
	Serial.print("Fact: ");Serial.println(_spiralAzmFactor);
	
	if (_spiralAzmFactor > 10.0) {
		// 10.0 corresponte a aproximadamente 84 graus de altura
		// Significa que o deslocamento do motor de azimute vai ser 10 vezes maior 
		//   que o do motor de altura
		return false;		// Nao inicia  busca espiral -> muito perto do zenite 
	}

	_spiralDirection = direction;

	_spiralCount = 0;
	_spiralPos   = 0;

	//_spiralLastNode.setXY( MotorAlt.currentPosition(), MotorAzm.currentPosition() ); 
	_spiralLastNode = _positionMotors;
	_spiralNextNode = _positionMotors;
	_spiralStart    = _positionMotors;

	//Serial.print("SPIRAL - diam:");
	//Serial.println(_spiralDiameter);
	
	setState(SPIRAL);
	return true;
}
//----------------------------------------------------

bool Telescope::spiralNextTarget(void)
{
	const uint8_t maxSpiralCount = 3;   // maximo de 3 circulos completos (3 diametros)

	if (_spiralPos == 0)
		_spiralCount++;		// contador de voltas
	if (_spiralCount > maxSpiralCount)
		return false;

	double nDiam = (_spiralDirection) ? _spiralDiameter : -_spiralDiameter;
	double nSize = nDiam * _spiralCount;
	double nHalf = nSize / 2.0;
	double nFAzm = _spiralAzmFactor;
	
	Coord desloc(0, 0);
	
	switch (_spiralPos) {
		case 0:
			// proximo alvo:
			desloc.setXY( nDiam, 0 );
			_spiralPos = 1;	
			break;
		case 1:
			desloc.setXY( 0, nHalf * nFAzm );
			_spiralPos = 2;
			break;
		case 2:
			desloc.setXY( -nHalf, +nHalf * nFAzm );
			_spiralPos = 3;
			break;
		case 3:
			desloc.setXY( -nSize, 0 );
			_spiralPos = 4;
			break;
		case 4:
			desloc.setXY( -nHalf, -nHalf * nFAzm );
			_spiralPos = 5;
			break;
		case 5:
			desloc.setXY( 0, -nSize * nFAzm );
			_spiralPos = 6;
			break;
		case 6:
			desloc.setXY( +nHalf, -nHalf * nFAzm );
			_spiralPos = 7;
			break;
		case 7:
			desloc.setXY( nSize, 0 );
			_spiralPos = 8;
			break;
		case 8:
			desloc.setXY( +nHalf, +nHalf * nFAzm );
			_spiralPos = 9;
			break;
		case 9:
			desloc.setXY( 0, +nHalf * nFAzm );
			_spiralPos = 0;
			break;
	}
	Coord oTmp(_spiralNextNode.getX(), _spiralNextNode.getY());

	_spiralNextNode.setY( _spiralNextNode.getY() + desloc.getY() ); 
	_spiralNextNode.setX( _spiralNextNode.getX() + desloc.getX() ); 
	//Serial.print(" === Next= ");Serial.print(_spiralNextNode.toString()); 
	_spiralLastNode.setXY( oTmp );

	bool ret = (_spiralCount <= maxSpiralCount);
	if (!_spiralDirection && _spiralLastNode == _spiralStart && _spiralLastNode != _spiralNextNode)
		ret = false;		// encerra a busca se chegou de volta na posicao inicial.
	//return (_spiralPos <= 1);
	return (_spiralCount < maxSpiralCount);
}
//----------------------------------------------------

void Telescope::spiralCancel(void)
{
	stop();
}
//----------------------------------------------------

void Telescope::spiralPause(void)
{
	stop();
	setState( SPIRAL_PAUSED );	// tem que ser depois do stop para evitar o estado STOPPING
}
//----------------------------------------------------

void Telescope::spiralContinue(void)
{
	if (getState() == SPIRAL_PAUSED)
		setState( SPIRAL );
}
//----------------------------------------------------

void Telescope::spiralInvertDirection(void)
{
	_spiralDirection = !_spiralDirection;
	Coord oTmp = _spiralLastNode;
	_spiralLastNode = _spiralNextNode;
	_spiralNextNode  = oTmp;
}
//----------------------------------------------------

String Telescope::makeStringFromCmd( char* cmd )
{
	int len = 0;
	// procura o final do comando ('#')
	//char* pos = (char*) memchr( cmd, '#', strlen(cmd) );
	int lenCmd = strlen( cmd );
	//Serial.print( "cmd=" ); Serial.println(cmd);
	//Serial.print("lenCmd=");Serial.println(lenCmd); Serial.flush();
	char *pos = cmd;
	while ( *pos != '#' && pos-cmd < lenCmd ) {
		pos++;
	}
	if (pos - cmd < lenCmd) {
		len = pos - cmd + 1;
	}
	String ret = makeString( cmd, len );
	//Serial.print("len="); Serial.println(len);
	//Serial.print("mks="); Serial.println(ret); 
	return ret;
}
//----------------------------------------------------

void VerifyTelSiderealClock()
{
	telescope.verifySiderealClock();	
}
//----------------------------------------------------
void Telescope::verifySiderealClock(void)
{
	double stnow = CalcSiderealTime( now(), _longitude, _timeZone );
	double stclock = TelSiderealClock.getSiderealTime();
	TelSiderealClock.setSiderealTime( stnow );  // reseta o clock sideral para o valor calculado
	double diff  = stnow - stclock;
	//yield();
	Serial.printf("  STclock: %.8f  Diferenca de ST (ST - STclock): %.6f\n", stclock, diff);
	//yield();
}
//----------------------------------------------------
void RecalcTelPosition()
{
	telescope.recalcPosition();
}
//----------------------------------------------------

void Telescope::recalcPosition(void)
{
	_positionMotors.setXY(MotorAzm.currentPosition(), MotorAlt.currentPosition());
	_positionHoriz = motorToHorizontal();
	//double st = TelSiderealClock.getSiderealTime();
	_positionEq = convToEquatorial( _positionHoriz );

	bool onTarget = (MotorAlt.distanceToGo() == 0 && MotorAzm.distanceToGo() == 0);

	TelState state = getState();
	TelState prevState = getPrevState();

	// Para Debug:
	//if (state != TRACKING) {
	//	Serial.print("S:");Serial.println(state);
	//}
	
	if ( state == TRACKING ) {
		moveTo(_trackTarget, TRACK);

	} else if ( state == MOVING && onTarget ) {
		_trackTarget = _positionEq;
		setState( TRACKING );

	} else if ( state == STOPPING && onTarget) {
		//if (getPrevState() == SPIRAL_PAUSED) {
		//	MotorAzm.moveTo(CENTER, _spiralNextNode.getY());
		//	MotorAlt.moveTo(CENTER, _spiralNextNode.getX());
		//} else {
			_trackTarget = _positionEq;
			setState( TRACKING );
		//}
	} else if ( state == SPIRAL ) {
			//moveTo(_spiralTarget, CENTER);    // continua para o alvo anterior depois da pausa
		if (onTarget) {
			bool continua = spiralNextTarget();
			if (continua) {
				//Serial.print("Last= ");Serial.println(_spiralLastNode.toString());
				//Serial.print("Next: ");Serial.print(_spiralNextNode.toString()); Serial.print(" P=");Serial.println(_spiralPos);
				//MotorAzm.moveTo(CENTER, _spiralNextNode.getY());
				//MotorAlt.moveTo(CENTER, _spiralNextNode.getX());				
				MotorAzm.freeSpeedMoveTo( 60 * _spiralAzmFactor, _spiralNextNode.getX());
				MotorAlt.freeSpeedMoveTo( 60, _spiralNextNode.getY());
			} else {
				spiralCancel();
			}
		}
	} else if ( state == SPIRAL_PAUSED ) {
		if (onTarget) {
			_trackTarget = _positionEq;
			moveTo(_positionEq, TRACK);
		}
	}
}
//----------------------------------------------------

// start of timerCallback  ===========================
byte timerCnt = 0;
byte timerCntMain = 0;          // contador pricipal, para calcular a posicao do telescopio 
uint32_t  timerCntMinute = 0;   // contador para 1 minuto 60 segundos ou 60000 milisegundos
void timerCallback(void *pArg) 
{
	// Timer callback interval: 1ms
	
	// Adjust Sidereal Clock: once each 10ms 
	++timerCnt;
	++timerCntMain;
	++timerCntMinute;
	if (timerCnt >= 10) {
		TelSiderealClock.adjust( SiderealClock::SiderealMillisecond * 10.0 );
		timerCnt = 0;
		if (timerCntMain >= 100) {
			RecalcTelPosition();   // Recalculate and update telescope position (100 millis = 0.1 sec = 10x per sec)
			timerCntMain = 0;
			//yield();
		}
		if (timerCntMinute >= 60007) {    // 60 segundos + 7 milis (primo) ...   3600000) {
			VerifyTelSiderealClock();   // verify and adjust telescope sidereal clock
			timerCntMinute = 0;
		}
	}

	// Recalculate motors positions:
	MotorAzm.run();
	MotorAlt.run();
	
} // End of timerCallback
//----------------------------------------------------

void Telescope::initUser(void) {
 /* os_timer_setfn - Define a function to be called when the timer fires
			void os_timer_setfn( os_timer_t *pTimer, os_timer_func_t *pFunction, void *pArg)
  	Define the callback function that will be called when the timer reaches zero. 
  	The pTimer parameters is a pointer to the timer control structure.
	The pFunction parameters is a pointer to the callback function.
	The pArg parameter is a value that will be passed into the called back function. 
	The callback function should have the signature:  void (*functionName)(void *pArg)
	The pArg parameter is the value registered with the callback function.
*/
	os_timer_setfn(&myTimer, timerCallback, NULL);

/* os_timer_arm -  Enable a millisecond granularity timer.
			void os_timer_arm( os_timer_t *pTimer, uint32_t milliseconds, bool repeat)
	Arm a timer such that is starts ticking and fires when the clock reaches zero.
	The pTimer parameter is a pointed to a timer control structure.
	The milliseconds parameter is the duration of the timer measured in milliseconds. 
	The repeat parameter is whether or not the timer will restart once it has reached zero.
*/
	os_timer_arm(&myTimer, 1, true);
} // End of InitUser()
//----------------------------------------------------

bool Telescope::setSiderealClock( double st )
{
	bool dataOk = false;
	if (_hasTime && _hasDate && _hasTimeZone && _hasLongitude) {
		TelSiderealClock.setSiderealTime( CalcSiderealTime(st, _longitude, _timeZone) );
		dataOk = true;
	}
	return dataOk;
}
//----------------------------------------------------
bool Telescope::processaCmd( char* net_buf, uint16_t buf_size )
{
	bool cmdOk = false;
	
	if ( _model == MEADE ) {
		// Alguns programas podem enviar mais de um comando na mesma requisicao (mais de um ":" no buffer)
      	for ( byte idx = 0; idx < buf_size; idx++) {
			if (net_buf[idx] == ':') {
				//Serial.print(" x");
				cmdOk = telescope.processaCmdLX200( (net_buf+idx) );
				if (!cmdOk) {
					// comando desconhecido. Print:
					Serial.write(net_buf, buf_size);
					Serial.println("");
				}
				// vai ate' a '#' (final de comando LX200):
				while (net_buf[idx] != '#' && idx < buf_size)
					idx++;
				delay(1);
			}
		}	
	} else if ( _model == CELESTRON ) {
		cmdOk = processaCmdCELESTRON( net_buf );
	}
	
	return cmdOk;
}
//----------------------------------------------------

byte count = 0;

bool Telescope::processaCmdLX200( char *net_buf_org ) 
{
	bool cmdOk = true;
	char net_buf[50];
	strcpy(net_buf, net_buf_org);	// para poder modificar o cmd (facilita processamento)
	if (memcmp( net_buf+1, "GR#", 3)==0 ) {				// Get RA
		//if (count++ < 10) { Serial.print("G"); Serial.print(" ");String s=getStringTelRA();Serial.print(s); }
		//String sra = getStringTelRA(); Serial.print("G "); Serial.println(sra);
		cliente.print( getStringTelRA() );
	} else	if (memcmp( net_buf+1, "GD#", 3)==0 ) {		// Get Dec
		//Serial.print("D");
		//String sde = getStringTelDec(); Serial.print("D "); Serial.println(sde);
		cliente.print( getStringTelDec() );
	} else 	if (memcmp( net_buf+1, "CM#", 3)==0 ) {		// Align
		//cliente.print(" M31 EX GAL MAG 3.5 SZ178.0'#"); // autostar fixed string 
		//Serial.print("CM#  ");
		alignTelPosition( _slewTarget );
		cliente.print( "Coordinates     matched.        #" ); 
	} else 	if (memcmp( net_buf+1, "MS#", 3)==0 ) {		// Slew to target
		//Serial.print("MS#");
		slewTo( _slewTarget );
		cliente.print('0');  // slew is possible
	} else 	if (memcmp( net_buf+1, "Me#", 3)==0 ) {		// Move East
		move( E, getCurSpeed() ); //Serial.println("E");
	} else 	if (memcmp( net_buf+1, "Mn#", 3)==0 ) {		// Move North
		move( N, getCurSpeed() ); //Serial.println("N");
	} else 	if (memcmp( net_buf+1, "Ms#", 3)==0 ) {		// Move South
		move( S, getCurSpeed() );
	} else 	if (memcmp( net_buf+1, "Mw#", 3)==0 ) {		// Move West
		move( W, getCurSpeed() );
	} else 	if (memcmp( net_buf+1, "Qe#", 3)==0 ) {		// Halt eastward slew
		stop( E );
	} else 	if (memcmp( net_buf+1, "Qn#", 3)==0 ) {		// Halt northward slew
		stop( N );  
	} else 	if (memcmp( net_buf+1, "Qs#", 3)==0 ) {		// Halt southward slew
		stop( S );
	} else 	if (memcmp( net_buf+1, "Qw#", 3)==0 ) {		// Halt westward slew
		stop( W );
	} else 	if (memcmp( net_buf+1, "Q#", 2)==0 ) {		// Halt all current slewing
		stop( N ); 
		stop( E );
		//cliente.print('1');
	} else 	if (memcmp( net_buf+1, "RS#", 3)==0 ) {		// Set Slew Rate to max Slew (fastest)
		_curSpeed = SLEW;//Serial.print("R");
	} else if (memcmp( net_buf+1, "Sd", 2)==0 ) {  		// set target DEC
		// format: :SdsDD*MM:SS#
		//Serial.print("d"); Serial.println(String(net_buf));
		int parms[3] = {0};
        short n = parseInt( net_buf+3, 3, "*:#", parms );
        char ret = '0';  // Dec invalida
        if (n == 3) {        
            short dd = parms[0];
            short mm = parms[1];
            short ss = parms[2];
            char sig = (char) *(net_buf+3);
            //Serial.print("D:");Serial.print(dd);Serial.print(" ");Serial.print(mm);Serial.print(" ");Serial.println(ss);
		    if ( _slewTarget.setDec(dd, mm, ss, sig) ) {
                ret = '1';	// Dec valida
                //Serial.print("received dec: ");Serial.print(_slewTarget.toString());Serial.print(" = "); Serial.println(_slewTarget.toStringF());
		    }
		} 
		cliente.print(ret);  

		//String coord = makeStringFromCmd( net_buf+3 );
		//coord.replace("*",":");
		//Serial.println( coord );
		//setTelTargetDec( coord );
		//cliente.print('1');
	} else if (memcmp( net_buf+1, "SC", 2)==0 ) {		// set Handbox Date to MM/DD/YY
		// format: :SCMM/DD/YY#
		//Serial.print("D");
        int parms[3] = {0};
        short n = parseInt( net_buf+3, 3, "/#", parms );
        String ret = "0";  // data invalida
        if (n == 3) {        
            short mm = parms[0];
            short dd = parms[1];
            short yy = parms[2];   // 17 significa 2017 (ano a partir de 2000)
			//Serial.print("D1");
            if ( mm >=1 && mm <=12 && dd >=1 && dd <=31 && yy >=0 && yy <= 99 ) {
				int nowHH = hour();
				int nowMM = minute();
				int nowSS = second();
				setTime( nowHH, nowMM, nowSS, dd, mm, yy );
				//char buf[20]; sprintf(buf, "data rec: %02d/%02d/%02d   now: %02d:%02d:%02d\n", yy,mm,dd, nowHH,nowMM,nowSS);
				//Serial.print(buf);
				_hasDate = true;
				setSiderealClock(now());
				ret = "1Updating Planetary Data#                              #";
            }
        }	
		cliente.print(ret);
		//Serial.print("t");
		//cliente.print("1Updating Planetary Data#                              #");
		//cliente.print("                #                #");
	} else 	if (memcmp( net_buf+1, "SG", 2)==0 ) {		// set #hours added to local time to yield UTC
		// format: :SGsHH.H#  ou SGsHH#
		//Serial.print("z");
		//Serial.print(net_buf);
        int parms[2] = {0};
        short n = parseInt( net_buf+3, 2, ".#", parms );
        char ret = '0';  // timeZone invalida
        if (n == 1 || n==2) {        
            short hh = parms[0];
            short h  = parms[1];
		    if ( (hh >= -12 && hh <= 12) && (h >= 0 && h < 10) ) {
		    	float frac = (hh >= 0)? h/10.0 : -h/10.0;
		    	setTimeZone( -1*(hh + frac) );  // -1: --> LX200: #hours ADDED to local time to yield UTC
		    	//Serial.print("tz:"); Serial.print(_timeZone);
				_hasTimeZone = true;
				setSiderealClock(now());
                ret = '1';	// timeZone valida
		    }
		} 
		cliente.print(ret);  
	} else if (memcmp( net_buf+1, "Sg", 2)==0 ) {		// set current site longitude
		// format: :SgDDD*MM#
		//Serial.println(net_buf_org); // a longitude com formato 0 a 360 graus
        int parms[2] = {0};
        short n = parseInt( net_buf+3, 2, "*#", parms );
        char ret = '0';  // longitude invalida
        if (n == 2) {        
            short dd = parms[0];
            short mm = parms[1];
		    if ( dd >= 0 && dd < 360 && mm >= 0 && mm < 60 ) {
		    	//Serial.printf("longitude: %d %d\n", dd, mm); 
		    	setLongitude( dd + mm/60.0 );
				_hasLongitude = true;
				setSiderealClock(now());
                ret = '1';	// latitude valida
		    }
		} 
		cliente.print(ret);  

	} else if (memcmp( net_buf+1, "SL", 2)==0 ) {		// set the local time
		// format: :SLHH:MM:SS#
		//Serial.print("t");
        int parms[3] = {0};
        short n = parseInt( net_buf+3, 3, ":#", parms );
        char ret = '0';  // local time invalido
        if (n == 3) {        
            short hh = parms[0];
            short mm = parms[1];
            short ss = parms[2];
            if ( hh >= 0 && hh <= 24 && mm >= 0 && mm < 60 && ss >=0 && ss < 60 ) {
				int nowDD = day();
				int nowMM = month();
				int nowYY = year();
				setTime( hh, mm, ss, nowDD, nowMM, nowYY );
				_hasTime = true;
				setSiderealClock(now());
				ret = '1';
            }
        }	
		cliente.print(ret);
	} else if (memcmp( net_buf+1, "Sr", 2)==0 ) {  	// set target RA
		// format: :SrHH:MM:SS#
		// GOTO:  :Sr + :Sd + :MS
		//Serial.print("r");
        int parms[3] = {0};
        short n = parseInt( net_buf+3, 3, ":#", parms );
        char ret = '0';  // RA invalido
		if (n == 3) {        
            short hh = parms[0];
            short mm = parms[1];
            short ss = parms[2];
			//Serial.print("R:");Serial.print(hh);Serial.print(" ");Serial.print(mm);Serial.print(" ");Serial.println(ss);
		    if ( _slewTarget.setRA(hh, mm, ss) ) {
                ret = '1';	// RA valido
				//Serial.print("R:");Serial.println(_slewTarget.toString());
		    }
		} 
		cliente.print(ret);  
		
		//String coord = makeStringFromCmd( net_buf+3 );
		//Serial.println( coord );
		//setTelTargetRA( coord );
		//cliente.print('1');
	} else if (memcmp( net_buf+1, "St", 2)==0 ) {		// sets the current site latitude
		// format:  :StsDD*MM#
        //Serial.print("L");
        int  parms[2] = {0};
        short n = parseInt( net_buf+3, 2, "*#", parms );
        char ret = '0';  // latitude invalida
        if (n == 2) {        
            short dd = parms[0];
            short mm = parms[1];
		    if ( dd >= -90 && dd <= +90 && mm >= 0 && mm < 60 ) {
		    	double latitude = dd + ((dd>=0)? mm/60.0 : -mm/60.0);
		    	setLatitude( latitude );   // set latitude para o telescopio
                ret = '1';	// latitude valida
			}
		} 
		cliente.print(ret);  
	} else 	if (memcmp( net_buf+1, "Sw", 2)==0 ) {		// set max slew rate to N degrees per second (N: 2..8)
		//Serial.println(net_buf);
		// format: S2N#
		char ret = '0';
		int rate = 0;
		short n = parseInt( net_buf+3, 1, "#", &rate );
		if (n == 1) {
			if (rate >= 1 && rate <= 4) {
				switch (rate) {
					case 1: _curSpeed = TRACK; break;
					case 2: _curSpeed = CENTER; break;
					case 3: _curSpeed = FIND; break;
					case 4: _curSpeed = SLEW; break;
				}
				ret = '1';
				//Serial.println("Sw = ok");
			}
		}
		cliente.print(ret);
	} else {
		cmdOk = false;   // comando desconhecido 
	}
	return cmdOk;
}
//----------------------------------------------------

bool Telescope::processaCmdCELESTRON( char *net_buf_org )
{
	bool cmdOk = true;
/*	char net_buf[50];
	strcpy(net_buf, net_buf_org);	// para poder modificar o cmd (facilita processamento)
	if (memcmp( net_buf, "$$$", 3)==0 ) {				// Config
		Serial.println("chegou: $$$");
		cliente.print("< 2.40-CEL >");
	} else if (memcmp( net_buf, "V#", 2 ) == 0 ) {    			// get version
		cliente.print( "16#" );    // versao: major minor #
	} else if (memcmp( net_buf, "t", 1 ) == 0 ) {    			// get tracking mode 
		// 0: Off
		// 1: alt/az
		// 2: EQ North
		// 3: EQ South
		cliente.print( "\1#" );    
	} else if (memcmp( net_buf, "e", 1 ) == 0 ) {				// get precise RA/DEC
		unsigned int  raPerc = _positionEq.getRA()  / 24.0  * (0xFFFFFFFF); // perc de uma revolucao (0xFFFFFFFF+1 = 4294967296);
		double dec = _positionEq.getDec();
		dec = (dec < 0)? 360+dec : dec;
		unsigned int decPerc = dec / 360.0 * (0xFFFFFFFF); 
		String s_ra  = String(raPerc,  HEX); 
		String s_dec = String(decPerc, HEX); 
		String zeros = String("00000000");
		String s_ra_dec = zeros.substring(0, 8-s_ra.length()) + s_ra + "," + zeros.substring(0, 8-s_dec.length()) + s_dec + String("#");
		//Serial.println(s_ra_dec);
		//cliente.print( "34AB0500,12CE0500#" );    
		cliente.print( s_ra_dec );    
	} else if (memcmp( net_buf, "M", 1 ) == 0 ) {				// cancel GOTO command
		stop( N );
		stop( E );		
		cliente.print( "#" );
	} else if (memcmp( net_buf, "r", 1 ) == 0 ) {				// GOTO precise RA/DEC
		// formato: rAAAAAAAA,DDDDDDDD         A: ra percentual em Hex   D: dec percentual em Hex
        unsigned int parms[2] = {0};
        short n = parseHexUInt( net_buf+1, 2, ",", parms );		
        Serial.print("n="); Serial.println(n);
        if (n == 2) {
        	double ra  = parms[0] / (double)(0xFFFFFFFF) * 24.0;
        	double dec = parms[1] / (double)(0xFFFFFFFF) * 360.0;
        	dec = (dec > 90)? dec-360 : dec;
        	//Serial.print(parms[0]); Serial.print("=="); Serial.println(ra);
        	//Serial.print(parms[1]); Serial.print("=="); Serial.println(dec);
        	//Serial.print("ra/dec: "); Serial.print(ra); Serial.print("/");Serial.println(dec);
        	_slewTarget.setRA( ra );
        	_slewTarget.setDec( dec );
        	slewTo( _slewTarget );
			cliente.print( "#" );    
        } else {
        	cmdOk = false;
        }
		
	} else if (memcmp( net_buf, "H", 1 ) == 0 ) {				// set Time
		// format: HQRSTUVWX
		// Q: hora (24 horas)    R: minutos     S: segundos
		// T: mes     U: dia     V: ano (seculo 20)  W: offset from GMT (se zona negativa: usar 256-zona
		// X: 1 para Daylight Savings, 0 para standard time
		short hh = net_buf[1];
		short mm = net_buf[2];
		short ss = net_buf[3];
		short MM = net_buf[4];
		short dd = net_buf[5];
		short yy = net_buf[6];
		short off = net_buf[7];
		short dls = net_buf[8];
		//Serial.print("H: hora:");Serial.print(hh);Serial.print(":");Serial.print(mm);Serial.print(":");Serial.print(ss);
		//Serial.print("  data:");Serial.print(yy);Serial.print("-");Serial.print(MM);Serial.print("-");Serial.print(dd);
		//Serial.print("  Ofset/dls:");Serial.print(off);Serial.print("/");Serial.println(dls);
        if ( ( MM >=1 && MM <=12 && dd >=1 && dd <=31 && yy >=0 && yy <= 99   ) &&
        	 ( hh >= 0 && hh <= 23 && mm >= 0 && mm <= 50 && ss >=0 && ss <= 59 ) ) {
    	 	if (dls) {
    	 		hh = (hh+1 < 23)? hh+1 : 0;
    	 		// PRECISA FAZER O ACERTO PARA HORARIO DE VERAO!! - VERIFICAR COMO VEM DO SKYSAFARI
    	 	}
			_hasTime = true;
			_hasDate = true;

			_timeZone = (off > 24)? off-256 : off;
			_hasTimeZone = true;

			setTime( hh, mm, ss, dd, MM, yy );
			setSiderealClock(now());
        }
		cliente.print( "#" ); 
	} else if (memcmp( net_buf, "W", 1 ) == 0 ) {				// set Location
		// format: WABCDEFGH
		// A: graus latitude    B: minutos latitude     C: segundos latitude
		// D: 0 p/ Norte, 1 para Sul   
		// E: graus longitude   F: minutos longitude  G: segundos longitude
		// H: 0 p/ Leste, 1 p/ Oeste
		short sinalLat = (net_buf[4] == 0)? +1 : -1;
		dms  latitude( sinalLat * net_buf[1], net_buf[2], net_buf[3] );
		short sinalLon = (net_buf[8] == 0)? +1 : -1;
		dms longitude( sinalLon * net_buf[5], net_buf[6], net_buf[7] );
		//Serial.print("lat/long: ");Serial.print(latitude.toString());Serial.print("/");Serial.println(longitude.toString());

		setLatitude( latitude.toDegree() );
		setLongitude( longitude.toDegree() );

		cliente.print( "#" ); 
	} else {
		cmdOk = false;
	}
*****/	
	return cmdOk;
}
//----------------------------------------------------
void Telescope::debugPrint(String strDebug)
{
	Serial.println(strDebug);
	Serial.print("lat:");Serial.print(_latitude);Serial.print("  st: ");Serial.println(TelSiderealClock.getSiderealTime(),4);
	Serial.print("_positionMotors: ");Serial.println(_positionMotors.toStringF(0));
	Serial.print("_positionHoriz: ");Serial.println(_positionHoriz.toStringF());
	Serial.print("_positionEq: ");Serial.println(_positionEq.toStringF());
	Serial.print("_trackTarget: ");Serial.println(_trackTarget.toStringF());
	CoordHorizontal trgH = convToHorizontal(_trackTarget);
	Serial.print("trgH = convToHorizontal(_trackTarget): "); Serial.println( trgH.toStringF() );
	Serial.print("horizontalToMotor( trgH ): ");Serial.println( horizontalToMotor( trgH ).toStringF(2) );
	
	
}
//----------------------------------------------------

void Telescope::setDebugFlag(bool estado)
{
	_debugFlag = estado;
}
//----------------------------------------------------
//----------------------------------------------------
 
