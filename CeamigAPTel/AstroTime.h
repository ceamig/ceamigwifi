/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file AstroTime.h
 * @author Marcos Ackel
 * @date  March 2017
 * @brief AstroTime include file - time routines.
 */

#ifndef ASTROTIME_H
#define ASTROTIME_H

#include "TimeLib.h"

typedef double SiderealTime_t;   // NAO FUNCIONA POR CONFUSAO DO PRE-PROCESSADOR ARDUINO....

double CalcSiderealTime( time_t dateTime, double longitude, int timeZone );

class SiderealClock
{
public:	
	SiderealClock() {_st=0;};        // Default Constructor
	SiderealClock(double stInicial) {_st = stInicial;};  // Constructor
	void setSiderealTime( double st ) {_st = st;};
	double getSiderealTime(void) {return _st;};
	void adjust(double adjustment) {_st += adjustment;};
	// Cada milisegundo de tempo (solar medio) corresponde a esse valor de milisegundos de tempo Sideral.
	static constexpr double SiderealMillisecond = (24.0 / 23.934469888888889 / 3600.0 / 1000.0); //0.997269578708333333;   
private:
	double _st;
};


#endif

