/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file AstroCoords.h
 * @author Marcos Ackel
 * @date  March 2017
 * @brief AstroCoords include file - class Coords.
 */

#ifndef ASTROCOORDS_H
#define ASTROCOORDS_H

// Half Second for rounding:
#define HALFSEC (1/(2*3600))D
#define SECROUND(x) ((x<0)? x-HALFSEC : x+HALFSEC)

//----------------------------------------------------

class dms
{
	private:
		int _dd;
		int _mm;
		double _ss;
		char _signal;  // Positivo: '+', Negativo: '-'     - E' necessario por causa de valores como -0:0:1 (nao tem zero negativo)
		
	public:
		dms();
		dms( int dd, int mm, double ss, char signal='+' ); 
		dms( double degree );
		int degree(void) {return _dd;};  // sempre positivo
		int minute(void) {return _mm;};  // sempre positivo
		double second(void) {return _ss;}  // sempre positivo
		char signal(void) {return _signal;};
		double toDegree(void) {  return (_signal=='+')? (_dd+_mm/60.0+_ss/3600.0) : (-_dd -_mm/60.0 -_ss/3600.0); };
		void roundSec(void);
		String toString(void); 
};
//----------------------------------------------------

class hms
{
	private:
		int _hh;
		int _mm;
		double _ss;
	public:
		// Nao considera ou trata hms negativos!
		hms();
		hms( int hh, int mm, double ss ) { _hh = hh; _mm = mm; _ss = ss; };
		hms( double hour );
		int hour(void) {return _hh;};
		int minute(void) {return _mm;};
		double second(void) {return _ss;}
		double toHour(void) {return _hh+_mm/60.0+_ss/3600.0;};
		void roundSec(void);
		String toString(void);
};
//----------------------------------------------------

class Coord
{
	private:
		double _x;
		double _y;
	public:
		Coord(double x=0, double y=0) { _x = x; _y = y; };
		double getX(void) {return _x;};
		double getY(void) {return _y;};
		void setX(double x) {_x = x;};
		void setY(double y) {_y = y;};
		void setXY(double x, double y) {_x = x; _y = y;};
		void setXY(Coord oToSet) { _x = oToSet.getX(); _y = oToSet.getY(); };
		void add(double dx, double dy) { _x += dx; _y += dy; };
		void add(Coord oToAdd) { _x += oToAdd.getX(); _y += oToAdd.getY(); };
		bool operator== (Coord& toComp) { return (_x == toComp.getX() && _y == toComp.getY()); };
		bool operator!= (Coord& toComp) { return (_x != toComp.getX() || _y != toComp.getY()); };
		String toString(void);
		String toStringF(byte decDigits=6);
};
//----------------------------------------------------
class CoordEquatorial;


class CoordHorizontal
{
	private:
		double _azm;
		double _alt;
	public:
		CoordHorizontal() { _azm = 0; _alt = 0;};
		CoordHorizontal(double azm, double alt) { setAzm( azm ); setAlt( alt ); };	
		CoordEquatorial toEquatorial(double latitude, double localSiderealTime);
		double getAzm(void) {return _azm;};
		double getAlt(void) {return _alt;};
		bool setAzm(double azm);
		bool setAlt(double alt);
		String toString();
		String toStringF(byte decDigits=6);
};
//----------------------------------------------------

class CoordEquatorial
{
	private:
		double _ra;
		double _dec;
	public:
		CoordEquatorial() { _ra = 0.0; _dec = 0.0; };
		CoordEquatorial(double ra, double dec) { setRA( ra ); setDec( dec ); };
		CoordHorizontal toHorizontal(double latitude, double localSiderealTime);
		bool setRA( double ra );
		bool setRA( short hh, short mm, double ss );
		bool setDec( double dec );
		bool setDec( short dd, short mm, double ss, char signal='+' );
		double getRA(void) {return _ra;};
		double getDec(void) {return _dec;};
		String decToString(void);   // formato: sDD*MM'SS#  (para LX200)
		String raToString(void);    // formato: HH:MM:SS#   (para LX200)
		String toString(void);      // formato: HH:MM:SS sDD*MM'SS"
		String toStringF(byte decDigits=6);
};		

#endif
//----------------------------------------------------
//----------------------------------------------------

