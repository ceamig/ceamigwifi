// Keypad.ino

#include "Keypad.h"

// Keypad:
const uint8_t SDA_PIN = 2;
const uint8_t SCL_PIN = 4;

PortButtonSet keypad(SDA_PIN, SCL_PIN);

// Endereco do keypad depende do chip de I2C usado no keypad:
// PCF8574: endereço = 0x20
// PCF8574A: endereço = 0x38
// No config: (veja ConfigHw.h):
// configHw.isHPadTipoA -> se true, endereço = 0x38
//                      -> se false, enereço = 0x20
// Usar funcao de inicializacao PortButtonSet::setAddress()

// Botoes:
PortButton btN(keypad, 0, true);	// Norte
PortButton btS(keypad, 1, true);	// Sul
PortButton btE(keypad, 2, true);	// Oeste (West)
PortButton btW(keypad, 3, true);	// Leste (East)
PortButton btC(keypad, 4, true);	// Centro
PortButton btL(keypad, 5, true);	// Esquerda (Left)
PortButton btR(keypad, 6, true);// Direita  (Right)	

const int defClickTicks = 50;
const int defPressTicks = 100;

KeypadSpeed_t _curKeypadSpeed = SLEW;

void InitKeypad(void)
{
	// keypad address from config:
	byte keypadAddr = (configHw.isHPadTipoA)? 0x38 : 0x20;
	keypad.setAddress(keypadAddr, true);   // pullDown: true   pois o PCF8574 inicializa as portas com todas em HIGH

	//keypad.setDefaultClickTicks(50);   // isso efetivamente desabilita a detecao de double-click
	//keypad.setDefaultPressTicks(60);
	btN.setClickTicks( defClickTicks ); btN.setPressTicks( defPressTicks );
	btS.setClickTicks( defClickTicks ); btS.setPressTicks( defPressTicks );
	btW.setClickTicks( defClickTicks ); btW.setPressTicks( defPressTicks );
	btE.setClickTicks( defClickTicks ); btE.setPressTicks( defPressTicks );
	btC.setClickTicks( defClickTicks ); btC.setPressTicks( defPressTicks );
	btL.setClickTicks( defClickTicks ); btL.setPressTicks( defPressTicks );
	btR.setClickTicks( defClickTicks ); btR.setPressTicks( defPressTicks );

	btN.attachLongPressStart(btN_ClickProc);
	btS.attachLongPressStart(btS_ClickProc);
	btW.attachLongPressStart(btW_ClickProc);
	btE.attachLongPressStart(btE_ClickProc);
	btC.attachLongPressStart(btC_ClickProc);
	btL.attachLongPressStart(btL_ClickProc);
	btR.attachLongPressStart(btR_ClickProc);
	btN.attachLongPressStop(btN_ClickProc);
	btS.attachLongPressStop(btS_ClickProc);
	btW.attachLongPressStop(btW_ClickProc);
	btE.attachLongPressStop(btE_ClickProc);
	btC.attachLongPressStop(btC_ClickProc);
	btL.attachLongPressStop(btL_ClickProc);
	btR.attachLongPressStop(btR_ClickProc);
	//btCtrl.attachDoubleClick(btCtrlDoubleClickProc);   // alternativo para iniciar os fases
	//btCtrl.attachDuringLongPress(btCtrlDuringProc);	
}


void processaKeypad(void)
{
	keypad.tick();
}

void btN_ClickProc()
{
	//Serial.println("Botao N");
	Telescope::TelState telState = telescope.getState();

	if (btN.isLongPressed()) {
		// button Pressed:
		if (btL.isLongPressed() && telState == Telescope::TRACKING) {
			// Inicia Busca Espiral:  
			telescope.spiralSearch(50.0, true);  // raio em minutos de arco
		} else if (telState == Telescope::SPIRAL) {
			telescope.spiralInvertDirection();
		} else {
			telescope.move( Telescope::N, _curKeypadSpeed );   // move N
		}
	} else {
		// button Released:
		if (telState != Telescope::SPIRAL)
			telescope.stop( Telescope::N );
	}

/*	
	if (btN.isLongPressed() && btL.isLongPressed()) {
			Serial.println("L e N Long");
		if (telState == Telescope::TRACKING) {
			// Inicia Busca Espiral:  
			telescope.spiralSearch(60.0, true);  // raio em minutos de arco
		}
	} else {
		if (btN.isLongPressed()) {
			if (telState == Telescope::SPIRAL)
				telescope.spiralInvertDirection();
			else				
				telescope.move( Telescope::N, _curKeypadSpeed );   // move N
		} else {
			if (telState != Telescope::SPIRAL)
				telescope.stop( Telescope::N );
		}
	}
	*/
}
void btS_ClickProc()
{
	//Serial.println("Botao S");
	Telescope::TelState telState = telescope.getState();
	if (btS.isLongPressed() && btL.isLongPressed()) {
		if (telState == Telescope::TRACKING) {
			// Inicia Busca Espiral:  
			telescope.spiralSearch(60.0, false);  // raio em minutos de arco
		}
	} else {
		if (btS.isLongPressed()) {
			if (telState == Telescope::SPIRAL)
				telescope.spiralInvertDirection();
			else				
				telescope.move( Telescope::S, _curKeypadSpeed );   // move S
		} else {
			if (telState != Telescope::SPIRAL)   
				telescope.stop( Telescope::S );
		}
	}
}

void btW_ClickProc()
{
	//Serial.println("Botao W");
	Telescope::TelState telState = telescope.getState();
	if (telState == Telescope::SPIRAL || telState == Telescope::SPIRAL_PAUSED) {
		telescope.spiralCancel();
	} else {
		if (btW.isLongPressed()) {
			telescope.move( Telescope::W, _curKeypadSpeed );
		} else {
			telescope.stop( Telescope::W );
		}
	}
}
void btE_ClickProc()
{
	//Serial.println("Botao E");
	Telescope::TelState telState = telescope.getState();
	if (telState == Telescope::SPIRAL || telState == Telescope::SPIRAL_PAUSED) {
		telescope.spiralCancel();
	} else {
		if (btE.isLongPressed()) {
			telescope.move( Telescope::E, _curKeypadSpeed );
		} else {
			telescope.stop( Telescope::E );
		}
	}
}


void btC_ClickProc()
{
	//Serial.println("Botao C");
	Telescope::TelState telState = telescope.getState();

	if (btC.isLongPressed()) {
		// button Pressed:
		if (telState == Telescope::SPIRAL) {
			telescope.spiralPause();
		} else if (telState == Telescope::SPIRAL_PAUSED) {
			telescope.spiralContinue();
		} else {
			// nada...		
		}
	} else {
		// button released:
		if (telState != Telescope::SPIRAL && telState != Telescope::SPIRAL_PAUSED) {
			//Serial.println("Botao C");
			if (_curKeypadSpeed == SLEW) {
				_curKeypadSpeed = CENTER;
				tone(330, 150);
			} else {
				_curKeypadSpeed = SLEW;
				tone(4000, 150);
			}
		}
	}
}

void btL_ClickProc()
{
	//Serial.println("Botao L");
	if (btL.isLongPressed() ) {
		Serial.println("L Long");
		telescope.setDebugFlag(true);
	} else {
		// released:
		telescope.setDebugFlag(false);
	}
	
}

void btR_ClickProc()
{
	//Serial.println("Botao R");
	//tone(440, 200);
	if (btR.isLongPressed()) {
		telescope.debugPrint();
	}
}
//----------------------------------------------------
//----------------------------------------------------
