

#include "AstroTime.h"
#include <math.h>

double fmod(double x, double y)
{
	double i, f;
	if (y == 0.0) {
		return 0.0;
	}
	/* return f such that x = i*y + f for some integer i
	   such that |f| < |y| and f has the same sign as x */
	i = floor(x/y);
	f = x - i*y;
	if ((x < 0.0) != (y < 0.0))
		f = f-y;
	return f;
}

double CalcSiderealTime( time_t dateTime, double longitude, int timeZone )
{
	// Belo Horizonte: timeZone = -3 h
	
	// http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?bibcode=1983IAPPP..13...16F&db_key=AST&page_ind=0&plate_select=NO&data_type=GIF&type=SCREEN_GIF&classic=YES
	// http://aa.usno.navy.mil/faq/docs/GAST.php
	
	// outra ref: http://www2.arnes.si/~gljsentvid10/sidereal.htm
	
	int ano = year(dateTime);
	int mes = month(dateTime);
	int dia = day(dateTime);
	int hora = hour(dateTime);
	int min = minute(dateTime);
	int seg = second(dateTime);

	// Greenwich 00:00:
	double JDN0 = 367*ano - ( 7*(ano + ((mes+9)/12))/4 ) + (275*mes/9) + dia + 1721013.5;
	double dfrac = (hora + min/60.0 + seg/3600.0)/24.0;

	double JDN = JDN0 + dfrac;
	double LJDN = JDN - timeZone/24.0;

	// desde 2000, jan1, 12h UT (Julian Date 2451545.0):
	double D = LJDN - 2451545.0;

	double GMST = 18.697374558 + 24.06570982441908 * D;

	double LGMST = GMST - longitude/15.0;  // logitude in hours (360/24 = 15h)
	
	double LMST = fmod( LGMST , 24 );

	//return (SiderealTime_t) LMST;
	return LMST;
		
}

