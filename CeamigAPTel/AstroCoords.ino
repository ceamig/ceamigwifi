

#include "AstroCoords.h"
#include <math.h>

//----------------------------------------------------

dms::dms( double degree )
{
	_signal = (degree < 0)? '-' : '+';
	_dd = (int) fabs(degree);
	double m = (fabs(degree) - _dd) * 60.0;
	_mm = (int) m;
	_ss = (m - _mm) * 60.0;
}
//----------------------------------------------------

dms::dms( int dd, int mm, double ss, char signal ) 
{ 
	// Qualquer sinal negativo em qualquer parametro faz o numero todo ficar negativo.
	// d,m e s sao armazenados em valor absoluto
	// Use case: 0:-1:0 e 0:0:-1  (g:m:s)
	// Use case mais comum: coordenadas de Declinacao proximas 
	// (a menos de 1 grau) do equador celeste, lado sul (negativo

	// condicao inicial:
	_dd = _mm = 0;
	_ss = 0.0;
	_signal = '+';

	if (signal == '+' || signal == '-') {
		if (dd < 0) {
			_signal = '-'; dd = abs(dd);			
		}
		if (mm < 0) {
			_signal = '-'; mm = abs(dd);
		}
		if (ss < 0) {
			_signal = '-'; ss = fabs(dd);
		}
		if (mm < 60 && ss < 60) {
			_dd = dd; _mm = mm; _ss = ss;
		}
	} 
}
//----------------------------------------------------

String dms::toString(void)
{
	char buf[20] = {0};
	//byte secPrecision1 = secPrecision + 3;  // 2 digitos para os segundos mais o ponto decimal
	roundSec();
	sprintf(buf, "%c%02d*%02d'%02d\"", _signal, _dd, _mm, (int)_ss);
	String ret( buf );
	return ret;
}
//----------------------------------------------------

void dms::roundSec(void)
{
	int ss = (int) round(_ss);
	if (ss==60) {
		_mm = _mm + 1;
		_ss = 0.0;
	}
	if (_mm == 60) {
		_mm = 0;
		_dd = _dd + 1;
	}
}
//----------------------------------------------------
//----------------------------------------------------

hms::hms( double hour )
{
	_hh = (int) hour;
	double m = (hour - _hh) * 60.0;
	_mm = (int) m;
	_ss = (m - _mm) * 60.0;
	
}
//----------------------------------------------------

void hms::roundSec(void)
{
	int ss = (int) round(_ss);
	int mm = _mm;
	if (ss==60)
		mm = (_hh >= 0)? _mm +1 : _mm -1;
	_hh = (mm == 60) ? _hh + 1 : _hh;
	_mm = (mm == 60) ? 0 : mm;
	_ss = (ss == 60) ? 0 : ss;
}
//----------------------------------------------------

String hms::toString(void)
{
	char buf[20] = {0};
	//byte secPrecision1 = secPrecision + 3;  // 2 digitos para os segundos mais o ponto decimal
	roundSec();
	sprintf(buf, "%02d:%02d:%02d", _hh, _mm, (int)_ss);
	String ret( buf );
	return ret;
}
//----------------------------------------------------
//----------------------------------------------------

String Coord::toString(void)
{
	String ret = _x + String(";") + _y;
	return ret;
}
//----------------------------------------------------

String Coord::toStringF(byte decDigits)
{
	return String(_x, decDigits)+String(";")+String(_y, decDigits);
}
//----------------------------------------------------
//----------------------------------------------------

CoordEquatorial CoordHorizontal::toEquatorial(double latitude, double localSiderealTime)
{
	// ra and lst in hours
	// dec and lat in degrees
	// http://star-www.st-and.ac.uk/~fv/webnotes/chapter7.htm

	double toRad = 3.1415926535897932385 / 180.0;   // constante PI / 180	
	// O arredondamento tem que ser para cima na ultima casa. 
	// Senao, qdo converte para graus, pode dar valores maiores que 90.0 para DEC
	double latRad = latitude * toRad;
	double altRad = _alt * toRad;
	double azmRad = _azm * toRad;
	double cosLat = cos(latRad);
	double sinLat = sin(latRad);
	double sinDec = sin(altRad) * sinLat + cos(altRad) * cosLat * cos(azmRad);
	sinDec = (sinDec > 1.0)? 1.0 : sinDec;
	sinDec = (sinDec < -1.0)? - 1.0 : sinDec;
	double decRad = asin(sinDec);
	double dec = decRad / toRad;
	double cosDec = cos(decRad);
	double sinH = -sin(azmRad) * cos(altRad) / cosDec;
	sinH = (sinH > 1.0) ? 1.0 : sinH;
	sinH = (sinH < -1.0) ? -1.0 : sinH;
	double cosH = (sin(altRad) - sin(decRad) * sinLat) / (cosDec * cosLat);
	
	double HRad = asin(sinH);
	double H = HRad / toRad;  
	if (cosH < 0)
		H = 180.0 - H;
	if (H < 0)
		H = 360.0 + H;

	double ra = (localSiderealTime - H/15.0);  // 15.0:  to convert degrees in hours.
	if (ra >= 24.0)
		ra = ra - 24.0;
	if (ra < 0)
		ra = ra + 24.0;

	return CoordEquatorial(ra, dec);
}
//----------------------------------------------------

bool CoordHorizontal::setAlt( double alt )
{
	bool ret = false;
	if ( alt >= -90.0 && alt <= +90.0 ) {
		_alt = alt;
		ret = true;
	}
	return ret;
}
//----------------------------------------------------

bool CoordHorizontal::setAzm( double azm )
{
	bool ret = false;
	if (azm >= -360.0 && azm <= 360.0) {
		_azm = azm;
		ret = true;
	}
	return ret;
}
//----------------------------------------------------

String CoordHorizontal::toString()
{
	dms al(_alt);
	dms az(_azm);
	String ret = az.toString() + String(" ") + al.toString();
	return ret;
}
//----------------------------------------------------

String CoordHorizontal::toStringF(byte decDigits)
{
	return String(_azm, decDigits)+String(" ")+String(_alt, decDigits);
}
//----------------------------------------------------
//----------------------------------------------------

CoordHorizontal CoordEquatorial::toHorizontal(double latitude, double localSiderealTime)
{
	// ra and lst in hours
	// dec and lat in degrees
	// http://star-www.st-and.ac.uk/~fv/webnotes/chapter7.htm

	// Local Hour Angle
	double H = (localSiderealTime - _ra) * 15;   // (lst - ra) is in hours. Need it in degrees, not in hours

	double toRad = 3.1415926535897932385 / 180.0;   // constante PI / 180 
	// O arredondamento tem que ser para cima na ultima casa.

	double HRad = H * toRad;  // em radianos
	double latRad = latitude * toRad;
	double decRad = _dec * toRad;
	
	double signalt = sin(decRad) * sin(latRad) + cos(decRad) * cos(latRad) * cos(HRad);

	double altRad = asin( signalt );
	double alt = altRad / toRad;

	// AZimuth:
	double sinAz =  -sin(HRad) * cos(decRad) / cos( altRad );
	double cosAz = ( sin(decRad) - sin(latRad) * signalt ) / ( cos(latRad) * cos(altRad) );

	double azmRad = asin( sinAz );
	double azm = azmRad / toRad;
	if (cosAz < 0)
    	azm = 180.0 - azm;
    if (azm < 0)
    	azm = 360.0 + azm;

    // azimute entre 0 e 360 graus

	return CoordHorizontal( azm, alt );
}
//----------------------------------------------------

String CoordEquatorial::decToString(void)
{
	// formato: sDD*MM'SS#  (para LX200)
	char buf[20] = {0};
	dms dec(_dec);
	dec.roundSec();
	sprintf( buf, "%c%02d*%02d'%02d#", dec.signal(), dec.degree(), dec.minute(), (int) dec.second() );
	String ret( buf );
	return ret;	
}
//----------------------------------------------------

String CoordEquatorial::raToString(void)
{
	// formato: HH:MM:SS#   (para LX200)
	char buf[20] = {0};
	hms ra(_ra);
	ra.roundSec();
	sprintf( buf, "%02d:%02d:%02d#", ra.hour(), ra.minute(), (int) ra.second() );
	String ret( buf );
	return ret;	
}
//----------------------------------------------------

bool CoordEquatorial::setRA( double ra )
{
	bool ret = false;
	if ( ra >= 0.0 && ra <= 24.0 ) {
		_ra = ra;
		ret = true;
	}
	return ret;
}
//----------------------------------------------------

bool CoordEquatorial::setRA( short hh, short mm, double ss )
{
	bool ret = false;	
	if (hh >= 0 && hh <= 24 && mm >= 0 && mm <= 59 && ss >= 0 && ss < 60.0) {
		_ra = hh + mm/60.0 + ss/3600.0;
		ret = true;
	}
	return ret;
}
//----------------------------------------------------

bool CoordEquatorial::setDec( double dec )
{
	bool ret = false;
	if (dec >= -90.0 && dec <= +90.0) {
		_dec = dec;
		ret = true;
	}
	return ret;
}
//----------------------------------------------------

bool CoordEquatorial::setDec( short dd, short mm, double ss, char signal )
{
	bool ret = false;
	signal = (dd < 0 || mm < 0 || ss < 0.0)? '-' : signal;	
	if (dd >= -90 && dd <= +90 && mm >= 0 && mm < 60 && ss >= 0 && ss < 60.0) {
		_dec = (double) abs(dd) + abs(mm)/60.0 + fabs(ss)/3600.0;	
		if ( signal == '-' )
			_dec *= -1.0;
		ret = true;
	}
	return ret;
}
//----------------------------------------------------

String CoordEquatorial::toString(void)
{
	// formato: HH:MM:SS sDD*MM'SS"
	hms ra(_ra);
	dms de(_dec);
	ra.roundSec();
	de.roundSec();
	char buf[40] = {0};
	sprintf( buf, "%02d:%02d:%02d %c%02d*%02d'%02d\"", 
			ra.hour(), ra.minute(), (int)ra.second(), de.signal(), de.degree(), de.minute(), (int)de.second() );
	String ret( buf );
	return ret;	
}
//----------------------------------------------------

String CoordEquatorial::toStringF(byte decDigits)
{
	return String(_ra, decDigits)+String(" ")+String(_dec, decDigits);
}
//----------------------------------------------------
//----------------------------------------------------

