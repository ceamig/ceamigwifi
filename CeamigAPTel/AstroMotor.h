/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file AstroMotor.h
 * @author Marcos Ackel
 * @date  March 2017
 * @brief AstroMotor include file - class AstroMotor.
 */

#ifndef ASTROMOTOR_H
#define ASTROMOTOR_H

#include "AccelStepper.h"

typedef enum {	ZERO  =0,		///< Stopped!
				TRACK =1,		///< Slowest Speed - tracking objects in the sky 
				CENTER=2, 		///< Second Slowest Speed for centering objects 
				FIND  =3,		///< Second Max Speed for finding objects 
				SLEW  =4, 		///< Max Speed - Slew
				FREESPEED =5	///< Free Speed
		} MotorSpeed;

const int NumItensMotorSpeed = (FREESPEED - ZERO + 1);

#define AstroMotorSpeed MotorSpeed

class AstroMotor : private AccelStepper
{
	public:
/*		typedef enum {	ZERO,		///< Stopped!
						TRACK, 		///< Slowest Speed - tracking objects in the sky 
						CENTER, 	///< Second Slowest Speed for centering objects 
						FIND, 		///< Second Max Speed for finding objects 
						SLEW 		///< Max Speed - Slew
				} AstroMotorSpeed;
*/
		typedef enum { 	MICROSTEP, 		///< Micro Step mode
						FULLSTEP	 	///< Full Step mode 
				} AstroStepMode;

		typedef enum {	CLOCKWISE,
						COUNTERCLOCKWISE
				} Direction;

		AstroMotor() {};
		AstroMotor(uint8_t pinStep, uint8_t pinDir, uint8_t pinSpeed); 
		
		void setMicrostep(uint8_t microstep) {_microstepSize = microstep;};
		void setMaxSpeed(float stepSpeed) {AccelStepper::setMaxSpeed(stepSpeed);};   // speed: steps/sec
		float getMaxSpeed(void) {return AccelStepper::maxSpeed();};
		void setAcceleration(float acceleration) 			// acceleration: steps/sec/sec
					{_acceleration = acceleration; AccelStepper::setAcceleration(acceleration);};
		float getAcceleration(void) {return _acceleration;}; 
		void moveTo( AstroMotorSpeed speed, long newPosition, bool warnOnTarget=false );
		void move( AstroMotorSpeed speed, long relativePosition );
		void move( AstroMotorSpeed speed, Direction dir );
		void freeSpeedMoveTo( float newSpeed, long newPosition );
		
		void stop(void);
		boolean run(void) {AdjustRunPosition(); return AccelStepper::run();};
		void AdjustRunPosition(void);
		long currentPosition(void);
		void setCurrentPosition(long position) {AccelStepper::setCurrentPosition(position);};
		
		void setDefaultSpeed( AstroMotorSpeed spd, AstroStepMode mode, float stepSpeed );
		long distanceToGo(void);
		bool onTargetWarning(void) {return _onTargetWarning;};
		void clearOnTargetWarning(void) {_onTargetWarning=false;};

		void setInvertedMotorDirection(bool inverted=false) {AccelStepper::setPinsInverted(inverted);};
		
	private:
		uint8_t _microstepSize;     ///> microstep fraction Size: 16 means 1/16
		uint8_t _pinSpeed;			///> hardware pin to control microstep 0: Full Step, 1: Microstep
		float   _acceleration;      // nao tem get no Acceleration... armazenar
		//long _currentPosition;		///> in microsteps units
		//long _currentFullStepPosition ///> positon when the motor is operating in Full steps
		AstroMotorSpeed _currentSpeed;
		bool _onTargetWarning;
		
		typedef struct controlSpeed_t { AstroStepMode mode; 
										float stepSpeed; 
									};

		controlSpeed_t _controlSpeed[NumItensMotorSpeed];	// one for each speed: ZERO, TRACK, CENTER, FIND, SLEW

		void setStepMode( AstroStepMode mode );
		
};

#endif

