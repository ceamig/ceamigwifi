/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file Telescope.h
 * @author Marcos Ackel
 * @date  March 2017
 * @brief Telescope include file - class Telescope.
 */

#ifndef TELESCOPE_H
#define TELESCOPE_H

#include <Arduino.h>
#include <AccelStepper.h>
#include "AstroTime.h"
#include "AstroCoords.h"
#include "AstroMotor.h"

// Telescope SPEEDS are the same as the Motors:
#define TelSpeed MotorSpeed

struct ConfigHw_st;

class Telescope {
	public:
		typedef enum { MEADE,		///< Modelo defaut: MEADE LX200
					   CELESTRON
		} TelModel;
		
		typedef enum { 	ALTAZ, 		///< Alt-Azimultal Telescope. 
						EQUAT, 		///< Equatorial Fork Telescope 
						GERMAN, 	///< German Equatorial Telescope. 
		} TelType;
		
		typedef enum { 	N, 		/**< Direction North */
						S,	 	/**< Direction South */
						E, 		/**< Direction East  */
						W 		/**< Direction West  */
		} TelDirection;
			
		typedef enum { 	MOTORALT, 	/**< Altitude Motor */
						MOTORAZM 	/**< Azimuth  Motor */
		} TelMotor;

		typedef enum { 	STOPPED,	///< Telescope is stopped
						STOPPING,   ///< Telescope is stopping after a command to stop
						TRACKING,	///< Telescope is Tracking
						MOVING,		///< Telescope is Moving (any speed that is not Tracking)
						SPIRAL,		///< Telescope in Spiral Search
						SPIRAL_PAUSED	///< Telescope in Spiral Search Pause
		} TelState;

		Telescope(TelModel model, TelType type);	// Constructor

		bool processaCmd( char *net_buf, uint16_t buf_size );
		
		//void setMotorSpeed( TelMotor motor, TelSpeed speed );
		//AstroMotor::AstroMotorSpeed TelSpeedToAstroMotorSpeed( TelSpeed speed );
		void initTelescope( ConfigHw_st *stConfig );
		void setModel(TelModel model) {_model = model;};
		TelModel getModel(void) {return _model;};
		
		void move( TelDirection dire, TelSpeed speed );
		void moveTo( CoordHorizontal azAlt, TelSpeed speed );
		void moveTo( CoordEquatorial equat, TelSpeed speed );
		void slewTo( CoordEquatorial equat );
		void stop( TelDirection dire );
		void stop( TelMotor motor );
		void stop( void );			// all stop (keep tracking astronomical target)

		void setTelSpeed( TelSpeed speed );
		TelSpeed getCurSpeed(void) {return _curSpeed;};

		TelState getState( void ) {return _state;};
		TelState getPrevState( void ) {return _prevState;};

		CoordHorizontal convToHorizontal( CoordEquatorial equat );
		CoordEquatorial convToEquatorial( CoordHorizontal azAlt );
		
		void alignTelPosition( CoordEquatorial newPosEq );
		void verifySiderealClock();
		CoordEquatorial getEquatorialPosition(void) {return _positionEq;};
		CoordHorizontal getHorizontalPosition(void) {return _positionHoriz;};
		void recalcPosition(void);
		bool onTargetWarning(void);
		void clearOnTargetWarning(void);

		double setLatitude(double latitude);
		void setLongitude(double longitude);
		void setTimeZone(double timeZone);

		void setMaxSpeed(TelMotor motor, float speed);
		void setAcceleration(TelMotor motor, float accel);
		float getMaxSpeed(TelMotor motor);
		float getAcceleration(TelMotor motor);

		bool spiralSearch( double diameterMinArc, bool direction=true );
		void spiralCancel(void);
		void spiralPause(void);
		void spiralContinue(void);
		void spiralInvertDirection(void);

		void debugPrint(String strDebug="");
		void setDebugFlag(bool estado);

	private:
		// Modelo do telescopio:
		TelModel _model;				// Modelo do telescopio (Meade, Celestron)
		TelType  _type;					// Tipo de montagem do telescopio (Alt-Azimutal, equatorial...)

		ConfigHw_st *_stConfig;			// Parametros gerais de configuracao

		CoordEquatorial _slewTarget;	// target for future Goto or Align
		CoordEquatorial _trackTarget;	// tracking target

		// Para a busca espiral:
		Coord   _spiralNextNode;		// posicao dos motores Alt e Azm do proximo nodo 
		Coord   _spiralLastNode;		// posicao dos motores Alt e Azm do ultimo nodo percorrido
		Coord   _spiralStart;			// posicao dos motores no inicio da busca
		uint8_t _spiralCount;
		uint8_t _spiralPos;
		double  _spiralDiameter;		// diametro em microsteps
		double  _spiralAzmFactor;		// fator de correcao do diametro para azimute (que nao e' circulo maximo)
		bool    _spiralDirection=true;	// 1 ou 0 para direcao
		
		CoordEquatorial _positionEq;  // read only. ==> only Telescope::recalcPostion() should set
		CoordHorizontal _positionHoriz;    // read only. ==> only Telescope::recalcPostion() should set
		Coord _positionMotors;		  // read only. ==> only Telescope::recalcPostion() should set

		TelState _state;		// Telescope state (Stopped, Tracking, Moving, etc) --> set only with setState()!
		TelState _prevState;	// Previous Telescope state
		
		TelState setState( TelState newState ) {TelState prev=_prevState; _state = newState; return prev;};
		
		double _latitude;	// current site latitude - Memoria Flash
		double _longitude;
		double _timeZone;

		// Correcoes da montagem: eixos:
		double _erroEixoDec;
		double _erroEixoRA;

		//time_t _localTime;  // in seconds since 1970 (TimeLib.h) -- Usando o sysTime da Time Lib
		
		String _sTelRA  = "06:30:00#";
		String _sTelDec = "-15*30'00#";
		String _sTelTargetRA  = "06:30:00#";
		String _sTelTargetDec = "-15*30'00#";

		bool _invertedAzimuth;	// motor de azimute com direcao invertida - Memoria Flash
		bool _invertedAltitude; // motor de altura com direcao invertida - Memoria Flash

		double _RAStepsPerRev;			// Motor de Azm/RA, em microsteps
		double _DecStepsPerRev;			// Motor de Alt/Dec, em microsteps
		double _RAStepSize;				// Motor de Azm/RA,  em segundos de arco
		double _DecStepSize;			// Motor de Alt/DEC, em segundos de arco

		TelSpeed _curSpeed;
		//TelSpeed _motorPresentSpeed;
		//bool _adjustAltMotorStepsNeeded;
		//bool _adjustAzmMotorStepsNeeded;
		
		//AccelStepper MotorAlt;
		//AccelStepper MotorDec;
		//AstroMotor _motorAlt;
		//AstroMotor _motorAzm;
	
		void initMotors(void);
		void initUser(void);
		//void timerCallback(void *pArg);
		String makeStringFromCmd( char* cmd );
		//String makeString( char* str, byte len );
        //int parseInt( char* cmd, int numInts, char* cSeparators, int intsParsed[]);
     
		bool processaCmdLX200( char *net_buf );
		bool processaCmdCELESTRON( char *net_buf );

		Coord horizontalToMotor( CoordHorizontal &coord );
		void motorMoveTo( TelMotor motor, double position );
		CoordHorizontal motorToHorizontal();
		void setTelRA( String ra );
		void setTelDec( String dec );
		//String getTelRA();
		//String getTelDec();
		String getStringTelRA();  //{String sr = _position.raToString();  return sr;};
		String getStringTelDec();  // {String sd = _position.decToString(); return sd;};

		bool setSiderealClock( double st );
		bool _hasTime;   // Ajustou a hora
		bool _hasDate;   // Ajustou a data
		bool _hasLongitude; // Ajustou a longitude
		bool _hasTimeZone;  // Ajustou ambos

		bool spiralNextTarget(void);

		bool _segundoSlew = false;
		bool _debugFlag;
};

#endif
