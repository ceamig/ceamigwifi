// AstroMotor.ino

#include "AstroMotor.h"

// Constructor:
AstroMotor::AstroMotor(uint8_t pinStep, uint8_t pinDir, uint8_t pinSpeed) : 
						AccelStepper( AccelStepper::DRIVER, pinStep, pinDir )
{
	// Default Values:
	_microstepSize=16; 
	_pinSpeed=pinSpeed; 
	_controlSpeed[ZERO  ] = { MICROSTEP, 0    };
	_controlSpeed[TRACK ] = { MICROSTEP, 500  };
	_controlSpeed[CENTER] = { MICROSTEP, 1000 };
	_controlSpeed[FIND  ] = { FULLSTEP,  500  };
	_controlSpeed[SLEW  ] = { FULLSTEP,  1200 };
	_controlSpeed[FREESPEED  ] = { FULLSTEP,  0 };

	AccelStepper::setMaxSpeed(900);
	AccelStepper::setAcceleration(1000);
	_currentSpeed = ZERO;
	_onTargetWarning = false;
};
//----------------------------------------------------

void AstroMotor::moveTo( AstroMotorSpeed newSpeed, long newPosition, bool warnOnTarget )
{
	long positionToGo = newPosition;

	if (newSpeed != _currentSpeed) {
		AstroStepMode curMode = _controlSpeed[_currentSpeed].mode;
		AstroStepMode newMode = _controlSpeed[ newSpeed ].mode;
		if ( curMode < newMode ) {
			// Tem que controlar a mudanca de step:
			// =============================  IMPLEMENTAR!!!! ============================
		}
		long startSteps = 0;
		if ( curMode == MICROSTEP && newMode == FULLSTEP ) {
			AccelStepper::setCurrentPosition( AccelStepper::currentPosition() / _microstepSize );
			startSteps = newPosition % _microstepSize;     // microsteps to complete the position
			positionToGo = newPosition / _microstepSize;
		} else if ( curMode == FULLSTEP && newMode == MICROSTEP ) {
			AccelStepper::setCurrentPosition( AccelStepper::currentPosition() * _microstepSize );
		}
		float newStepSpeed = _controlSpeed[ newSpeed ].stepSpeed;
		setStepMode( newMode );
		AccelStepper::setMaxSpeed( newStepSpeed );
		//Serial.print(newStepSpeed); Serial.print(" "); Serial.println(newPosition);
		//AccelStepper::moveTo( 0 );
		_currentSpeed = newSpeed;
	}
	
	AccelStepper::moveTo( positionToGo );
}
//----------------------------------------------------

void AstroMotor::freeSpeedMoveTo( float newSpeed, long newPosition )
{
	// freeSpeed -> sempre em FULLSTEP!
	long positionToGo = newPosition;
	AstroStepMode curMode = _controlSpeed[_currentSpeed].mode;
	
	if ( curMode == MICROSTEP ) {
		AccelStepper::setCurrentPosition( AccelStepper::currentPosition() / _microstepSize );
	}
	positionToGo = newPosition / _microstepSize;
	setStepMode( FULLSTEP );
	AccelStepper::setMaxSpeed( newSpeed );
	_currentSpeed = FREESPEED;
	
	AccelStepper::moveTo( positionToGo );
}
//----------------------------------------------------

void AstroMotor::AdjustRunPosition(void)
{
	AstroStepMode curMode = _controlSpeed[_currentSpeed].mode;

	if (curMode == FULLSTEP && AccelStepper::distanceToGo() == 0) {
		if (_currentSpeed==SLEW ) {
			//Serial.println("TARGET!");
			_onTargetWarning = true;
		}
		AccelStepper::setCurrentPosition( AccelStepper::currentPosition() * _microstepSize );
		AccelStepper::setMaxSpeed( ZERO );
		setStepMode(MICROSTEP);
		AccelStepper::moveTo( AccelStepper::currentPosition() );
		_currentSpeed = ZERO;
	}
}
//----------------------------------------------------

long AstroMotor::distanceToGo()
{
	return AccelStepper::distanceToGo();
}
//----------------------------------------------------

void AstroMotor::move( AstroMotorSpeed speed, long relativePosition )
{
	moveTo(speed, currentPosition() + relativePosition );
}
//----------------------------------------------------

void AstroMotor::move( AstroMotorSpeed speed, AstroMotor::Direction direction)
{
	if ( direction == CLOCKWISE )
		moveTo( speed, currentPosition()+10000000L );	
	else
		moveTo( speed, currentPosition()-10000000L );	
}
//----------------------------------------------------

void AstroMotor::setDefaultSpeed( AstroMotorSpeed spd, 
									AstroMotor::AstroStepMode mode, float stepSpeed )
{
	_controlSpeed[ spd ].mode = mode;
	_controlSpeed[ spd ].stepSpeed = stepSpeed;	
}
//----------------------------------------------------

void AstroMotor::setStepMode( AstroStepMode newMode )
{

	if (newMode == MICROSTEP)	
		digitalWrite( _pinSpeed, HIGH );
	else if (newMode == FULLSTEP)
		digitalWrite( _pinSpeed, LOW );

}
//----------------------------------------------------

long AstroMotor::currentPosition(void)
{
	AstroStepMode curMode = _controlSpeed[ _currentSpeed ].mode;
	if ( curMode == MICROSTEP )
		return AccelStepper::currentPosition();
	else {
		return AccelStepper::currentPosition() * _microstepSize;
	}
}
//----------------------------------------------------

void AstroMotor::stop(void)
{
	AstroStepMode curMode = _controlSpeed[_currentSpeed].mode;

	AccelStepper::stop();

	
/*	delay(10);
	if ( curMode == FULLSTEP ) {
		AccelStepper::setCurrentPosition( AccelStepper::currentPosition() * _microstepSize );
	}
	setStepMode(MICROSTEP);
*/	
	_onTargetWarning = false;
//	_currentSpeed = ZERO;   // desaceleracao!! nao e' zero ainda...
}
//----------------------------------------------------
//----------------------------------------------------

