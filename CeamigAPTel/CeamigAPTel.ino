// CeamigAPTel - Ceamig Access Point Telescope
//
// ESP8266 Boot mode:
//                 GPIO15 GPIO0 GPIO2    (boot mode - decimal)
// upload/UART       0      0     1         1
// normal boot       0      1     1         3
// SD-Card boot      1      0     0         4     (nao usado)

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "FS.h"
#include <EEPROM.h>
#include <String.h>
#include "Telescope.h"
#include "Keypad.h"
#include "Util.h"
#include "ConfigHw.h"

int G_debug = 0;

const char VERSAO[] = "1.2.0";

// Servers:
// os valores das portas serao atualizados em configWifi()
WiFiServer server(4030);				// server de interface com o telescopio
ESP8266WebServer serverConfig(80);		// server de configuracao de parametros do telescopio

Telescope telescope(Telescope::MEADE, Telescope::ALTAZ);

//----------- Estruturas de configuracao para gravar em EEPROM
// Preferivel usar EEPROM pois nao sobrescreve os dados de configuracao quando grava o SSPI.
const char cfgAssinatura[4+1] = "1A2B";       // Maximo: 4 bytes
// Assinatura para garantir que as estruturas de configuracao estao corretas e foram inicializadas.
const unsigned int ADDR_EEPROM_SIGN = 0;
const unsigned int ADDR_EEPROM_CONFIGHW = ADDR_EEPROM_SIGN + sizeof(cfgAssinatura);

// Parametros defaut de configuracao: (podem ser mudados pela pagina de configuracao web)
struct ConfigHw_st  configHw  =  { 
		"CeamigWiFi",
		IPAddress(10,0,0,1),
		IPAddress(255,255,255,0),
		4030,
		80,
		
		Telescope::MEADE,
		Telescope::ALTAZ,
		
		-19.933,					// Dados de Belo Horizonte
		43.95,						// Longitude: 0 a 360 graus
		-3.0,
		910,
		// Motores:
		191.0 / 11.0 * 4.8 * 4.8 * 200 * 16,		// engrenagens e caixa de reducao padrao
		191.0 / 11.0 * 4.8 * 4.8 * 200 * 16,
		true,						// inverteAzimute
		false,						// inverteAltura
		1200,						// velocidade de motor
		1200,
		1500,						// aceleracao
		1500,
		true,						// Azm 16 microsteps/step
		true,						// Alt
		// Handpad:
		true,						// handpad: chip pcf8574A: true
	};

const unsigned int EEPROM_SIZE = sizeof(cfgAssinatura) + sizeof(configHw);
//---------------------------------------


void setup()
{
  initHardware();
  delay(500);

  Serial.println();

  EEPROM.begin(EEPROM_SIZE);

//  SPIFFS.begin();  // inicializa sistema de arquivos

  // Verifica assinatura na configuracao da EEPROM e carrega os dados armazenados:
  // Para DEBUG: writeToEEPROM(ADDR_EEPROM_SIGN, "0X0X"); // força assinatura incorreta

  if (VerificarAssinatura()) {
    //GravarParametrosHw();  Serial.println("Gravar OK");// executar somente para gravar inicialmente os parametros
    // teste:
    //configHw.latitude = 33.7;

    // Assinatura Ok. Ler parametros:
    LerParametrosHw();
  } else {
    // Assinatura incorreta. Gravar parametros default:
    Serial.println("Erro Assinatura EEPROM.");
    GravarAssinatura();
    GravarParametrosHw();
    // Le novamente para ter parametros validos na var cfg:
    if ( LerParametrosHw() )
    	Serial.println("Gravaçao de Parametros Default OK.");
    tone(300, 500);  // erro!
  }

  Serial.println("Configurando Ponto de Acesso...");

  // variaveis globais dos servidores:
  //WiFiPort = configHw.netPort;
  //WiFiPortConfig = configHw.netPortWeb;
  
  configWiFi(&configHw, true);	// configura o ponto de acesso Wifi

  SPIFFS.begin();  // inicializa sistema de arquivos

  InitKeypad();
  telescope.initTelescope( &configHw );

  if (0) {
    // para debug - verificar a memoria flash e mostrar na Serial:
    checkFlash();
  }

  // LER DA CONFIGURACAO
  //telescope.setModel(Telescope::CELESTRON);
  Serial.println("");
  Serial.println("--- Config: ---");
  String model =   ((configHw.telescopeModel == Telescope::MEADE) ? "MEADE" :
      				(configHw.telescopeModel == Telescope::CELESTRON) ? "CELESTRON" : "????");
  String tipo = "????";
  switch (configHw.telescopeType) {
    case (Telescope::ALTAZ):  tipo = "Alt-Az"; break;
    case (Telescope::EQUAT):  tipo = "Equat Fork"; break;
    case (Telescope::GERMAN): tipo = "Eq German"; break;
  }
  Serial.printf("Telescopio: %s/%s\n", model, tipo);
  Serial.print("Localizacao: lat/long/tz/alt: ");
  Serial.print(configHw.latitude); Serial.print(" / ");
  Serial.print(configHw.longitude); Serial.print(" / ");
  Serial.print(configHw.timeZone); Serial.print(" / ");
  Serial.println(configHw.altitude);

  Serial.println("Motores: steps/rev;rotacao;veloc;aceler; microsteps/step: ");
  Serial.printf("  Azm/RA : %f ; %s ; %d ; %d ; %d\n", configHw.RAStepsPerRev, 
  						(configHw.inverteAzimute) ? "Invert" : "Direta",
  						configHw.slewSpeedAzimute, configHw.aceleracaoAzimute,
  						(configHw.isAzmUSteps16) ? 16 : 32 );

  Serial.printf("  Alt/DEC: %f ; %s ; %d ; %d ; %d\n", configHw.DecStepsPerRev, 
  						(configHw.inverteAltura) ? "Invert" : "Direta",
  						configHw.slewSpeedAltura, configHw.aceleracaoAltura,
  						(configHw.isAltUSteps16) ? 16 : 32 );

  Serial.printf("Handpad: tipo %s\n", (configHw.isHPadTipoA) ? "0 (pcf8574 :0x20)" : "A (pcf8574A :0x38)");
  						
  Serial.println("---------------");


  //Serial.print("Motor Max Speed: ");
  //Serial.println(telescope.getMaxSpeed(Telescope::MOTORAZM));
  tone(4000, 100);
  delay(100);
  tone(4000, 100);
}
//--------------------------------------------------------------

boolean GravarParametrosHw()
{
	return writeToEEPROM(ADDR_EEPROM_CONFIGHW, configHw);
}
// ---------------------------------------------------------------------------

boolean LerParametrosHw(void)
{
	return readFromEEPROM(ADDR_EEPROM_CONFIGHW, configHw);
}
// ---------------------------------------------------------------------------

boolean VerificarAssinatura()
{
  char tmp[5] = "....";
  readFromEEPROM(ADDR_EEPROM_SIGN, tmp);
  if ( tmp[0] == cfgAssinatura[0] && tmp[1] == cfgAssinatura[1] &&
       tmp[2] == cfgAssinatura[2] && tmp[3] == cfgAssinatura[3] )
    return true;
  else
    return false;
}
//-----------------------------------------------------

boolean GravarAssinatura()
{
	return writeToEEPROM(ADDR_EEPROM_SIGN, cfgAssinatura);
}
//-----------------------------------------------------


// serial end ethernet buffer size
#define BUFFER_SIZE 128

WiFiClient cliente;

//------------------------- LOOP --------------------------------

void loop()
{
  uint16_t bytes_read;
  uint8_t net_buf[BUFFER_SIZE] = {0};

  //	if (G_debug != 0) {
  //		Serial.print("Debug: "); Serial.println(G_debug);
  //	}
  // trata requisicoes do server de configuracao:
  serverConfig.handleClient();

  processaKeypad();
  if (telescope.onTargetWarning()) {
    telescope.clearOnTargetWarning();
    tone(3000, 300);
    //Serial.println("main - onTargetWarning");
  }
  delay(1);

  // Verifica se um cliente conectou
  cliente = server.available();
  if (cliente) {
    cliente.setNoDelay(true);
    while (!cliente.available()) {
      processaKeypad();
      //Serial.print(".");
      delay(1);
    }
  }

  if (cliente && cliente.connected()) {
    // check the network for any bytes to send to the serial
    int count = cliente.available();
    //   	Serial.printf("!%d", count);
    if (count > 0) {
      //	        Serial.print("~");
      bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
      // Para debug de comandos:
      //Serial.print("Cmd: "); Serial.print(bytes_read, DEC); Serial.print(" "); Serial.write(net_buf, bytes_read); Serial.println("");

      bool cmdOk = telescope.processaCmd( (char*) net_buf, bytes_read );
      if (!cmdOk)
        Serial.println( (char*) net_buf );

      delay(10);
      //Serial.flush();
    }
    delay(1);
  }
  delay(1);
}
//--------------------------------------------------------------

void configWiFi(ConfigHw_st *configHw, bool bPrintInfo)
{
  delay(2000);

  IPAddress apIP = configHw->netIPAddress;
  IPAddress apSubRede = configHw->netSubnetMask;
  //IPAddress apIP(10, 0, 0, 1);  // para teste
  //IPAddress apSubRede(255,255,255,0);
  
  WiFi.softAPConfig(apIP, apIP, apSubRede);

  // Mac Address (para compor o SSID "unico":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  IPAddress myIP = WiFi.softAPIP();
  // Anexar os dois ultimos digitos (em HEXA) do mac address ao SSID:
  char buff[5];
  sprintf( buff, "%02X%02X", mac[WL_MAC_ADDR_LENGTH - 2], mac[WL_MAC_ADDR_LENGTH - 1] );
  String macBytes = buff;
  String wifiSSIDCompl = String(configHw->deviceName) + String("-") + macBytes;
  /* You can remove the password parameter if you want the AP to be open. */
  //WiFi.softAP(wifiSSIDCompl, WiFiSENHA);
  //WiFi.softAP(WiFiSSID);

  WiFi.softAP(wifiSSIDCompl.c_str());

  server.begin(configHw->netPort);		// server de interface do telescopio
  server.setNoDelay(true);
  serverConfig.begin(configHw->netPortWeb);  // server web de configuracao

  serverConfig.on( "/", HTTP_GET, handleWebRoot );
  serverConfig.on( "/", HTTP_POST, handleWebRootPost );
  serverConfig.on( "/Ceamig-Logo-Foto.jpg", HTTP_GET, handleLogo );
  serverConfig.on( "/setserial", handleSetSerial );
  serverConfig.onNotFound( handleNotFound );
  serverConfig.begin();	// server de configuracao de parametros

  if (bPrintInfo) {
    Serial.printf("SSID: %s\n", wifiSSIDCompl.c_str() );
    Serial.printf("AP endereco IP: %s\n", myIP.toString().c_str() );
    Serial.printf("AP mascara de SubRede: %s\n", apSubRede.toString().c_str() );
    Serial.printf("Porta: %d\n", configHw->netPort);
    Serial.printf("Porta Configuracao: %d\n", configHw->netPortWeb);
  }
}
//--------------------------------------------------------------

void handleWebRoot() {
	String temp;
	char buf[50] = {0};
	
	Serial.println("======= ROOT ========");
	//if (SPIFFS.exists("/CeamigWiFiConfig.html")) {
	File f = SPIFFS.open("/CeamigWiFiConfig.html", "r");
	temp = f.readString();
	//Serial.println(temp);
	
	// configura os campos da pagina:
	//
	temp.replace("$verFirmware$", VERSAO);
	
	IPAddress myIP = WiFi.softAPIP();
	temp.replace("$enderIP$", myIP.toString());
	
	uint8_t mac[WL_MAC_ADDR_LENGTH];
	WiFi.softAPmacAddress(mac);
	// Anexar os dois ultimos digitos (em HEXA) do mac address ao SSID:
	sprintf(buf, "%02X:%02X:%02X:%02X", mac[WL_MAC_ADDR_LENGTH - 4],
										mac[WL_MAC_ADDR_LENGTH - 3],
										mac[WL_MAC_ADDR_LENGTH - 2],
										mac[WL_MAC_ADDR_LENGTH - 1] ); 
	//macBytes.toUpperCase();
	temp.replace("$macAddr$", buf);  memset(buf, 0, sizeof(buf));
	
	temp.replace("$deviceName$", configHw.deviceName);
	sprintf(buf, "-%02X%02X", mac[WL_MAC_ADDR_LENGTH - 2], mac[WL_MAC_ADDR_LENGTH - 1] );  
	temp.replace("$deviceName_Mac$", buf);   memset(buf, 0, sizeof(buf));

	switch (configHw.telescopeModel) {
		case Telescope::MEADE:		temp.replace("Meade_LX200\">", "Meade_LX200\" selected>"); break;
		case Telescope::CELESTRON:	temp.replace("Celestron_NexStar\">", "Celestron_NexStar\" selected>"); break;
	}

	switch (configHw.telescopeType) {
		case Telescope::ALTAZ:	temp.replace("AltAzimutal\"", "AltAzimutal\" selected"); break;
		case Telescope::GERMAN:	temp.replace("EquatorialGermanica\"", "EquatorialGermanica\" selected"); break;
		case Telescope::EQUAT:	temp.replace("EquatorialFork\"", "EquatorialFork\" selected"); break;
	}
	
	// Motores:
	temp.replace("$azm_steps$", String(configHw.RAStepsPerRev, 0) );
	temp.replace("$azm_velmax$", String(configHw.slewSpeedAzimute) );
	temp.replace("$azm_aceler$", String(configHw.aceleracaoAzimute) );
	temp.replace("$azmRotDireta$", (configHw.inverteAzimute)? "" : "checked");
	temp.replace("$azmRotInvert$", (configHw.inverteAzimute)? "checked" : "");
	temp.replace("$azmUSteps16$", (configHw.isAzmUSteps16)? "checked" : "");
	temp.replace("$azmUSteps32$", (configHw.isAzmUSteps16)? "" : "checked");

	temp.replace("$alt_steps$", String(configHw.DecStepsPerRev, 0) );
	temp.replace("$alt_velmax$", String(configHw.slewSpeedAltura) );
	temp.replace("$alt_aceler$", String(configHw.aceleracaoAltura) );
	temp.replace("$altRotDireta$", (configHw.inverteAltura)? "" : "checked");
	temp.replace("$altRotInvert$", (configHw.inverteAltura)? "checked" : "");
	temp.replace("$altUSteps16$", (configHw.isAltUSteps16)? "checked" : "");
	temp.replace("$altUSteps32$", (configHw.isAltUSteps16)? "" : "checked");
	// Handpad:
	temp.replace("$hpad_0$", (configHw.isHPadTipoA)? "" : "checked");
	temp.replace("$hpad_A$", (configHw.isHPadTipoA)? "checked" : "");

	temp.replace("$latitude$", String(configHw.latitude, 3) );
	temp.replace("$longitude$", String(configHw.longitude, 3) );
	temp.replace("$timezone$", String(configHw.timeZone, 1) );
	temp.replace("$altitude$", String(configHw.altitude, 0) );

	

	//	temp.replace("$ender_ip$", configHw.netIPAddress.toString() );  // NAO FUNCIONA - BUG DA LIB!!!
	IPAddress netIP = configHw.netIPAddress;   
	temp.replace("$ender_ip$", netIP.toString() ); 
	IPAddress netSM = configHw.netSubnetMask;
	temp.replace("$ender_subrede$", netSM.toString() );
	temp.replace("$porta_rede$", String(configHw.netPort) );

	
	serverConfig.send( 200, "text/html", temp.c_str() );
	f.close();
}
//--------------------------------------------------------------

void handleWebRootPost()
{
	
	Serial.println("======= ROOT - POST !!! ========");
	String message = "URI: ";

	message += serverConfig.uri();
	message += "\nArgumentos: ";
	message += serverConfig.args();
	message += "\n";
	for ( uint8_t i = 0; i < serverConfig.args(); i++ ) {
		message += " " + serverConfig.argName( i ) + ": " + serverConfig.arg( i ) + "\n";
	}
	// Arguments: device_name=CeamigWiFi
	// Arguments: Telecopio+Tipo=Meade+LX200&Tipo+Montagem=AltAzimutal&azmRotacao=AZM_DIRETA&
	//   azim_steps=1296000&azm_velmax=1200&azm_aceler=1500&altRotacao=ALT_DIRETA&alt_steps=1294999&alt_velmax=1200&alt_aceler=1500
	// Arguments: latitude=-19&longitude=-43&timezone=-3&altitude=910
	// Arguments: settings_address_adhoc=10.0.0.1&settings_address_adhoc_subnet=255.255.255.0&porta=4030
	for ( uint8_t i = 0; i < serverConfig.args(); i++ ) {
		String nomeArg = serverConfig.argName( i );
		String valArg  = serverConfig.arg( i );
		if ( nomeArg == String("device_name") ) {
			strncpy(configHw.deviceName, valArg.c_str(), 25);
		} else if ( nomeArg == String("Telecopio+Tipo") ) {
			if (valArg == String("Celestron_NexStar") ) {
				configHw.telescopeModel = Telescope::CELESTRON;
			} else {
				configHw.telescopeModel = Telescope::MEADE;
			}
		} else if ( nomeArg == String("Tipo+Montagem") ) {
			if (valArg == String("EquatorialGermanica") ) {
				configHw.telescopeType = Telescope::GERMAN;
			} else if (valArg == String("EquatorialFork") ) {
				configHw.telescopeType = Telescope::EQUAT;
			} else {
				configHw.telescopeType = Telescope::ALTAZ;
			}
		} else if ( nomeArg == String("azm_steps") ) {		// MOTOR AZIMUTE
			configHw.RAStepsPerRev = valArg.toInt();
		} else if ( nomeArg == String("azm_velmax") ) {
			configHw.slewSpeedAzimute = valArg.toInt();
		} else if ( nomeArg == String("azm_aceler") ) {
			configHw.aceleracaoAzimute = valArg.toInt();
		} else if ( nomeArg == String("azmRotacao") ) {				
			configHw.inverteAzimute = (valArg == String("AZM_INVERT"))? true : false;
		} else if ( nomeArg == String("azmUSteps") ) {				
			configHw.isAzmUSteps16 = (valArg == String("AZM_U16"))? true : false;
			
		} else if ( nomeArg == String("alt_steps") ) {		// MOTOR ALTURA
			configHw.DecStepsPerRev = valArg.toInt();
		} else if ( nomeArg == String("alt_velmax") ) {
			configHw.slewSpeedAltura = valArg.toInt();
		} else if ( nomeArg == String("alt_aceler") ) {
			configHw.aceleracaoAltura = valArg.toInt();
		} else if ( nomeArg == String("altRotacao") ) {				
			configHw.inverteAltura = (valArg == String("ALT_INVERT"))? true : false;
		} else if ( nomeArg == String("altUSteps") ) {				
			configHw.isAltUSteps16 = (valArg == String("ALT_U16"))? true : false;
			
		// Handpad:
		} else if ( nomeArg == String("handpad") ) {				
			configHw.isHPadTipoA = (valArg == String("HPAD_A"))? true : false;
			
		} else if ( nomeArg == String("latitude") ) {
			configHw.latitude = valArg.toFloat();
		} else if ( nomeArg == String("longitude") ) {
			configHw.longitude = valArg.toFloat();
		} else if ( nomeArg == String("timezone") ) {
			configHw.timeZone = valArg.toFloat();
		} else if ( nomeArg == String("altitude") ) {
			configHw.altitude = valArg.toInt();
		} else if ( nomeArg == String("ender_ip") ) {
			int parms[4] = {0};
        	short n = parseInt( valArg.c_str(), 4, ".", parms );
        	if ( n == 4 ) {
        		configHw.netIPAddress = IPAddress(parms[0],parms[1],parms[2],parms[3]);
        	}
		} else if ( nomeArg == String("ender_subrede") ) {
			int parms[4] = {0};
        	short n = parseInt( valArg.c_str(), 4, ".", parms );
        	if ( n == 4 ) {
        		configHw.netSubnetMask = IPAddress(parms[0],parms[1],parms[2],parms[3]);
        	}
		} else if ( nomeArg == String("porta_rede") ) {
			configHw.netPort = valArg.toInt();
		}
	}	

	// Gravar na EEPROM:
	GravarParametrosHw();

	handleWebRoot();
	//serverConfig.send( 200 );		// send HTTP status code OK (200)
	
	//Serial.println( message );

}
//--------------------------------------------------------------

void handleSetSerial()
{
  //Serial.println("======= /setserial ========");
  serverConfig.send(204);  // no content!
}
//--------------------------------------------------------------

void handleLogo()
{
  //Serial.println("====  LOGO ====");
  String logo = "/Ceamig-Logo-Foto.jpg";
  File f = SPIFFS.open("/Ceamig-Logo-Foto.jpg", "r");
  serverConfig.streamFile(f, "image/jpeg");   // com problema nessa versao.... :-((
  /*char buf[1024];
    int siz = f.size();
    while(siz > 0) {
  	size_t len = min((int)(sizeof(buf) - 1), siz);
  	f.read((uint8_t *)buf, len);
     	serverConfig.client().write((const char*)buf, len);
  	siz -= len;
    } //- See more at: http://www.esp8266.com/viewtopic.php?p=57334#sthash.3IdCvS1j.dpuf
  */
  f.close();
}
//--------------------------------------------------------------

void handleNotFound() {
  String message = "Arquivo nao encontrado\n\n";
  message += "URI: ";
  message += serverConfig.uri();
  message += "\nMetodo: ";
  message += ( serverConfig.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += serverConfig.args();
  message += "\n";

  for ( uint8_t i = 0; i < serverConfig.args(); i++ ) {
    message += " " + serverConfig.argName( i ) + ": " + serverConfig.arg( i ) + "\n";
  }

  serverConfig.send ( 404, "text/plain", message );

  Serial.println( message );
}
//--------------------------------------------------------------
//--------------------------------------------------------------
