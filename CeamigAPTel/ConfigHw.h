/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file ConfigHw.h
 * @author Marcos Ackel
 * @date  June 2017
 * @brief Configuracao de Hardware
 */
#ifndef CONFIGHW_H
#define CONFIGHW_H

#include "Telescope.h"


// Estrutura de parametros de configuracao:

typedef struct ConfigHw_st {
	// Dispositivo:	
	char deviceName[25+1];
	// Rede:
	IPAddress netIPAddress;
	IPAddress netSubnetMask;
	uint16_t  netPort;		// Porta de rede para o telescopio
	uint16_t  netPortWeb;   // Porta do servidor Web de configuracao

	// Dados do Telescopio:
	Telescope::TelModel telescopeModel;	// Modelo telescopio (Meade, Celestron)
	Telescope::TelType  telescopeType;     // Tipo de montagem do telescopio: Alt-Azim, Equatorial...
	
	// Localizacao:
	float latitude;			// latitude: -90 a +90
	float longitude;		// longitude: 0 a 360
	float timeZone;			// em horas
	float altitude;			// em metros
	
	// Motores:
	double RAStepsPerRev;				// RA/Azimute
	double DecStepsPerRev;				// DEC/Altura
	bool   inverteAzimute;
	bool   inverteAltura;
	uint16_t slewSpeedAzimute;			// em steps por segundo
	uint16_t slewSpeedAltura;
	uint16_t aceleracaoAzimute;			// em steps por segundo por segundo.
	uint16_t aceleracaoAltura;
	bool isAzmUSteps16;					// driver A4988 (16 usteps/step): true    DRV8825 (32 usteps/step): false
	bool isAltUSteps16;					// driver A4988 (16 usteps/step): true    DRV8825 (32 usteps/step): false

	// Handpad:
	bool isHPadTipoA;					// pcf8754A (ender 0x38): true    pcf8754 (ender 0x20): false
};

#endif
