/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file Util.h
 * @author Marcos Ackel
 * @date  June 2017
 * @brief Utility functions.
 */

#ifndef UTIL_H
#define UTIL_H

#define min(a,b) ((a)<(b)?(a):(b))

void initHardware(void);

void tone(int freq, int duracao);

void checkFlash(void);

String makeString( char* str, byte len );
char * strpbrk2(const char* s1, const char* s2);

int parseInt( const char* cmd, int numInts, char* cSeparators, int aIntsParsed[]);

int parseHexUInt( const char* cmd, int numUInts, char* cSeparators, unsigned int aUIntsParsed[]);

template <class T> bool writeToEEPROM(int ee, const T& value);
template <class T> bool readFromEEPROM(int ee, T& value);

#endif
//----------------------------------------------------
//----------------------------------------------------
