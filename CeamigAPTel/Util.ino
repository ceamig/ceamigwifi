// Funcoes utilitarias

//#include <arduino.h>
#include "util.h"
#include <EEPROM.h>

const uint8_t PinBeep = 13;

void tone(int freq, int duracao)
{
	tone( PinBeep, freq, duracao );
	//analogWriteFreq(freq);   // freq PWM
	//analogWrite(PinBeep, 512);
	//delay(duracao);
	//analogWrite(PinBeep, 0);
}
//--------------------------------------------------------------

void initHardware()
{
	//pinMode(LED_PIN, OUTPUT);
	pinMode(PinBeep, OUTPUT);
	pinMode(A0, INPUT);
	Serial.begin(74880); // vel default do chip (para ver as msg do bootloader)
}

//--------------------------------------------------------------


void checkFlash()
{
    uint32_t realSize = ESP.getFlashChipRealSize();
    uint32_t ideSize = ESP.getFlashChipSize();
    FlashMode_t ideMode = ESP.getFlashChipMode();

	Serial.println("");
    Serial.printf("Flash real id:   %08X\n", ESP.getFlashChipId());
    Serial.printf("Flash real size: %u\n\n", realSize);

    Serial.printf("Flash ide  size: %u\n", ideSize);
    Serial.printf("Flash ide speed: %u\n", ESP.getFlashChipSpeed());
    Serial.printf("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));

    //if(ideSize != realSize) {
    //    Serial.println("Flash Chip configuration wrong!\n");
    //} else {
    //    Serial.println("Flash Chip configuration ok.\n");
    //}
	delay(1000);	
}
//--------------------------------------------------------------

/*
 * Find the first occurrence in s1 of a character in s2 (excluding NUL).
 */
char * strpbrk2(const char* s1, const char* s2) //register const char *s1, *s2;
{
	register const char *scanp;
	register int c, sc;

	while ((c = *s1++) != 0) {
		for (scanp = s2; (sc = *scanp++) != 0;)
			if (sc == c)
				return ((char *)(s1 - 1));
	}
	return (NULL);
}
//----------------------------------------------------

String makeString( char* str, byte len )
{
	char buf[100] = {0};
	memcpy(buf, str, len);
	String ret = String( buf );
	return ret;
}
//----------------------------------------------------

int parseInt( const char* cmd, int numInts, char* cSeparators, int aIntsParsed[])
{
    char* ptr;
    int nIntsParsed = 0;
    char tmpChr;
    const char* iniPtr = cmd;
    while ((ptr = strpbrk2(iniPtr, cSeparators)) != NULL && nIntsParsed < numInts) {
        tmpChr = *ptr;
        *ptr = '\0';
        aIntsParsed[nIntsParsed] = atoi(iniPtr);
        *ptr = tmpChr;
        iniPtr = ptr + 1;
        nIntsParsed++;
    }
    if (*iniPtr != '\0' && nIntsParsed < numInts) {
    	aIntsParsed[nIntsParsed] = atoi(iniPtr);
    	nIntsParsed++;
	}    
    return nIntsParsed;
}
//----------------------------------------------------

int parseHexUInt( const char* cmd, int numUInts, char* cSeparators, unsigned int aUIntsParsed[])
{
    char* ptr;
    int nUIntsParsed = 0;
    char tmpChr;
    const char* iniPtr = cmd;
    while ((ptr = strpbrk2(iniPtr, cSeparators)) != NULL && nUIntsParsed < numUInts) {
        tmpChr = *ptr;
        *ptr = '\0';
        aUIntsParsed[nUIntsParsed] = strtoul(iniPtr, NULL, 16);
        *ptr = tmpChr;
        iniPtr = ptr + 1;
        nUIntsParsed++;
    }
    if (*iniPtr != '\0' && nUIntsParsed < numUInts) {
    	aUIntsParsed[nUIntsParsed] = strtoul(iniPtr, NULL, 16);
    	nUIntsParsed++;
	}
    return nUIntsParsed;
}
//----------------------------------------------------

template <class T> bool writeToEEPROM(int ee, const T& value)
{
    const byte* p = (const byte*)(const void*)&value;
    unsigned int i;
    bool retVal = false;
    for (i = 0; i < sizeof(value); i++)
          EEPROM.write(ee++, *p++);
    if ( EEPROM.commit() && i == sizeof(value) )
    	retVal = true;
    return retVal;
}
//----------------------------------------------------

template <class T> bool readFromEEPROM(int ee, T& value)
{
    byte* p = (byte*)(void*)&value;
    unsigned int i;
    bool retVal = false;
    for (i = 0; i < sizeof(value); i++)
          *p++ = EEPROM.read(ee++);
    return ( i == sizeof(value) );
}
//----------------------------------------------------
//----------------------------------------------------
